using System;
using UnityEngine;

using Entity.WorkerNPC;
using GameLogic;

namespace Entity.Control
{
    [RequireComponent(typeof(WorkerList))]
    [RequireComponent(typeof(WorkerCreator))]
    public class WorkerControl : MonoBehaviour
    {        
        public delegate void WorkerControllerUpdate();
        public WorkerControllerUpdate u_WorkerControllerUpdate;

        public GameTick MovementTick = new GameTick();
        public GameTick BehaviorTick = new GameTick();
        public GameTick StatusTick = new GameTick();

        private GameControl _gameControl;

        #region Mono
        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
        }

        private void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(WorkUpdate);
            _gameControl.TickControl.UpdateTick.Subscribe(WorkTickUpdate);
        }

        private void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(WorkUpdate);
            _gameControl.TickControl.UpdateTick.UnSubscribe(WorkTickUpdate);
        }
        #endregion

        /// <summary>
        ///     We're going to separete the Update on stages.
        ///     Like:
        ///         -MovementState
        ///         -Status
        ///     
        ///     In theory, if we do this we can have a small performace improve.
        /// </summary>
        private void WorkUpdate()
        {
            if (MovementTick.Tick() != null)
            {
                MovementTick.Tick().Invoke();
            }

            if (BehaviorTick.Tick() != null)
            {
                BehaviorTick.Tick().Invoke();
            }
            
            if (u_WorkerControllerUpdate != null)
            {
                u_WorkerControllerUpdate.Invoke();
            }
        }

        private void WorkTickUpdate()
        {
           if (StatusTick.Tick() != null)
            {
                //Debug.Log("Calling Status Update!");

                StatusTick.Tick().Invoke();
            }
        }
    }
}