using UnityEngine;
using System.Collections;
using Entity.WorkerNPC;
using GameLogic.Interface;

namespace GameLogic.Events
{
    public class PopulationEvent : BaseEvent
    {
        public string Message;

        private WorkerCreator _workerCreator;
        private GameInterfaceControl _gameInterfaceControl;

        [Range(0, 10)]
        public int MaxNpcsByEvent = 6;

        public override void Awake()
        {
            base.Awake();

            Type = EventType.Population;

            _workerCreator = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerCreator>();
            _gameInterfaceControl =
                GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameInterfaceControl>();
        }

        protected override void Event()
        {
            base.Event();

            int rngWorker = Random.Range(0, MaxNpcsByEvent);

            for (int i = 0; i < rngWorker; i++)
            {
                _workerCreator.SpawnWorkerAtCaslte();
            }

            _gameInterfaceControl.RequestNewInterfaceUpdate(Message);
        }        
    }
}