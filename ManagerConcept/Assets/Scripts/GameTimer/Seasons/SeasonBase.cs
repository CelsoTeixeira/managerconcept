using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace GameLogic.Seasons
{
    public enum SeasonsType
    {
        None = 0,
        Spring = 1,
        Summer = 2,
        Autumn = 3,
        Winter = 4
    }

    /// <summary>
    ///     Each season has 60 ticks.
    ///     InitialSeasonEvent will be trigger on tick 1.
    ///     MiddleSeasonEvent will be trigger on tick 30.
    ///     EndSeasonEvent will be trigger on tick 59.
    ///     
    ///     Each season could have an different Event that could be trigger with some RNG.
    /// </summary>
    public class SeasonBase : MonoBehaviour
    {
        public SeasonsType Type = SeasonsType.None;

        private GameEvent _initialSeasonEvent = new GameEvent();
        private GameEvent _middleSeasonEvent = new GameEvent();
        private GameEvent _endSeasonEvent = new GameEvent();
        
        private List<GameEvent> _randomSeasonEvent = new List<GameEvent>();
        


    }
}