using UnityEngine;
using System.Collections;

[System.Serializable()]
public struct MyVector3
{
    public float X;
    public float Y;
    public float Z;

    public void Setup(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }
}
