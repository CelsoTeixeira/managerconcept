using UnityEngine;
using System.Collections;
using CameraController;

using UIGeneral;

/// <summary>
///     This is the middle class for the movement lock/unlock button.
/// When we want to lock/unlock the camera movement we just call
/// the LockUnlockCameraMovement method, it will change the status
/// on the camera movement and we pass the status as a paremeter to 
/// the UI display to change the icon.
/// </summary>
public class CameraGeneralHandler : MonoBehaviour
{
    private UIMovementController _uiMovementController;
    private CameraMovement _cameraMovement;

    void Awake()
    {
        _uiMovementController =
            GameObject.FindGameObjectWithTag(Helper.CameraMovementLockUnlock).GetComponent<UIMovementController>();

        _cameraMovement = GameObject.FindGameObjectWithTag(Helper.CameraController).GetComponent<CameraMovement>();
    }

    public void LockUnlockCameraMovement()
    {
        if (_cameraMovement.Status == CameraStatus.LOCK)
        {
            _cameraMovement.ChangeCameraStatus(CameraStatus.UNLOCK);
        }
        else if (_cameraMovement.Status == CameraStatus.UNLOCK)
        {
            _cameraMovement.ChangeCameraStatus(CameraStatus.LOCK);
        }
        
        _uiMovementController.LockUnlockMovement(_cameraMovement.Status);
    }
}