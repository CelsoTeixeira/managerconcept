using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[Serializable]
public struct ResourceComboUI
{
    public Image Image;
    public Text Text;
};

public class UIResources : MonoBehaviour 
{

    public List<ResourceComboUI> ResourcesUI = new List<ResourceComboUI>(); 
    public List<Resource> Resources = new List<Resource>(); 
    
    private ResourceContainer _resourceContainer;

    #region Mono
    void Awake()
    {
        _resourceContainer =
            GameObject.FindGameObjectWithTag(Helper.ResourceController).GetComponent<ResourceContainer>();
        
    }

    void Start()
    {
        for (int x = 0; x < _resourceContainer.Resources.Count; x++)
        {
            Resources.Add(_resourceContainer.Resources[x]);
        }

        ForceUpdate();
    }
    
    #endregion

    //We're not running this on update, we only change the visual when we actually change some data.
    private void UpdateTexts()
    {
        for (int i = 0; i < Resources.Count; i++)
        {
            ResourcesUI[i].Text.text = Resources[i].Amount.ToString();
        }
    }

    public void ForceUpdate()
    {
        UpdateTexts();
    }

}
