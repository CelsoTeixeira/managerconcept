using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation
{
    public class RandomizerCollection : ScriptableObject
    {
        public List<RandomizerDefinition> Collection;
    }
}