using System;
using System.Collections.Generic;
using Generation;

/// <summary>
///     This is used to store what we need to be able to 
/// recreate the grid when we want to load it againd.
/// 
///     From what I studied we can't serialize Unity shit, 
/// thats why we're have a MyVector3 struct.
/// </summary>

[Serializable]
public struct SaveMapDataInfo
{
    public List<MyVector3> MapPositions;                     //Each position on the grid.
    public List<TileDefinition> MapTileDefinitions;          //Each tile definition we have.

    public GridDefinition MapGridDefinition;                 //The settings used to build the grid.        
}

[Serializable]
public class SaveMapData : SaveGame
{
    public SaveMapDataInfo MapData;
}