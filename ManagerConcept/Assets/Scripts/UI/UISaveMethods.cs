using System;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

/// <summary>
///     What we need to make this work:
///         -> InputField will be used to receive the name of what we're going to do on the folder.
///         -> LoadsText it's where we display all the loads we have on the folder.
///         -> NOTE(Celso): We want to make a scrollbar and have buttons and not text to select what we want do.
/// 
///     DataSize 2500 GameObjects -> 152 KB
///     DataSize 400  GameObjects -> 12  KB
/// 
/// </summary>
public class UISaveMethods : MonoBehaviour
{
    //TODO(Celso): We need to decouple UI from logic.
    public InputField InputField;
    public Text LoadsText;

    private GridCollection _collection;
    private GridController _gridController;

    #region Mono
    void Awake()
    {
        _collection = GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<GridCollection>();
        _gridController = GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<GridController>();

        this.gameObject.SetActive(false);
    }

    public void OnEnable()
    {
        GetSaveNames();
    }
    #endregion

    public void SaveNewPlayerData()
    {
        SaveNewData(SaveDataType.PlayerData);    
        GetSaveNames();
    }

    public void SaveNewMapData()
    {
        SaveNewData(SaveDataType.MapData);
        GetSaveNames();
    }

    public void LoadMapData()
    {
        //Debug.Log("Trying to load new map data.");

        string newLoad = InputField.text;

        if (!String.IsNullOrEmpty(newLoad))
        {
            if (SaveGameSystem.DoesSaveGameExist(newLoad, SaveDataType.MapData))
            {
                SaveMapData newInfo = SaveGameSystem.LoadGame(newLoad, SaveDataType.MapData) as SaveMapData;
                MapDataInfo mapData = GridInterpreter.SaveMapDataInfoInterpreter(newInfo);

                _gridController.RecreateGrid(mapData.MapPositions, mapData.MapTileDefinitions, mapData.MapGridDefinition);
            }

            this.gameObject.SetActive(false);
        }
    }
    
    private void SaveNewData(SaveDataType dataType)
    {
        //Debug.Log("Saving new data");

        string newName = InputField.text;

        if (!String.IsNullOrEmpty(InputField.text))
        {
            switch (dataType)
            {
                case SaveDataType.PlayerData:
                    SaveObject newSave = new SaveObject();
                    newSave.PlayerName = newName;
                    SaveGameSystem.SaveGame(newSave, newName, SaveDataType.PlayerData);
                    break;

                case SaveDataType.MapData:      
                    MapDataInfo info = _collection.GetMapDataInfo();    //Get the current map information

                    //This is our check to see if we have enough data on the GridCollection.
                    if (info.MapGridDefinition == null || info.MapPositions == null || info.MapTileDefinitions == null)
                    {
                        Debug.Log("We can't create a new map data because we don't have enough data.");
                        return;
                    }

                    SaveMapData newMap = new SaveMapData();
                    newMap.MapData = GridInterpreter.MapDataInfoInterpreter(info);      //Convert MapDataInfo -> SaveMapDataInfo
                    SaveGameSystem.SaveGame(newMap, newName, SaveDataType.MapData);     //Send the info to the save system.
                    break;
            }
        }
        else if (String.IsNullOrEmpty(InputField.text))
        {
            Debug.Log("We cant store data without a name.");
        }
        
        //Debug.Log(Application.persistentDataPath);     
        //Debug.Log("Saving new information.");
    }

    /// <summary>
    ///     Checking for the text on the InputField and 
    /// trying to deleta the respective data.
    /// </summary>
    public void DeleteSaveGame(SaveDataType type)
    {
        string newText = InputField.text;

        if (!String.IsNullOrEmpty(newText))
        {
            if (SaveGameSystem.DoesSaveGameExist(newText, type))
            {
                Debug.Log("DELETING DATA!" + newText + " / " + type);
                SaveGameSystem.DeleteSaveGame(newText, type);
            }
            else if (!SaveGameSystem.DoesSaveGameExist(newText, type))
            {
                Debug.Log("We don't have any relative data referent to the Input passed.");
            }
        }
    }
    
    /// <summary>
    ///     This is used to refresh what we're
    /// displaying on the Loads text.
    ///     We want this to be called when we open
    /// the save/load panel and when we do any button
    /// press.
    /// </summary>
    public void GetSaveNames()
    {
        string[] saves = Directory.GetFiles(Application.persistentDataPath);
        string[] newSaves = new string[saves.Length];

        LoadsText.text = string.Empty;

        if (saves == null || saves.Length == 0)
            LoadsText.text = "No save games found";

        if (saves.Length > 0)
        {
            for (int i = 0; i < saves.Length; i++)
            {
                newSaves[i] = saves[i].Substring(saves[i].IndexOf(@"\") + 1);   //This will cut the final string to be only the save name.
                //Debug.Log(newSaves[i]);

                LoadsText.text += newSaves[i] + Environment.NewLine;
            }
        }    
    }


}