using UnityEngine;
using System.Collections;

public class SplashScreenTimer : MonoBehaviour
{
    public GameObject GameSplashScreen;
    public GameObject CompanySplashScreen;

    public float TimeToWait;

    public AudioSource AudioSource;

    void Awake()
    {
        CompanySplashScreen.SetActive(false);
        GameSplashScreen.SetActive(false);

        StartCoroutine(SplahScreen());
    }

    private IEnumerator SplahScreen()
    {
        CompanySplashScreen.SetActive(true);

        yield return new WaitForSeconds(TimeToWait);

        CompanySplashScreen.SetActive(false);

        GameSplashScreen.SetActive(true);

        yield return new WaitForSeconds(TimeToWait);

        GameSplashScreen.SetActive(false);        

        AudioSource.Play();
    }    
}
