using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpritesCollection : MonoBehaviour
{
    public List<Sprite> TileSprites;

    //NOTE(Celso): 0 -> Alone / 1 -> Low / 2 -> Mid / 3 -> High
    public List<Sprite> TileForest;
    public List<Sprite> TileMountain;

    public List<Sprite> TileDebug;

    //Sequencia -> LS, OLS, OS, NLS, NLSO, NSO, NL, NLO, NO, S, O, N, L

    public List<Sprite> TileBackgroundMontanha;
    public List<Sprite> TileBackgroundFloresta;
    public List<Sprite> TileBackgroundGrama;
    public List<Sprite> TileBackgroundGramaMontanha;

    public List<Sprite> SpriteFloresta;
    public List<Sprite> Montanha;
}