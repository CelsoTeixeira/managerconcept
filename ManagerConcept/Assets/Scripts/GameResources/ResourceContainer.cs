using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO(Celso): CHange namespace to Resource.

public enum ResourceType
{
    None = 0,
    Food = 1,
    Water = 2,
    Wood = 3,
}

[System.Serializable]
public class Resource
{
    public ResourceType Type;
    public double Amount;
}

public class ResourceContainer : MonoBehaviour
{
    public List<Resource> Resources = new List<Resource>();
    
    private Dictionary<ResourceType, Resource> _collection = new Dictionary<ResourceType, Resource>();

    private UIResources _uiResources;

    void Awake()
    {
        _uiResources = GameObject.FindGameObjectWithTag(Helper.UiController).GetComponent<UIResources>();

        Resource food = new Resource();
        food.Type = ResourceType.Food;
        food.Amount = 0;
        Resources.Add(food);
        _collection.Add(food.Type, food);

        Resource wood = new Resource();
        wood.Type = ResourceType.Wood;
        wood.Amount = 0;
        Resources.Add(wood);
        _collection.Add(wood.Type, wood);

        Resource water = new Resource();
        water.Type = ResourceType.Water;
        water.Amount = 0;
        Resources.Add(water);
        _collection.Add(water.Type, water);

    }
    
    public void AddAmount(ResourceType type, double amountToChange)
    {
        if (_collection.ContainsKey(type))
        {
            _collection[type].Amount += amountToChange;

            _uiResources.ForceUpdate();
        }        
    }

    //Return false if we failed to get resources.
    public bool GetAmount(ResourceType type, double amountToGet)
    {
        if (_collection.ContainsKey(type))
        {
            double toGet = _collection[type].Amount;

            toGet -= amountToGet;

            if (toGet < 0)
            {
                //Debug.Log("We dont have enough resources to consume.");
                return false;
            }

            _collection[type].Amount -= amountToGet;
            _uiResources.ForceUpdate();
            return true;
        }

        return false;
    }

    public bool TakeAmount(ResourceType type, double amount)
    {
        if (_collection.ContainsKey(type))
        {
            double toGet = _collection[type].Amount;
            toGet -= amount;

            if (toGet >= 0)
            {
                _collection[type].Amount -= amount;


                _collection[type].Amount = Mathf.Clamp((float)_collection[type].Amount, 0, 10000000);
                _uiResources.ForceUpdate();

                return true;
            }
        }

        return false;
    }

}