﻿using UnityEngine;
using System.Collections;

namespace Entity.Behaviour
{
    public static class BehaviorHelper
    {
        public static Vector3 NearPosition(Vector3 vector3, Vector2 Diference)
        {
            Vector3 nearPosition = vector3;
            nearPosition.x += Random.Range(Diference.x, Diference.y);
            nearPosition.y += Random.Range(Diference.x, Diference.y);

            return nearPosition;
        }
    }
}