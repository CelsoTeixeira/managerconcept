using UnityEngine;
using System;
using Building;
using CameraController;
using GameLogic;

/// <summary>
///     Every single method we want to be trigger by the Kerboard, should be done here.
///     We have two differents types of events for now, one for Update and another for Pause.
///     Update:
///         -PauseGame -> Pause the game.
///         -IncreaseTime   -> Increase the time scale.
///         -DecreaseTime   -> Decrease the time scale.
///         -LockUnlockCamera   -> Lock/Unlock the camera movement.
///         -CameraFocusCastle  -> Move the camera to the castle.
/// 
///     Pause:
///         -ResumeGame -> Resume the game.
///     
/// </summary>
public class KeyboardInput : MonoBehaviour
{
    private GameTick _keyboardUpdateTick = new GameTick();
    private GameTick _keyboardPauseTick = new GameTick();

    public KeyCode PauseResumeKey = KeyCode.Escape;

    public KeyCode IncreaseTimeKey = KeyCode.Plus;
    public KeyCode DecreaseTimeKey = KeyCode.Minus;

    public KeyCode LockUnlockCameraKey = KeyCode.BackQuote;
    public KeyCode CameraFocusCastleKey = KeyCode.Space;
    
    private GameControl _gameControl;
    private GameTimeUI _gameTimeUi;
    private CameraMovement _cameraMovement;
    private BuildTracking _buildTracking;

    void Awake()
    {
        _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
        _gameTimeUi = GameObject.FindGameObjectWithTag(Helper.UiController).GetComponent<GameTimeUI>();
        _cameraMovement = GameObject.FindGameObjectWithTag(Helper.CameraController).GetComponent<CameraMovement>();
        _buildTracking = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildTracking>();

        _keyboardUpdateTick.Subscribe(PauseGame);
        _keyboardUpdateTick.Subscribe(IncreaseTime);
        _keyboardUpdateTick.Subscribe(DecreaseTime);
        _keyboardUpdateTick.Subscribe(LockUnlockCamera);
        _keyboardUpdateTick.Subscribe(CameraFocusCastle);

        _keyboardPauseTick.Subscribe(ResumeGame);
    }

    void OnEnable()
    {
        _gameControl.TickControl.UpdateConstantTick.Subscribe(KeyboardConstantUpdate);
        _gameControl.TickControl.PauseTick.Subscribe(KeyboardConstantPause);
    }

    void OnDisable()
    {
        _gameControl.TickControl.UpdateConstantTick.UnSubscribe(KeyboardConstantUpdate);
        _gameControl.TickControl.PauseTick.UnSubscribe(KeyboardConstantPause);
    }

    private void KeyboardConstantUpdate()
    {
        if (_keyboardUpdateTick.Tick() != null)
        {
            _keyboardUpdateTick.Tick().Invoke();
        }
    }

    private void KeyboardConstantPause()
    {
        if (_keyboardPauseTick.Tick() != null)
        {
            _keyboardPauseTick.Tick().Invoke();
        }
    }

    #region Update shortcuts
    private void PauseGame()
    {
        if (Input.GetKeyDown(PauseResumeKey))
        {
            _gameControl.ChangeGameState(GameState.Pause);
        }
    }
    
    private void IncreaseTime()
    {
        //Debug.Log("Run");

        if (Input.GetKeyDown(IncreaseTimeKey))
        {
            //Debug.Log("Hit");

            _gameTimeUi.IncreaseTimeScale();
        }
    }

    private void DecreaseTime()
    {
        if (Input.GetKeyDown(DecreaseTimeKey))
        {
            _gameTimeUi.DecreaseTimer();
        }
    }

    private void LockUnlockCamera()
    {
        if (Input.GetKeyDown(LockUnlockCameraKey))
        {
            _cameraMovement.LockUnlockStatus();
        }
    }

    private void CameraFocusCastle()
    {
        if (Input.GetKeyDown(CameraFocusCastleKey))
        {
            _cameraMovement.ForceCameraPosition(_buildTracking.CityCenterPosition);
        }
    }
    #endregion

    #region Pause shortcuts
    private void ResumeGame()
    {
        if (Input.GetKeyDown(PauseResumeKey))
        {
            _gameControl.ChangeGameState(GameState.Update);
        }
    }
    #endregion
}