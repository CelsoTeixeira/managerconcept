using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Biomes
{
    public class MountainBiome
    {
        public List<TileInformation> GenerateMountainBiome(EpicenterData data, GridCollection col)
        {
            int amountToGenerate = BiomeUtility.GetIterationByBiomeSize(data.Size);

            Vector3 epicenter = BiomeUtility.GetRandomEpicenter(col.GetGridDefinition());

            List<Vector3> tilePos = new List<Vector3>();
            List<TileInformation> tileUsed = new List<TileInformation>();

            tilePos.Add(epicenter);

            int iterationCounter = 0;

            do
            {
                Vector3 currentPos = new Vector3();

                int rngPos = Random.Range(0, tilePos.Count);

                currentPos = tilePos[rngPos];
                tilePos.RemoveAt(rngPos);

                TileInformation currentInfo = col.GetValueOnCollection(currentPos);

                if (!tileUsed.Contains(currentInfo))
                {
                    //Change the tile sprite
                    currentInfo.ChangeTileType(TileType.Mountain);
                    
                    tileUsed.Add(currentInfo);
                    iterationCounter++;
                }
                
                List<Vector3> nearTiles = BiomeUtility.GetAdjacentTileVector(currentPos, col);

                foreach (Vector3 nearTile in nearTiles)
                {
                    tilePos.Add(nearTile);
                }
                
            } while (iterationCounter < amountToGenerate);

            return tileUsed;
        }

        public void UpdateMountainTileDensity(List<TileInformation> tiles, GridCollection col)
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                List<TileInformation> nearTiles = BiomeUtility.GetAdjacentTileInfoByInfo(tiles[i], col);

                if (nearTiles != null)
                {
                    int numberOfAdjacentForest = 0;

                    for (int j = 0; j < nearTiles.Count; j++)
                    {
                        if (nearTiles[j].TileDefinition.Type == TileType.MountainHighDensity ||
                            nearTiles[j].TileDefinition.Type == TileType.MountainMidDensity ||
                            nearTiles[j].TileDefinition.Type == TileType.MountainLowDensity)
                        {
                            numberOfAdjacentForest++;
                        }
                    }

                    switch (numberOfAdjacentForest)
                    {
                        case 0:
                            tiles[i].ChangeTileType(TileType.MountainLowDensity);
                            break;

                        case 1:
                            tiles[i].ChangeTileType(TileType.MountainMidDensity);
                            break;

                        case 2:
                            tiles[i].ChangeTileType(TileType.MountainMidDensity);
                            break;

                        case 3:
                            tiles[i].ChangeTileType(TileType.MountainHighDensity);
                            break;

                        case 4:
                            tiles[i].ChangeTileType(TileType.MountainHighDensity);
                            break;
                    }
                }
            }
        }
    }
}