﻿using UnityEngine;
using System.Collections;

public enum EntityType
{
    NONE = 0,
    NPC = 1,
    BUILD = 2,
}

public class BaseEntity : MonoBehaviour 
{
    public EntityType EntityType = EntityType.NONE;

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }
}