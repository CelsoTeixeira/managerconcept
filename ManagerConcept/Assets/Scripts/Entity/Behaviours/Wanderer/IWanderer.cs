using UnityEngine;
using System.Collections;

namespace Entity.Behaviour
{
    public interface IWanderer
    {
        void ForceMovement();
    }
}