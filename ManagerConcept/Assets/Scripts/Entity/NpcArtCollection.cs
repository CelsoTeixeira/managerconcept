using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class NpcArtCollection : ScriptableObject
{
    public List<Sprite> Heads;
    public List<Sprite> Bodys;
}