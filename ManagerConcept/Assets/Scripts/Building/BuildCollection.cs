using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Building
{

    [Serializable]
    public struct BuildCostData
    {
        public float FoodCost;
        public float WoodCost;
        public int ConstructionTick;
    }

    public class BuildCollection : MonoBehaviour
    {
        public List<GameObject> Builds = new List<GameObject>();

        private Dictionary<BuildName, GameObject> _collection = new Dictionary<BuildName, GameObject>();

        void Awake()
        {
            //Populating the collection on Awake
            for (int i = 0; i < Builds.Count; i++)
            {
                BuildObject buildObjet = Builds[i].GetComponent<BuildObject>();
                AddBuild(buildObjet.BuildName, Builds[i]);
            }
        }

        void AddBuild(BuildName name, GameObject go)
        {
            if (!_collection.ContainsKey(name))
            {
                _collection.Add(name, go);
            }    
        }

        public bool ContainKeyOnCollection(BuildName name)
        {
            return _collection.ContainsKey(name);
        }

        public GameObject GetBuildFromCollection(BuildName name)
        {
            if (_collection.ContainsKey(name))
            {
                return _collection[name];
            }

            return null;
        }
        
    }

}