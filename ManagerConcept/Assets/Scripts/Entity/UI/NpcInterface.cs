using System;
using UnityEngine;
using System.Collections;
using Entity.Behaviour;
using Entity.Behaviour.Status;
using Entity.WorkerNPC;
using UnityEngine.UI;

[Serializable]
public class NpcInterface
{
    public GameObject Panel;

    public Text Name;
    public Text Status;

    public Image FoodImage;
    public Image WaterImage;
    public Image SleepImage;

    public Image PortraitImage;

    private Worker _currentWorker;
    private BehaviorUpdate _currentBehaviorUpdate;
    private StatusControl _currentStatusControl;

    public void Setup(Worker npc)
    {
        Panel.SetActive(true);

        _currentWorker = npc.gameObject.GetComponent<Worker>();
        Name.text = _currentWorker.Name;

        _currentBehaviorUpdate = npc.gameObject.GetComponent<BehaviorUpdate>();
        Status.text = _currentBehaviorUpdate.GetCurrentStatus();

        _currentStatusControl = npc.gameObject.GetComponent<StatusControl>();
        FoodImage.fillAmount = _currentStatusControl.HungerStatus.Rate/100;
        WaterImage.fillAmount = _currentStatusControl.ThirstyStatus.Rate/100;
        SleepImage.fillAmount = _currentStatusControl.SleepStatus.Rate/100;

        PortraitImage.sprite = _currentWorker.Head.sprite;        
    }

    public void Update()
    {
        Status.text = _currentBehaviorUpdate.GetCurrentStatus();

        FoodImage.fillAmount = _currentStatusControl.HungerStatus.Rate / 100;
        WaterImage.fillAmount = _currentStatusControl.ThirstyStatus.Rate / 100;
        SleepImage.fillAmount = _currentStatusControl.SleepStatus.Rate / 100;
    }

    public void DeactivePanel()
    {
        Panel.SetActive(false);
    }
}