using UnityEngine;
using System.Collections;

//TODO: Move this to some custom input / validation namespace.

public class MouseInput : MonoBehaviour
{
    public float RayLenght = 20f;
    

    public TileInformation GetRayHitTileInfo()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        ray.direction *= RayLenght;

        RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, RayLenght, LayerHelper.GridLayerBitmask);
        
        if (rayHit.collider != null)
        {
            return rayHit.collider.gameObject.GetComponent<TileInformation>();        
        }
        
        //Debug.DrawLine(ray.origin, rayHit.point, Color.red);
        return null;
    }

    public Vector3 GetRayHitPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        ray.direction *= RayLenght;

        RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, RayLenght, LayerHelper.GridLayerBitmask);

        Vector3 newPos = Vector3.one;
        
        if (rayHit.collider != null)
        {
            newPos.x = Mathf.RoundToInt(rayHit.point.x);
            newPos.y = Mathf.RoundToInt(rayHit.point.y);
            newPos.z = 0;

            return newPos;
        }

        //if (rayHit.collider == null)
        //    Debug.Log("No hit point");

        //Debug.DrawLine(ray.origin, rayHit.point, Color.red);

        return newPos;
    }
    
}