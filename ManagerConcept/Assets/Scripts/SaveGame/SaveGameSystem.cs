using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public enum SaveDataType
{
    PlayerData = 1,
    MapData = 2,
}

public static class SaveGameSystem
{
    public static bool SaveGame(SaveGame saveGame, string name, SaveDataType dataType)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name, dataType), FileMode.Create))
        {
            try
            {
                formatter.Serialize(stream, saveGame);
            }
            catch (Exception)
            {
                return false;
            }
        }

        return true;
    }

    public static SaveGame LoadGame(string name, SaveDataType dataType)
    {
        if (!DoesSaveGameExist(name, dataType))
        {
            Debug.Log(GetSavePath(name, dataType));
            return null;
        }

        BinaryFormatter formatter = new BinaryFormatter();

        using (FileStream stream = new FileStream(GetSavePath(name, dataType), FileMode.Open))
        {
            try
            {
                return formatter.Deserialize(stream) as SaveGame;
            }
            catch (Exception e)
            {
                Debug.Log("Error." + e.Message);
                return null;
            }
        }
    }

    public static bool DeleteSaveGame(string name, SaveDataType dataType)
    {
        try
        {
            File.Delete(GetSavePath(name, dataType));
        }
        catch (Exception)
        {
            return false;
        }

        return true;
    }

    public static bool DoesSaveGameExist(string name, SaveDataType dataType)
    {
        return File.Exists(GetSavePath(name, dataType));
    }

    private static string GetSavePath(string name, SaveDataType dataType)
    {
        switch (dataType)
        {
            case SaveDataType.PlayerData:
                return Path.Combine(Application.persistentDataPath, name + ".heroesSave");

            case SaveDataType.MapData:
                return Path.Combine(Application.persistentDataPath, name + ".heroesMap");
        }

        return null;
        
    }
}
