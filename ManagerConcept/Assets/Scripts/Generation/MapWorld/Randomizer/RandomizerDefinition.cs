using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation
{
    public class RandomizerDefinition : ScriptableObject
    {
        public TileType Type;
        public Sprite Sprite;
        public int AmountToGenerate;
    }
}

