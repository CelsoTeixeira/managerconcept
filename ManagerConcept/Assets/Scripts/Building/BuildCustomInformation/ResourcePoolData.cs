using UnityEngine;
using System.Collections;

namespace Entiy.Build.CustomData
{
    [System.Serializable]
    public class ResourcePoolData : ScriptableObject
    {
        public int ResourcePool;        //This is the amount of resource we have on the build.

        //RecoverRate have a flat amount right now, we could work a little more here and add a graph for it too, so we 
        //start doing some influence over tick on it to have diversity on the final amount.
        public ResourcePoolRate RecoverRate;       //This is the amount we recover each tick if there is no work done.
        public ResourcePoolRate DecayRate;         //This is the amount we spend each tick if we're working.

        public int DepletedTimeToRecover;          //This is the the amount of ticks we want to go before we can generate again.
    }
}