using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Generation.Biomes;

namespace Generation
{
    public class ForceCenterGridEmpty
    {
        public void EmptyCenter(GridCollection col)
        {
            Vector3 center = new Vector3();
            center.x = Mathf.Round(col.GetGridDefinition().Size_X/2);
            center.y = Mathf.Round(col.GetGridDefinition().Size_Y/2);
            center.z = 0f;

            List<TileInformation> centerTiles = BiomeUtility.GetAdjacentTileInfoByDistance(center, col, 4);

            for (int i = 0; i < centerTiles.Count; i++)
            {
                //centerTiles[i].ChangeTileType(TileType.EpicenterDebug2);

                if (centerTiles[i].TileDefinition.Type != TileType.Grass)
                {
                    centerTiles[i].ChangeTileType(TileType.Grass);
                }
            }
        }
    }
}


