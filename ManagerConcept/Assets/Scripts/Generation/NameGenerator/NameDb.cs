using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Name
{
    public class NameDb : ScriptableObject
    {
        public List<string> Names;
    }
}

