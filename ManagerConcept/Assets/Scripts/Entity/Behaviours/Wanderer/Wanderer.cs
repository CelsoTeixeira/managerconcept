using UnityEngine;
using System.Collections;
using Building;

namespace Entity.Behaviour
{
    public enum WandererStatus
    {
        Waiting = 1,
        Walking = 2,      
    }

    public class Wanderer : MonoBehaviour
    {
        [Tooltip("The radius around we can get a position to go.")]
        public float Radius = 2f;

        [Tooltip("Max time we want to wait before we get another position.")]
        public float MaxTimeToWait = 10f;

        [Tooltip("Min time we want to wait before we get another position.")]
        public float MinTimeToWait = 6f;

        [Tooltip("The current behaviour status.")]
        public WandererStatus Status = WandererStatus.Waiting;

        private Vector3 _cityCenter;  //We want to wanderer based on this, not from the current transform.
        private BuildTracking _buildTracking;

        private float _actualTimeToWait;
        private float _timer;

        private Vector3 _target;

        private Movement _movement;
        public Movement Movement
        {
            get { return _movement; }
            set { _movement = value; }
        }

        public void BehaviourUpdate()
        {
            switch (Status)
            {
                case WandererStatus.Waiting:
                    WaitingStatus();
                    break;

                case WandererStatus.Walking:
                    WalkingStatus();
                    break;
            }            
        }

        private void WaitingStatus()
        {
            _timer += Time.deltaTime;

            if (_timer >= _actualTimeToWait)
            {
                WalkAround();
            }
        }

        private void WalkingStatus()
        {
            if (Vector3.Distance(this.transform.position, _target) < 0.2f)
            {
                _timer = 0;

                _actualTimeToWait = Random.Range(MinTimeToWait, MaxTimeToWait);

                Status = WandererStatus.Waiting;
            }
        }

        public void WalkAround()
        {
            Vector3 randomTarget = _cityCenter + (Random.insideUnitSphere*Radius);
            randomTarget.z = transform.position.z;
            //Debug.Log("Current position: " + transform.position);
            //Debug.Log("City center: " + _cityCenter);
            //Debug.Log("Random position: " + randomTarget);
            
            _target = randomTarget;

            Status = WandererStatus.Walking;

            _movement.StartPath(randomTarget);
        }

        public void BehaviourSetup()
        {
            _buildTracking = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildTracking>();
            _cityCenter = _buildTracking.CityCenterPosition;
            
            _actualTimeToWait = Random.Range(MinTimeToWait, MaxTimeToWait);            
        }
    }
}