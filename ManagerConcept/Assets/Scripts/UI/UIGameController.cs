using UnityEngine;
using System.Collections;
using GameLogic;

public class UIGameController : MonoBehaviour
{
    private GameControl _gameControl;

    void Awake()
    {
        _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
    }

    public void BeginGame()
    {
        //Debug.Log("Trying to begin the game.");
        _gameControl.BeginGame();
    }
}