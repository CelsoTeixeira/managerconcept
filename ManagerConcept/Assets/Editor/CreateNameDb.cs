using UnityEngine;
using System.Collections;
using Generation.Name;
using UnityEditor;

public class CreateNameDb : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Names Database")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<NameDb>();
    }
}