using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: Reuse objects and not destroy and stuff.

namespace Generation
{
    [Serializable]
    public class CreateGridVisualization
    {
        public List<GameObject> EnabledGridTiles;
        
        public CreateGridVisualization()
        {
            //Initializa what we need
            EnabledGridTiles = new List<GameObject>();
        }

        //NOTE(Celso): We should move this information to an diferent class.
        public GameObject TilePrefab;               //This is our base tile prefab.
        public Transform EnabledTilesTransform;     //This is what we're to hold the grid.
        public string TileName = "TilePrefab";      //This is our tile name.
        public float SizeModifier = 5;


        public List<GameObject> CreateGridTiles(List<Vector3> pos, GridDefinition def)
        {
            //We create one game object for each position we have and put it on
            //a position based on the vector3 list.
            CreateNewTiles(def.TileSize_X, def.TileSize_Y, pos);
            
            //Return what we have just created.
            return EnabledGridTiles;
        }
        
        private void CreateNewTiles(int tileSizeX, int tileSizeY, List<Vector3> pos)
        {
            for (int x = 0; x < pos.Count; x++)
            {
                //Create a new game object
                GameObject tempTile = GameObject.Instantiate(TilePrefab, Vector3.zero, Quaternion.identity) as GameObject;
                
                //Change the local scale to the appropriate one.
                tempTile.transform.localScale = new Vector3(tileSizeX * SizeModifier, SizeModifier, tileSizeY);
                
                //Change the parent to our holder.
                tempTile.gameObject.transform.SetParent(EnabledTilesTransform);
                
                //Rotate it.
                //tempTile.transform.Rotate(new Vector3(90f, 0f, 0f), Space.World);
                

                //Change the positon
                tempTile.transform.position = pos[x];

                //Change the name to the default one.
                tempTile.gameObject.name = TileName + pos[x].ToString();

                //Put it on our grid tile tracker.
                EnabledGridTiles.Add(tempTile);               
            }            
        }
    }
}