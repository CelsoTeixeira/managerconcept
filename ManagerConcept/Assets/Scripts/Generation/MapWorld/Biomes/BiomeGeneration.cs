using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Biomes
{
    public enum BiomeSize
    {
        None = 0,
        Small = 1,
        Medium = 2,
        Large = 3
    }

    [Serializable]
    public struct BiomePercentData
    {
        public int MaximunEpicenterAttempts;

        public float EpicenterAmountToDelete;
        public float MountainPercentToGenerate;
    }

    [Serializable]
    public class BiomeGeneration
    {
        private readonly ForestBiome _forestBiome = new ForestBiome();
        private readonly MountainBiome _mountainBiome = new MountainBiome();
        private readonly BiomeEpicenters _biomeEpicenters = new BiomeEpicenters();
        private readonly ForceCenterGridEmpty _emptyCenter = new ForceCenterGridEmpty();

        private List<EpicenterData> _epicenterDatas = new List<EpicenterData>();

        public List<EpicenterData> GenerateFullBiome(List<TileInformation> tilesInfo, BiomePercentData percentData, GridCollection col)
        {
            List<TileInformation> allForest = new List<TileInformation>();
            List<TileInformation> allMountains = new List<TileInformation>();

            _epicenterDatas = _biomeEpicenters.FindEpicenter(tilesInfo, col.GetGridDefinition(),
                                                                percentData.MaximunEpicenterAttempts);

            int epicenterToDelete = (int) (_epicenterDatas.Count*percentData.EpicenterAmountToDelete);

            //foreach (EpicenterData data in _epicenterDatas)
            //{
            //    TileInformation info = col.GetValueOnCollection(data.Position);
            //    info.ChangeTileType(TileType.EpicenterDebug1);
            //}

            for (int i = 0; i < epicenterToDelete; i++)
            {
                _epicenterDatas.RemoveAt(UnityEngine.Random.Range(0, _epicenterDatas.Count));
            }

            int biomePercentToMountain = (int) (_epicenterDatas.Count * percentData.MountainPercentToGenerate);
            int biomePercentToForest = (int) (_epicenterDatas.Count - biomePercentToMountain);

            for (int i = 0; i < _epicenterDatas.Count; i++)
            {
                if (i < biomePercentToMountain)
                {
                    List<TileInformation> newMountains = _mountainBiome.GenerateMountainBiome(_epicenterDatas[i], col);

                    for (int j = 0; j < newMountains.Count; j++)
                    {
                        if (!allMountains.Contains(newMountains[j]))
                        {
                            allMountains.Add(newMountains[j]);
                        }
                    }
                }
                else if (i >= biomePercentToMountain && i <= biomePercentToForest)
                {
                    List<TileInformation> newForest = _forestBiome.GenerateForest(_epicenterDatas[i], col);

                    for (int j = 0; j < newForest.Count; j++)
                    {
                        if (!allForest.Contains(newForest[j]))
                        {
                            allForest.Add(newForest[j]);
                        }
                    }
                }
            }

            _mountainBiome.UpdateMountainTileDensity(allMountains, col);
            _forestBiome.UpdateForestTileDensity(allForest, col);
            _emptyCenter.EmptyCenter(col);

            //foreach (EpicenterData data in _epicenterDatas)
            //{
            //    TileInformation info = col.GetValueOnCollection(data.Position);
            //    info.ChangeTileType(TileType.EpicenterDebug2);
            //}

            return _epicenterDatas;
        }
        
        public List<EpicenterData> GetEpicenterDatas()
        {
            return _epicenterDatas;
        }

        public void EpicenterVisualDebug(GridCollection col)
        {
            for (int i = 0; i < _epicenterDatas.Count; i++)
            {
                TileInformation epicenterInfo = col.GetValueOnCollection(_epicenterDatas[i].Position);
                epicenterInfo.ChangeTileType(TileType.EpicenterDebug1);
            }
        }        
    }
}