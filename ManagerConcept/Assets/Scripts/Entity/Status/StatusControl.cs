using UnityEngine;
using System.Collections;
using Entity.Control;
using Entity.WorkerNPC;

namespace Entity.Behaviour.Status
{
    public class StatusControl : MonoBehaviour
    {
        public StatusBase HungerStatus = new StatusBase();
        public StatusBase ThirstyStatus = new StatusBase();
        public SleepStatus SleepStatus = new SleepStatus();
        
        public Vector2 BaseHungerModifier = new Vector2(0.88f, 3.22f);      //The base here is 1.66f
        public Vector2 BaseThirstyModifier = new Vector2(4.00f, 6.00f);     //The base here is 5.00f
        public Vector2 BaseSleepModifier = new Vector2(1.00f, 2.00f);       //The base here is 1.66f

        public float HungerWorkingModifier = 3;
        public float ThirstyWorkingModifier = 2;
        public float SleepWorkingModifier = 2;

        public float FoodConsumeRateWorking;
        public float WaterConsumeRateWorking;
        public float SleepConsumeRateWorking;

        public float FoodConsumeRate;
        public float WaterConsumeRate;
        public float SleepConsumeRate;

        private BehaviorUpdate _behaviorUpdate;
        private ResourceContainer _resourceContainer;
        private WorkerControl _workerControl;
        
        public void Init()
        {
            RandomizerModifiers();

            ChangeModifiers(WorkerState.Wanderer);
        }

        void Awake()
        {
            _behaviorUpdate = GetComponent<BehaviorUpdate>();
            _resourceContainer =
                GameObject.FindGameObjectWithTag(Helper.ResourceController).GetComponent<ResourceContainer>();

            _workerControl = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerControl>();

            HungerStatus.Setup(_behaviorUpdate, _resourceContainer);
            ThirstyStatus.Setup(_behaviorUpdate, _resourceContainer);
            SleepStatus.Setup(_behaviorUpdate, _resourceContainer);           
        }

        void OnEnable()
        {
            _workerControl.StatusTick.Subscribe(StatusControlTickUpdate);
        }

        void OnDisable()
        {
            _workerControl.StatusTick.UnSubscribe(StatusControlTickUpdate);
        }

        public void StatusControlConstantUpdate()
        {       
            PriorityHandler();
        }
                
        private void StatusControlTickUpdate()
        {
            HungerStatus.StatusTickUpdate();
            ThirstyStatus.StatusTickUpdate();
            SleepStatus.StatusTickUpdate();
        }
        
        //This should be constant
        private void PriorityHandler()
        {            
            //If we are consuming or going to consume, we dont want to leave the current state.
            if (_behaviorUpdate.StateToChange == WorkerState.Drinking || _behaviorUpdate.StateToChange == WorkerState.Eating || _behaviorUpdate.StateToChange == WorkerState.Sleeping ||
                _behaviorUpdate.CurrentState == WorkerState.Drinking || _behaviorUpdate.CurrentState == WorkerState.Eating || _behaviorUpdate.CurrentState== WorkerState.Sleeping)
            {
                return;    
            }
            
            //Debug.Log("Priority Handler", this.gameObject);

            if (SleepStatus.Necessity == StatusNecessity.High)
            {
                //Debug.Log("Going to sleep!");

                //Go to sleep.
                _behaviorUpdate.ChangeState(WorkerState.Sleeping);
                //GoingToConsume = true;
            }
            else if (HungerStatus.Necessity == StatusNecessity.High)
            {
                //Debug.Log("Going to eat!");

                //Go eat
                _behaviorUpdate.ChangeState(WorkerState.Eating);
                //GoingToConsume = true;
            }
            else if (ThirstyStatus.Necessity == StatusNecessity.High)
            {
                //Debug.Log("Going to drink!");

                //Go drink.
                _behaviorUpdate.ChangeState(WorkerState.Drinking);
                //GoingToConsume = true;
            }
            else if (HungerStatus.Necessity == StatusNecessity.Penalty)
            {
                if (HungerStatus.CurrentTickTryingToConsume == 5)
                {
                    _behaviorUpdate.ChangeState(WorkerState.Eating);
                    HungerStatus.ResetCounterTryingToConsume();
                }
            }
            else if (ThirstyStatus.Necessity == StatusNecessity.Penalty)
            {
                if (ThirstyStatus.CurrentTickTryingToConsume == 5)
                {
                    _behaviorUpdate.ChangeState(WorkerState.Drinking);
                    ThirstyStatus.ResetCounterTryingToConsume();
                }
            }
        }
       
        private void RandomizerModifiers()
        {
            FoodConsumeRate = Random.Range(BaseHungerModifier.x, BaseHungerModifier.y);
            SleepConsumeRate = Random.Range(BaseSleepModifier.x, BaseSleepModifier.y);
            WaterConsumeRate = Random.Range(BaseThirstyModifier.x, BaseThirstyModifier.y);

            FoodConsumeRateWorking = FoodConsumeRate*HungerWorkingModifier;
            WaterConsumeRateWorking = WaterConsumeRate*ThirstyWorkingModifier;
            SleepConsumeRateWorking = SleepConsumeRate*SleepWorkingModifier;
        }

        public void ChangeModifiers(WorkerState state)
        {
            if (state == WorkerState.Working)
            {
                //Debug.Log("Changing to working consume rates.");

                HungerStatus.DecayRate = FoodConsumeRateWorking;
                ThirstyStatus.DecayRate = WaterConsumeRateWorking;
                SleepStatus.DecayRate = SleepConsumeRateWorking;
            }
            else if (state != WorkerState.Working)
            {
                //Debug.Log("Changing to wanderer consume rates.");

                HungerStatus.DecayRate = FoodConsumeRate;
                ThirstyStatus.DecayRate = WaterConsumeRate;
                SleepStatus.DecayRate = SleepConsumeRate;
            }
        }
    }
}