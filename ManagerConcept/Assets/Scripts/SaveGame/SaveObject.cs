using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable()]
public class SaveObject : SaveGame
{
    public string PlayerName;

    public List<int> Ints;

    public List<MyVector3> Vectors;
}

