using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using Building;

namespace Interface
{
    [Serializable]
    public class BuildInformationUpdate
    {
        public GameObject Panel;

        public Text Name;
        public Text Type;

        public Text ConstructionStatus;
        public Text BuildStatus;

        public Text CurrentWorkers;
        public Text MaxWorkers;

        public Button RemoveWorker;
        public Button AddWorker;
        public Button RemoveAllWorkers;

        public string TrabalhadoresTexto;
        public string MaximoTrabalhadoresTexto;
        public string PocoDeAguaTexto;
        public string HabitacaoTexto;
        public string EmConstrucaoTexto;
        public string GeracaoPausadaTexto;

        public void UpdatePanel(BuildObject build)
        {
            Panel.SetActive(true);

            Name.text = BuildName(build.BuildName.ToString());
            Type.text = "Tipo: " + BuildType(build.BuildType.ToString());

            ConstructionStatus.text = ConstructionStatusString(build.ConstructionStatus.ToString());

            DisableResourceComponents();

        }

        public void UpdatePanel(BuildResource build)
        {
            Panel.SetActive(true);
            
            Name.text = BuildName(build.BuildName.ToString());
            Type.text = "Tipo: " + BuildType(build.BuildType.ToString());

            EnableResourceComponents();

            CurrentWorkers.text = TrabalhadoresTexto + " : " + build.WorkersOnSite.Count.ToString();
            MaxWorkers.text = MaximoTrabalhadoresTexto + " : " + build.ResourceWorkerData.MaximunWorkers.ToString();

            ConstructionStatus.text = ConstructionStatusString(build.ConstructionStatus.ToString());
            BuildStatus.text = ResourceWorkStatus(build.WorkStatus.ToString());

            ResourceButtons(build);
        }

        //Castle = 1,
        //Farm = 2,
        //Woodcutter = 3,
        //Water = 4,
        //House = 5,

        private string BuildName(string name)
        {
            switch (name)
            {
                case "Castle":
                    return "Castelo";

                case "Farm":
                    return "Fazenda";

                case "Woodcutter":
                    return "Serralheira";

                case "Water":
                    return PocoDeAguaTexto;

                case "House":
                    return "Casa";
            }

            return "No name";
        }


        //    EMPTY = 0,
        //    Center = 1,
        //    House = 2,
        //    ResourceGeneration = 3


        private string BuildType(string name)
        {
            switch (name)
            {
                case "Center":
                    return "Centro";

                case "House":
                    return HabitacaoTexto;

                case "ResourceGeneration":
                    return "Gerador de recursos";
            }

            return "No name";
        }

        //WaitingForWork = 0,
        //Incomplete = 1,
        //Complete = 2,
        //OnConstruction = 3

        private string ConstructionStatusString(string name)
        {
            switch (name)
            {
                case "WaitingForWork":
                    return "Esperando trabalhadores.";

                case "Incomplete":
                    return "Incompleta!";

                case "Complete":
                    return "Completa";

                case "OnConstruction":
                    return EmConstrucaoTexto;
            }

            return "No name";
        }

        //Paused,
        //Working,

        private string ResourceWorkStatus(string name)
        {
            switch (name)
            {
                case "Paused":
                    return GeracaoPausadaTexto;

                case "Working":
                    return "Gerando recursos";
            }

            return "No name";
        }

        private void ResourceButtons(BuildResource build)
        {
            RemoveWorker.onClick.RemoveAllListeners();
            RemoveWorker.onClick.AddListener(build.RemoveResourceWorker); 

            AddWorker.onClick.RemoveAllListeners();
            AddWorker.onClick.AddListener(build.AskWorker);

            RemoveAllWorkers.onClick.RemoveAllListeners();
            RemoveAllWorkers.onClick.AddListener(build.RemoveAllWorkers);
        }

        private void DisableResourceComponents()
        {
            RemoveWorker.gameObject.SetActive(false);
            AddWorker.gameObject.SetActive(false);
            RemoveAllWorkers.gameObject.SetActive(false);

            CurrentWorkers.gameObject.SetActive(false);
            MaxWorkers.gameObject.SetActive(false);

            BuildStatus.gameObject.SetActive(false);
        }

        private void EnableResourceComponents()
        {
            RemoveWorker.gameObject.SetActive(true);
            AddWorker.gameObject.SetActive(true);
            RemoveAllWorkers.gameObject.SetActive(true);

            CurrentWorkers.gameObject.SetActive(true);
            MaxWorkers.gameObject.SetActive(true);

            ConstructionStatus.gameObject.SetActive(true);
            BuildStatus.gameObject.SetActive(true);
        }

        public void DeactivePanel()
        {
            Panel.SetActive(false);
        }
    }
}