using System;
using UnityEngine;
using System.Collections;

using GameLogic;

namespace GameLogic
{
    //NOTE(Celso): We need a way to track what we need to be displayed on screen. Like MainMenu, credits, game screen...
    public enum GameState
    {
        None = 1,
        Update = 2,
        Pause = 3,
        MainMenu = 4,
    }

    public enum GameScreen
    {
        None = 0,
        MainMenu = 1, 
        Game = 2,
        Pause = 3, 
        Credits = 4,
    }

    public class GameControl : MonoBehaviour
    {
        public GameTickControl TickControl = new GameTickControl();

        public GameState State = GameState.None;
        
                
        /*
        NOTE(Celso): This GameEvent is really a event, this is not supposed to 
        be constant, only one call and its done. Don't put stuff here that need to
        be updated on real time.
        StartGameEvent -> Rigth now it enable what we need on the UI side of the game.
        We call this when we want to start the game and thats it. When we pause we disable
        it and when we return to the game we enable again. Simple and fast.
        */
        public GameEvent StartGameEvent = new GameEvent();      //Trigger stuff when we go to the game.
        public GameEvent PauseGameEvent = new GameEvent();      //Trigger stuff when we pause the game.
        public GameEvent MainMenuEvent = new GameEvent();

        void Start()
        {
            ChangeGameState(GameState.MainMenu);
        }

        //This is our main update control.
        private void Update()
        {
            if (State == GameState.Update || State == GameState.Pause)
            {
                //Debug.Log("We are running the game.");
                TickControl.TickControlUpdate(State);
            }

            //else
            //{
            //    //Debug.Log("We are on the limbo here.");
            //}
        }

        public void ChangeGameState(GameState newState)
        {
            switch (newState)
            {
                case GameState.MainMenu:
                    MainMenuEvent.Broadcast();
                    break;

                case GameState.Update:                    
                    StartGameEvent.Broadcast();    //This will make sure we have the right stuff to run the game properly.
                    break;

                case GameState.Pause:
                    PauseGameEvent.Broadcast();     //This will make sure we change stuff when the game is paused.
                    break;                                    
            }

            State = newState;
        }

        //DEBUG
        public void BeginGame()
        {
            TickControl.ChangeTimeScale(GameTimeScale.Normal);
            
            ChangeGameState(GameState.Update);           
        }

    }
}
