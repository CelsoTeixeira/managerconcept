using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Entity.Behaviour;
using Entity.Control;
using Entity.Behaviour.Status;

using Building;

namespace Entity.WorkerNPC
{
    [RequireComponent(typeof(BehaviorUpdate))]
    public class Worker : BaseNPC, IMovement, IWorker, IWanderer
    {        
        private bool _currentSelection= false;
        public bool CurrentSelection    //This is a property because we can add some validation here.
        {
            get { return _currentSelection;}
            set { _currentSelection = value; }
        }

        private BehaviorUpdate _behaviorUpdate;
                
        private WorkerControl _workerControl;
        private WorkerList _workerList;
        
        #region Mono
        void Awake()
        {
            EntityType = EntityType.NPC;
            
            _behaviorUpdate = GetComponent<BehaviorUpdate>();

            _workerControl = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerControl>();
            _workerList = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerList>();

            //Update the BehaviourUpdate with the right references.
            _behaviorUpdate.Worker = this;
            _behaviorUpdate.WorkerList = _workerList;
        }

        void OnEnable()
        {
            _workerList.AddNewFreeWorker(this);

            _workerControl.MovementTick.Subscribe(_behaviorUpdate.MovementState.MovementUpdate);

            _workerControl.BehaviorTick.Subscribe(_behaviorUpdate.StateUpdate);            
        }

        void OnDisable()
        {
            _workerList.RemoveFreeWorker(this);

            _workerControl.MovementTick.UnSubscribe(_behaviorUpdate.MovementState.MovementUpdate);


            _workerControl.BehaviorTick.UnSubscribe(_behaviorUpdate.StateUpdate);            
        }
        #endregion
        
        public void ChangeState(WorkerState state, GameObject go)
        {
            _behaviorUpdate.ChangeState(state, go);
        }
        
        //NOTE(Celso): We dont want direct control over Workers, so we don't need this interface 
        //implemented here.
        #region IMovement implementation        
        public void GoToEmptyTile(Vector3 goTo)
        {
            _behaviorUpdate.MovementState.StartPath(goTo);
        }

        public void GoToBuild(BuildObject goTo, WorkerState state)
        {
            //Debug.Log("Going to work.");
            _behaviorUpdate.MovementState.StartPath(goTo.transform.position);

            //_behaviorUpdate.BuildToGo = goTo.gameObject;
            //State = WorkerState.Moving;

            _behaviorUpdate.ChangeState(state, goTo.gameObject);
        }
        #endregion
        
        #region Worker build control
        public void LeaveBuild()
        {
            ActiveSprites();                        
        }

        public void EnterBuild()
        {
            DeactiveSprites();
            _workerList.RemoveFreeWorker(this);            
        }

        public void AssignHome(Vector3 position)
        {
            _behaviorUpdate.Home = position;        //New home position
        }

        public void AssignJob(Vector3 position, GameObject gameObject)
        {
            _behaviorUpdate.WorkPosition = position;    //New job position
            _behaviorUpdate.WorkPlace = gameObject;     //Update the game object.
            _behaviorUpdate.HaveWork = true;            //Update bool

            IBuildWorker temp = gameObject.GetComponent<IBuildWorker>();
            _behaviorUpdate.IBuild = temp;
        }

        public void DeleteJob()
        {
            _behaviorUpdate.WorkPosition = Vector3.zero;   //Zero the workplace.
            _behaviorUpdate.HaveWork = false;           //Update the bool

            _workerList.AddNewFreeWorker(this);         //Add worker to the list.

            _behaviorUpdate.ChangeState(WorkerState.Wanderer);      //Go to Wanderer mode.
            ForceMovement();    //Force a wanderer movement;
        }

        #endregion

        #region WandererState interface
        public void ForceMovement()
        {
            _behaviorUpdate.WandererState.WalkAround();
        }
        #endregion
    }
}