using UnityEngine;
using System.Collections.Generic;

public static class GridInterpreter
{
    /// <summary>
    //  We want to send data to be saved.
    //  ->MapDataInfo
    //      ->MapDataInfo.MapPositions need to be converted to MyVector3 type.
    //  ->Return the SaveMapDataInfo with the conversion.
    /// </summary>
    public static SaveMapDataInfo MapDataInfoInterpreter(MapDataInfo info)
    {
        SaveMapDataInfo newInfo = new SaveMapDataInfo();

        //Changing the type Vector3 -> MyVector3
        List<MyVector3> newVector = new List<MyVector3>();

        for (int x = 0; x < info.MapPositions.Count; x++)
        {
            MyVector3 newPos = new MyVector3();
            newPos.Setup(info.MapPositions[x].x, info.MapPositions[x].y, info.MapPositions[x].z);

            newVector.Add(newPos);
        }

        newInfo.MapPositions = newVector;                       
        newInfo.MapTileDefinitions = info.MapTileDefinitions;
        newInfo.MapGridDefinition = info.MapGridDefinition;
        
        return newInfo;
    }

    /// <summary>
    //  We receive data from the load.
    //  -> List<MyVector3> Position.
    //      -> This need to be changed to Vector3.
    //  -> List<TileDefinition> Definitions.
    //  -> GridDefinition MyGridDefinition.
    //  We pass the Position, TileDefinition and GridDefinition to CreateGridVisualization
    //  We receive the finals GameObjects.
    //  Finish
    /// </summary>
    public static MapDataInfo SaveMapDataInfoInterpreter(SaveMapData info)
    {
        MapDataInfo newInfo = new MapDataInfo();

        //Changing the type MyVector3 -> Vector3
        List<Vector3> newVector3s = new List<Vector3>();

        for (int x = 0; x < info.MapData.MapPositions.Count; x++)
        {
            Vector3 newPos = new Vector3(info.MapData.MapPositions[x].X, info.MapData.MapPositions[x].Y, info.MapData.MapPositions[x].Z);

            newVector3s.Add(newPos);
        }

        newInfo.MapPositions = newVector3s;
        newInfo.MapTileDefinitions = info.MapData.MapTileDefinitions;
        newInfo.MapGridDefinition = info.MapData.MapGridDefinition;

        return newInfo;
    }
}
