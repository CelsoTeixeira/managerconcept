using System;

[Serializable]
public class SavePlayerData : SaveGame
{
    public string Name;         
    public int Level;           
    public float Experience;
}