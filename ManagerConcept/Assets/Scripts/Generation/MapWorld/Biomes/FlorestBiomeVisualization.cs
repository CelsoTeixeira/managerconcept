using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Biomes
{
    public enum GenerationMode
    {
        Random = 1,
        Pattern = 2
    }

    public class FlorestBiomeVisualization : MonoBehaviour
    {
        public List<Vector3> Directions = new List<Vector3>()
        {
            new Vector3(0, 0, 1),   //N
            new Vector3(1, 0, 0),    //E
            new Vector3(0, 0, -1),  //S
            new Vector3(-1, 0, 0),  //W
        };

        public  GridCollection Collection;

        public List<Vector3> usedTiles = new List<Vector3>();

        public List<Vector3> GetAdjacentTile(Vector3 vec)
        {
            List<Vector3> nearTiles = new List<Vector3>();

            for (int x = 0; x < Directions.Count; x++)
            {
                Vector3 dir = vec + Directions[x];
                //Debug.Log("Direction Vector3: " + dir);

                TileInformation tileInfo = Collection.GetValueOnCollection(dir);

                if (tileInfo != null)
                {
                    if (!usedTiles.Contains(dir))
                    {
                        nearTiles.Add(dir);
                        usedTiles.Add(dir);
                    }

                    //if (tileInfo.TileDefinition.Type != TileType.ForestHighDensity)
                    //{
                    //    nearTiles.Add(dir);
                    //}
                }
            }

            return nearTiles;
        }

        public IEnumerator GenerateVisualizeFlorest(float TimeToWait, BiomeSize size, GenerationMode mode)
        {
            int amountToGenerate;

            switch (size)
            {
                case BiomeSize.Small:
                    amountToGenerate = 42;
                    break;

                case BiomeSize.Medium:
                    amountToGenerate = 80;
                    break;

                case BiomeSize.Large:
                    amountToGenerate = 200;
                    break;

                default:
                    amountToGenerate = 10;
                    break;
            }


            GridDefinition def = Collection.GetGridDefinition();

            Vector3 epicenter = new Vector3();
            epicenter.x = Random.Range(0, def.Size_X);
            epicenter.y = 0;
            epicenter.z = Random.Range(0, def.Size_Y);

            List<Vector3> tilePos = new List<Vector3>();

            usedTiles.Clear();

            tilePos.Add(epicenter);
            usedTiles.Add(epicenter);

            do
            {
                Vector3 currentPos = new Vector3();

                switch (mode)
                {
                    case GenerationMode.Pattern:
                        currentPos = tilePos[0];
                        tilePos.RemoveAt(0);
                        break;

                    case GenerationMode.Random:
                        int rngPos = Random.Range(0, tilePos.Count);
                        currentPos = tilePos[rngPos];
                        tilePos.RemoveAt(rngPos);
                        break;
                }

                TileInformation currentInfo = Collection.GetValueOnCollection(currentPos);

                if (currentInfo != null)
                {
                    currentInfo.ChangeTileType(TileType.ForestHighDensity);
                    currentInfo.UpdateTileSprite();
                }

                List<Vector3> nearTiles = GetAdjacentTile(currentPos);

                foreach (Vector3 nearTile in nearTiles)
                {
                    tilePos.Add(nearTile);
                }

                amountToGenerate -= 1;

                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(TimeToWait);
                
            } while (amountToGenerate != 0);
            
            //Debug.Log("Generation done");

            yield return null;
        }

    }

}
