using UnityEngine;
using System.Collections;

using Pathfinding;

namespace Entity.Behaviour
{
    [RequireComponent(typeof(Seeker))]
    public class Movement : MonoBehaviour
    {
        public float Speed = 10;
        public float NextWayPointDistance = 2;

        private int _currentWaypoint = 0;

        private Seeker _seeker;
        private Path _path;

        private Rigidbody2D _rigidbody2D;

        void Awake()
        {
            _seeker = GetComponent<Seeker>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        void Start()
        {            
            _rigidbody2D.isKinematic = true;
        }

        public void StartPath(Vector3 goTo)
        {
            _seeker.StartPath(transform.position, goTo);
        }

        //This will be subscribed on the Worker class.
        public void MovementUpdate()
        {
            if (_path == null)
                return;

            if (_currentWaypoint >= _path.vectorPath.Count)
            {
                //Debug.Log("End of path reached.");
                _rigidbody2D.velocity = Vector2.zero;
                _rigidbody2D.angularVelocity = 0f;
                return;
            }

            Vector3 dir = (_path.vectorPath[_currentWaypoint] - transform.position).normalized;
            dir *= Speed * Time.deltaTime;
            transform.position += dir;
            _rigidbody2D.MovePosition(transform.position + dir);

            if (Vector3.Distance(transform.position, _path.vectorPath[_currentWaypoint]) < NextWayPointDistance)
            {
                _currentWaypoint++;                
                return;
            }
        }

        private void OnPathComplete(Path p)
        {
            //Debug.Log("Got a path back. " + p.error);

            if (!p.error)
            {
                //Reset the current waypoint.
                _path = p;
                _currentWaypoint = 0;

                _rigidbody2D.velocity = Vector2.zero;
                _rigidbody2D.angularVelocity = 0f;
            }
        }

        private void OnEnable()
        {
            _seeker.pathCallback += OnPathComplete;
        }

        private void OnDisable()
        {
            _seeker.pathCallback -= OnPathComplete;
        }

    }
}