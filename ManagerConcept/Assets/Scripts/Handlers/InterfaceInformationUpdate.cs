using UnityEngine;
using System.Collections;
using Building;
using Entity.WorkerNPC;
using GameLogic;

namespace Interface
{
    public enum CurrentSelectionType
    {
        NONE = 0, 
        BUILD = 1,
        BUILDRESOURCE = 2,
        NPC = 3,
    }

    public class InterfaceInformationUpdate : MonoBehaviour
    {
        public NpcInterface NpcInformation = new NpcInterface();
        public BuildInformationUpdate BuildInformation = new BuildInformationUpdate();

        public CurrentSelectionType SelectionType = CurrentSelectionType.NONE;

        private Worker _currentWorker;
        private BuildObject _currentBuildObject;
        private BuildResource _currentBuildResource;

        private GameControl _gameControl;

        #region Mono
        void Awake()
        {
            ResetPanelInformation();

            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
        }

        void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(UpdateInformation);
        }

        void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(UpdateInformation);
        }
        #endregion

        public void InformationUpdate(Worker worker)
        {
            //Debug.Log("Activating worker panel!");
            ResetPanelInformation();

            SelectionType = CurrentSelectionType.NPC;
            _currentWorker = worker;

            NpcInformation.Setup(_currentWorker);
        }

        public void InformationUpdate(BuildObject build)
        {
            //Debug.Log("Activating build panel!");
            ResetPanelInformation();

            SelectionType = CurrentSelectionType.BUILD;
            _currentBuildObject = build;
        }

        public void InformationUpdate(BuildResource build)
        {
            //Debug.Log("Activating build resource panel!");
            ResetPanelInformation();

            SelectionType = CurrentSelectionType.BUILDRESOURCE;
            _currentBuildResource = build;
        }

        private void UpdateInformation()
        {
            if (SelectionType == CurrentSelectionType.NONE)
                return;

            //Debug.Log("Information Update ticking.");

            switch (SelectionType)
            {
                case CurrentSelectionType.BUILD:
                    NpcInformation.DeactivePanel();
                    BuildInformation.UpdatePanel(_currentBuildObject);
                    break;
                    
                case CurrentSelectionType.BUILDRESOURCE:
                    NpcInformation.DeactivePanel();
                    BuildInformation.UpdatePanel(_currentBuildResource);
                    break;

                case CurrentSelectionType.NPC:
                    BuildInformation.DeactivePanel();
                    NpcInformation.Update();
                    break;
            }
        }
        
        public void ResetPanelInformation()
        {
            BuildInformation.DeactivePanel();
            NpcInformation.DeactivePanel();
            
            _currentWorker = null;
            _currentBuildObject = null;
            _currentBuildResource = null;

            SelectionType = CurrentSelectionType.NONE;
        }
    }   
}