using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIBuildCost : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private UIBuildCostController _uiBuildCostController;

    public GameObject BuildPanel;

    public string WoodCost;
    public string TickCost;

    void Awake()
    {
        _uiBuildCostController =
            GameObject.FindGameObjectWithTag(Helper.UiController).GetComponent<UIBuildCostController>();
    }

    
    public void OnPointerEnter(PointerEventData eventData)
    {
        for (int i = 0; i < eventData.hovered.Count; i++)
        {
            if (eventData.hovered[i].gameObject == BuildPanel)
            {
                //Debug.Log("We have the right panel!");

                _uiBuildCostController.RequestPanel(WoodCost, TickCost);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        for (int i = 0; i < eventData.hovered.Count; i++)
        {
            if (eventData.hovered[i].gameObject == BuildPanel)
            {
                //Debug.Log("We have the right panel!");

                _uiBuildCostController.HidePanel();
            }
        }        
    }
}