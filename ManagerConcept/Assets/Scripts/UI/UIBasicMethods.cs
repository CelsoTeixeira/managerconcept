using UnityEngine;
using System.Collections;

public class UIBasicMethods : MonoBehaviour
{
    public void TogglePanel(GameObject panelToToggle)
    {
        panelToToggle.SetActive(!panelToToggle.activeInHierarchy);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}