using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameLogic.Interface
{

    /// <summary>
    ///     This is used when we need to call for the UI to some
    /// some information needed about anything.
    /// </summary>
    [Serializable]
    public class GameInterfaceComponent
    {
        public GameObject Panel;
        public Text Text;

        private bool _canBeUsed = true;
        public bool CanBeUsed
        {
            get { return _canBeUsed;}
        }

        public int TimeToWait = 5;

        public Action HideCallback;

        public int _internalTimer = 0;
        private GameInterfaceControl _gameInterfaceControl;

        public void SetupControl(GameInterfaceControl control)
        {
            _gameInterfaceControl = control;
        }

        public void ChangeText(string textToChange)
        {
            //Debug.Log("Trying to change the text.");

            Text.text = textToChange;
        }

        public void ShowText()
        {
            //Debug.Log("Trying to show the text panel.");

            Text.gameObject.SetActive(true);      //Show the text
            Panel.gameObject.SetActive(true);

            _internalTimer = 0;     //Reset the current timer.

            _gameInterfaceControl.SendToEnabledList(this);

            _canBeUsed = false;
        }

        //This is called when we 
        public void HideText()
        {
            Text.gameObject.SetActive(false);
            Panel.gameObject.SetActive(false);

            _gameInterfaceControl.SendToDisabledList(this);

            _canBeUsed = true;
        }

        public void InternalUpdate()
        {
            //Debug.Log("Internal interface component update!");

            _internalTimer ++;

            if (_internalTimer >= TimeToWait)
            {
                HideText();

                if (HideCallback != null)
                {
                    HideCallback.Invoke();
                }
            }
        }
    }

}

