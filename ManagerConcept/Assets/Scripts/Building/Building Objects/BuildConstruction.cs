using System;
using UnityEngine;
using System.Collections;
using Building;

namespace Building
{
    [Serializable]
    public class BuildConstruction
    {
        private float _constructionTime = 10f;
        public float ConstructionTime
        {
            get { return _constructionTime;}
            set
            {
                if (value >= 0)
                {
                    _constructionTime = value;
                }
            }
        }

        public float WorkerModifier = 0.5f;

        private float _timer = 0f;
        private int _tickTimer = 0;

        //TODO: MAKE THE AMOUNT OF WORKERS INFLUENCE THE CURRENT BUILD RATE.        
        public void ConstructionUpdate(int currentWorkers, float time, Action callback)
        {
            if (currentWorkers == 0)
            {
                //Debug.Log("We don't have any worker on site.");
                return;
            }

            Debug.Log("Timer: " + _timer + " / " + _constructionTime);

            //_timer += time + (currentWorkers * time);
            _timer += time;

            //Debug.Log("Construction current timer: " + _timer);

            if (_timer > _constructionTime)
            {
                //Debug.Log("Construction finished!");

                //invoke callback.             
                callback.Invoke();
            }
        }

        public void ConstructionTickUpdate(int currentWorkers, Action callback)
        {
            if (currentWorkers == 0)
            {
                return;
            }

            //Debug.Log("Construction tick");

            _tickTimer ++;

            if (_tickTimer > _constructionTime)
            {
                
                callback.Invoke();          //Callback
            }
        }
    }
}