using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation
{
    [System.Serializable]
    public class ApplyGridRandomizer
    { 
        public ApplyGridRandomizer() { }

        public void PopulateWithGrass(List<TileInformation> infos)
        {
            foreach (TileInformation info in infos)
            {
                info.ChangeTileType(TileType.Grass);
                info.UpdateTileSprite();
            }
        }

        //We need a data strucute here to define values for what we want to generate.
        public void ApplyGridRandomFill(List<TileInformation> infos)
        {
            //for (int x = 0; x < infos.Count; x++)
            //{
            //    TileInformation currentInfo = infos[x];

            //    //We want to generate a random value here and change the tile type based
            //    //on this value.
            //}
        }

        public List<GameObject> RandomizerApplier(List<GameObject> objs, RandomizerCollection defRng, List<TileInformation> infos)
        {
            for (int y = 0; y < objs.Count; y++)
            {

                infos[y].ChangeTileType(TileType.Grass);
                infos[y].UpdateTileSprite();

            }
            
            return objs;

        } 
    }
}

