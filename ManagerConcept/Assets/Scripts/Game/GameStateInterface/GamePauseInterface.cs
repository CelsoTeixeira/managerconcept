using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLogic;

public class GamePauseInterface : BaseEventInterface 
{
    public override void OnEnable()
    {
        _GameControl.PauseGameEvent.Subscribe(ShowPanels);
        _GameControl.StartGameEvent.Subscribe(HidePanels);
        _GameControl.MainMenuEvent.Subscribe(HidePanels);
    }

    public override void OnDisable()
    {
        _GameControl.PauseGameEvent.UnSubscribe(ShowPanels);
        _GameControl.StartGameEvent.UnSubscribe(HidePanels);
        _GameControl.MainMenuEvent.UnSubscribe(HidePanels);
    }
}
