using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Generation;

public static class GridUtility 
{
    public static List<TileInformation> GetTileInformation(List<GameObject> tiles)
    {
        List<TileInformation> infos = new List<TileInformation>();

        for (int x = 0; x < tiles.Count; x++)
        {
            infos.Add(tiles[x].GetComponent<TileInformation>());
        }

        return infos;
    }

    public static void DestroyGridObjects(List<GameObject> tiles)
    {
        if (tiles == null || tiles.Count == 0)
            return;

        for (int x = 0; x < tiles.Count; x++)
        {
            Object.Destroy(tiles[x]);
        }
    }

    public static Vector3 GetGridCenter(GridDefinition col)
    {
        Vector3 center = new Vector3();
        center.x = col.Size_X/2;
        center.x = Mathf.Round(center.x);
        center.y = col.Size_Y / 2;
        center.y = Mathf.Round(center.y);
        center.z = 0f;

        return center;
    }
}
