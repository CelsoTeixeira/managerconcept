using UnityEngine;
using System.Collections;
using Building;
using Entity;
using Entity.Behaviour;
using Entity.WorkerNPC;
using GameLogic;
using Interface;

namespace Entity.Control
{
    public class EntitySelection : MonoBehaviour
    {
        #region Selection
        private GameObject _currentGameObjectSelection;
        private BaseEntity _currentSelectionEntity;
        private Worker _currentWorker;

        #endregion

        private GameControl _gameControl;
        private InterfaceInformationUpdate _interfaceInformationUpdate;

        #region Mono    
        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
            _interfaceInformationUpdate =
                GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<InterfaceInformationUpdate>();
        }

        //Subscribing the method on the constant loop.
        void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(SelectionUptate);
        }

        //Make sure we unsubscribe the method when we disable the object.
        void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(SelectionUptate);
        }
        #endregion
        
        private void SelectionUptate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("Mouse click");
                
                //Get selection
                if (_currentGameObjectSelection == null)
                {
                    //NOTE(Celso): Clear the current selection. ??
                    ClearCurrentSelection();

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    //First ray to NPC layer, then if its null we do a second to the Build layer.
                    
                    RaycastHit2D npcRayHit = Physics2D.Raycast(ray.origin, ray.direction, 30,
                        LayerHelper.NPCsLayerBitmask);

                    //Ray check for the NPC layer.
                    if (npcRayHit.collider != null)
                    {
                        //Debug.Log("Hitted NPC!");

                        //Get the component.
                        _currentSelectionEntity = npcRayHit.collider.gameObject.GetComponentInParent<BaseEntity>();

                        //Get the game object.
                        _currentGameObjectSelection = _currentSelectionEntity.GetGameObject();

                        //Refresh information about the worker.
                        _currentWorker = _currentGameObjectSelection.GetComponent<Worker>();
                        _currentWorker.CurrentSelection = true;

                        _interfaceInformationUpdate.InformationUpdate(_currentWorker);

                    }
                    else    //If we dont hit anything, we do on for the building layer.
                    {
                        RaycastHit2D buildRayHit = Physics2D.Raycast(ray.origin, ray.direction, 30,
                            LayerHelper.BuildsLayerBitmask);

                        if (buildRayHit.collider != null)
                        {
                            //Debug.Log("Hitted build!");

                            //Get the component
                            _currentSelectionEntity = buildRayHit.collider.gameObject.GetComponentInParent<BaseEntity>();

                            //Get the game object.
                            _currentGameObjectSelection = _currentSelectionEntity.GetGameObject();

                            //Refresh the information.
                            BuildObject buildObject = _currentGameObjectSelection.GetComponent<BuildObject>();

                            //Debug.Log("Current build: " + buildObject.BuildType.ToString(), buildObject.gameObject);

                            if (buildObject.BuildType == BuildType.ResourceGeneration)
                            {
                                BuildResource buildResource = _currentGameObjectSelection.GetComponent<BuildResource>();
                                _interfaceInformationUpdate.InformationUpdate(buildResource);
                            }
                            else
                            {
                                _interfaceInformationUpdate.InformationUpdate(buildObject);
                            }
                        }
                    }

                    Debug.DrawRay(ray.origin, ray.direction * 30f, Color.red, 5f);                    
                }
                else if (_currentGameObjectSelection != null)
                {
                    if (_currentSelectionEntity.EntityType == EntityType.NPC)
                    {
                        //If its a NPC we want to deselect.
                        ClearCurrentSelection();
                    }
                    else if (_currentSelectionEntity.EntityType == EntityType.BUILD)
                    {
                        //If its a build we dont want to deselect it.
                    }
                }
            }
            
            if (Input.GetMouseButtonDown(1))
            {
                if (_currentGameObjectSelection != null)
                {
                    if (_currentSelectionEntity.EntityType == EntityType.NPC)
                    {
                        //NOTE(Celso): Actually, we don't really want to deal with movement because we don't want 
                        //to have to deal with movement like a RTS.                        
                        IMovement move = _currentGameObjectSelection.GetComponent<IMovement>();

                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, 30);

                        if (rayHit.collider != null)
                        {
                            //Debug.Log("Trying to move!" + rayHit.collider.gameObject.transform.position, rayHit.collider.gameObject);
                            
                            if (rayHit.collider.CompareTag(Helper.BuildTag))
                            {
                                BuildObject build = rayHit.collider.gameObject.GetComponent<BuildObject>();

                                if (build != null)
                                {
                                    //Debug.Log("Trying to move to a build");

                                    if (move != null)
                                    {
                                        //TODO: Check this WorkerState
                                        //move.GoToBuild(build, WorkerState.Working);
                                    }
                                }
                            }
                            else
                            {
                                ////If we have a interface for movement.
                                if (move != null)
                                {
                                    //move.GoToEmptyTile(rayHit.collider.gameObject.transform.position);
                                }
                            }                            
                        }
                    }
                    else if (_currentSelectionEntity.EntityType == EntityType.BUILD)
                    {
                        //If its a build we want to deselect
                        ClearCurrentSelection();
                    }
                }
            }
        }

        /// <summary>
        ///     Clear the current selection, just make
        /// all the stuff we need to handle to null.
        /// </summary>
        private void ClearCurrentSelection()
        {
            if (_currentWorker != null)
            {
                _currentWorker.CurrentSelection = false;
                _currentWorker = null;
            }

            //Debug.Log("Cleaning curret selection");
            _currentGameObjectSelection = null;
            _currentSelectionEntity = null;

            //When we deselect any entity, we want to reset the UI Information
            _interfaceInformationUpdate.ResetPanelInformation();
        }
    }
}