using UnityEngine;
using System.Collections.Generic;

using GameLogic;

namespace Building
{
    /// <summary>
    ///     TODO: We need a tracking behaviour for this.
    /// </summary>

    [RequireComponent(typeof(BuildPlacement))]
    [RequireComponent(typeof(BuildValidation))]
    [RequireComponent(typeof(BuildCollection))]
    [RequireComponent(typeof(BuildTracking))]
    public class BuildingControl : MonoBehaviour
    {   
        public GameTick TickUpdate;
        public GameTick ConstantUpdate;

        public GameTick BuildUtilitiesTick;

        private UIResources _uiResources;
        private GameControl _gameControl;
        
        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
            _uiResources = GameObject.FindGameObjectWithTag(Helper.UiController).GetComponent<UIResources>();
        }

        private void BuildTickUpdate()
        {
            if (TickUpdate.Tick() != null)
            {
                //Debug.Log("Build control tick loop");

                TickUpdate.Tick().Invoke(); //Process all the generation builds

                _uiResources.ForceUpdate(); //Force the text update            
            }
            //else
            //{
            //    Debug.Log("Build Update, tick null.");
            //}
        }

        private void ConstantBuildUpdate()
        {
            if (ConstantUpdate.Tick() != null)
            {
                //Debug.Log("Build control update loop");

                ConstantUpdate.Tick().Invoke();
            }
            //else
            //{
            //    Debug.Log("Build Update, constant null.");
            //}
        }

        private void BuildUtilityUpdate()
        {
            if (BuildUtilitiesTick.Tick() != null)
            {
                BuildUtilitiesTick.Tick().Invoke();
            }
        }

        private void OnEnable()
        {
            //Debug.Log("Subscribing action to main loop");
            _gameControl.TickControl.UpdateTick.Subscribe(BuildTickUpdate);
            _gameControl.TickControl.UpdateConstantTick.Subscribe(ConstantBuildUpdate);
            _gameControl.TickControl.UpdateConstantTick.Subscribe(BuildUtilityUpdate);
        }

        private void OnDisable()
        {
            //Debug.Log("UnSubscribing action from main loop");
            _gameControl.TickControl.UpdateTick.UnSubscribe(BuildTickUpdate);
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(ConstantBuildUpdate);
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(BuildUtilityUpdate);
        }
    }
}