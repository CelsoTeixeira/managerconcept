﻿using UnityEngine;
using System.Collections;

public interface IBuild
{
    void Construct();
    void Demolish();
}