using UnityEngine;
using System;
using System.Collections.Generic;


using Entity.WorkerNPC;
using GameLogic.Interface;
using Generation.History;

namespace Building
{
    public enum BuildConstructionStatus
    {
        WaitingForWork = 0,
        Incomplete = 1,
        Complete = 2,
        OnConstruction = 3
    }

    public enum BuildName
    {
        EMPTY = 0,
        Castle = 1,
        Farm = 2,
        Woodcutter = 3,
        Water = 4,
        House = 5,
    }

    public enum BuildType
    {
        EMPTY = 0,
        Center = 1,
        House = 2,
        ResourceGeneration = 3
    }
    
    public class BuildObject : BaseEntity, IBuildActivation, IBuildWorker
    {
        public string UiRequestMessage;
        public LogBase HistoryLogBase = new LogBase();

        public BuildName BuildName = BuildName.EMPTY;
        public BuildType BuildType = BuildType.EMPTY;

        public BuildConstructionStatus ConstructionStatus = BuildConstructionStatus.Incomplete;

        public BuildCostData BuildCost = new BuildCostData();
        
        public List<Worker> WorkersOnSite = new List<Worker>();

        public BuildConstruction BuildConstruction = new BuildConstruction();

        public ParticleSystem ParticleSystem;
        public Renderer ParticleRenderer;
        public List<Material> ParticleMaterials; 

        protected BuildingControl p_BuildingControl;        
        protected BuildTracking p_BuildTracking;
        protected BuildCollection p_BuildCollection;

        protected GameInterfaceControl p_GameInterfaceControl;
        protected HistoryLogComponent p_HistoryLogComponent;


        #region Mono
        protected virtual void Awake()
        {
            EntityType = EntityType.BUILD;

            p_BuildingControl = GameObject.FindGameObjectWithTag(Helper.BuildController)
                                    .GetComponent<BuildingControl>();

            p_BuildTracking = GameObject.FindGameObjectWithTag(Helper.BuildController)
                                    .GetComponent<BuildTracking>();

            p_BuildCollection = GameObject.FindGameObjectWithTag(Helper.BuildController)
                                    .GetComponent<BuildCollection>();

            p_GameInterfaceControl = GameObject.FindGameObjectWithTag(Helper.GameController)
                                    .GetComponent<GameInterfaceControl>();

            p_HistoryLogComponent = GameObject.FindGameObjectWithTag(Helper.HistoryController)
                                    .GetComponent<HistoryLogComponent>();

            BuildConstruction.ConstructionTime = BuildCost.ConstructionTick;
        }
        
        protected virtual void Start() { }
        protected virtual void OnDisable() { }
        protected virtual void OnEnable() { }
        #endregion

        /// <summary>
        ///     We can use two types of Updates, one will be constantly,
        /// like Unity Update, the other will be called based on a tick.
        /// Every time we reach a timer this Tick will go out, calling the 
        /// TickUpdate.
        ///     We can override this on the what we need and just call the base
        /// to preserve this behaviour on every build.
        /// </summary>
        protected virtual void BuildConstantUpdate()
        {
            //ConstructionUpdate();
        }

        protected virtual void ConstructionTick()
        {
            if (ConstructionStatus == BuildConstructionStatus.Complete)
                return;

            if (WorkersOnSite.Count == 0)
            {
                ConstructionStatus = BuildConstructionStatus.WaitingForWork;
            }
            else if (WorkersOnSite.Count > 0)
            {
                ConstructionStatus = BuildConstructionStatus.OnConstruction;
            }

            if (ConstructionStatus == BuildConstructionStatus.OnConstruction)
            {
                BuildConstruction.ConstructionTickUpdate(WorkersOnSite.Count, ConstructionFinished);
            }

            BuildParticleControl();
        }

        protected virtual void BuildTickUpdate()
        {
            ConstructionTick();
        }

        protected virtual void BuildParticleControl()
        {
            if (ParticleSystem == null)
                return;

            if (ConstructionStatus == BuildConstructionStatus.OnConstruction)
            {
                ParticleRenderer.material = ParticleMaterials[0];
                if (!ParticleSystem.isPlaying)
                {
                    ParticleSystem.Play();
                }
            }
            else if (ConstructionStatus != BuildConstructionStatus.OnConstruction)
            {
                ParticleSystem.Stop();
            }
        }

        /// <summary>
        ///     This is called when the construction hits 100% construction.
        /// </summary>
        protected virtual void ConstructionFinished()
        {
            
            //Debug.Log("Construction done!");
            //Changing our status.
            ConstructionStatus = BuildConstructionStatus.Complete;

            p_BuildTracking.BuildComplete(this);

            p_GameInterfaceControl.RequestNewInterfaceUpdate(this.UiRequestMessage);        //This is requesting for a new panel to send a message.

            p_HistoryLogComponent.AddLog(HistoryLogBase);

            //Removing all the workers from site.
            RemoveAllWorkers();
        }

        #region Particles Controller

        private void EnableParticles()
        {
            if (!ParticleSystem.isPlaying)
            {
                ParticleSystem.Play();
            }
        }

        private void DisableParticles()
        {
            if (ParticleSystem.isPlaying)
            {
                ParticleSystem.Stop();
            }
        }

        #endregion

        #region IBuildWorker implementation
        /*
        NOTE(Celso): This is what we will use to add and 
        remove worker from the build. Implementing Interfaces
        will make the logic easy to understand and to implement
        on other types of builds.

        This is used by the workers itself, if we need to do stuff from
        the UI we need to use other interface not yet implemented, check
        BuildResource for more info.
        */
        public virtual void AddWorker(Worker worker)
        {
            //Debug.Log("Adding a new worker to the build.");
            if (!WorkersOnSite.Contains(worker))
            {
                WorkersOnSite.Add(worker);

                if (ConstructionStatus != BuildConstructionStatus.Complete)
                {
                    ConstructionTick();
                }
            }
            
        }

        public virtual void RemoveWorker(Worker worker)
        {
            //Debug.Log("Removing a worker to the build.");
            WorkersOnSite.Remove(worker);

            //Do what we need when we leave the build.
            Worker currentWorker = worker;

            currentWorker.LeaveBuild();
            currentWorker.DeleteJob();                                   
        }
        
        public virtual void RemoveAllWorkers()
        {
            //Debug.Log("Removing all the workers on site.");

            //foreach (Worker worker in WorkersOnSite)
            //{
            //    RemoveWorker(worker);
            //}

            for (int i = 0; i < WorkersOnSite.Count; i++)
            {
                RemoveWorker(WorkersOnSite[i]);
            }
        }
        #endregion

        #region Activation

        /// <summary>
        ///     This is what we use to subscribe the methods we 
        /// want on the build control update. We just need to 
        /// override this on the children class we want functionality.
        /// </summary>
        protected virtual void ActiveBuild()
        {
            p_BuildTracking.AddNewBuild(this);
        }

        //This is the interface method.
        public void Active()
        {
            ActiveBuild();
        }
        #endregion        
    }
}