using System;
using UnityEngine;
using System.Collections;

namespace GameLogic
{
    /// <summary>
    ///     We're doing one tick per minute on the tick update.
    /// </summary>

    public enum GameTimeScale
    {
        Normal = 1,
        Fast = 2,
        DoubleFast = 3,
        TripleFast = 4,
    }

    [Serializable]
    public class GameTickControl
    {
        public GameTick UpdateTick = new GameTick();                //This need to be update from time to time.
        public GameTick UpdateConstantTick = new GameTick();        //This need to be update constant.

        public GameTick PauseTick = new GameTick();                 //This need to be update when our game is paused.
        public GameTick PauseConstantTick = new GameTick();         //This need to be update constant.


        private static int _gameTick = 60;                //This is the current time before we update.
        private static int _gameTickInternal = 60;

        public int CurrentGameTick { get { return _gameTickInternal; } }
        public float CurrentGameTickTimer { get { return _gameTimer; } }

        private GameTimeScale _currentTimeScale = GameTimeScale.Normal;
        public GameTimeScale CurrentTimeScale { get { return _currentTimeScale; } }
        
        private float _gameTimer = 0;

        public GameTickControl()
        {
            _gameTickInternal = _gameTick;
        }

        public void TickControlUpdate(GameState state)
        {
            //Debug.Log("Game Timer: " + _gameTimer);
            //Debug.Log("Time Scale: " + _currentTimeScale);
            
            switch (state)
            {
                case GameState.Update:

                    ConstantGameTickUpdate();
                    GameTickUpdate();
                    //Debug.Log("Update");
                    break;

                case GameState.Pause:
                    GamePausedUpdate();
                    break;
            }
        }

        public void ChangeTimeScale(GameTimeScale scaleToChange)
        {
            _currentTimeScale = scaleToChange;

            switch (scaleToChange)
            {
                case GameTimeScale.Normal:
                    _gameTickInternal = _gameTick;
                    break;

                case GameTimeScale.Fast:
                    _gameTickInternal = _gameTick/2;
                    break;

                case GameTimeScale.DoubleFast:
                    _gameTickInternal = _gameTick/6;
                    break;

                case GameTimeScale.TripleFast:
                    _gameTickInternal = _gameTick/10;
                    break;
            }   
        }
           
        private void GameTickUpdate()
        {    
            /*Debug.Log("Update tick: " + Mathf.Round(_gameTimer))*/
            
            _gameTimer += Time.deltaTime;

            //This is a timed tick update
            if (_gameTimer >= _gameTickInternal)
            {
                //Debug.Log("Invoking tick" + Time.time);
                //Debug.Log("Tick invoke!");

                if (UpdateTick.Tick() != null)
                {
                    UpdateTick.Tick().Invoke();
                }

                //Debug.Log("Update tick: " + Mathf.Round(_gameTimer));

                _gameTimer = 0;

                //Debug.Log("Game Tick Update. " + _gameTickCounter);
            }
        }

        private void ConstantGameTickUpdate()
        {
            //This is a constant tick update.
            if (UpdateConstantTick.Tick() != null)
            {
                //Debug.Log("Constant update!");

                UpdateConstantTick.Tick().Invoke();
            }
        }

        private void GamePausedUpdate()
        {
            //Debug.Log("Pause tick");

            if (PauseTick.Tick() != null)
            {
                PauseTick.Tick().Invoke();
            }
        }
    }

}