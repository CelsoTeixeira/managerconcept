using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CameraController;
using Generation;
using Generation.Biomes;
using UnityEngine.UI;

public class GridControllerVisualization : MonoBehaviour
{
    public Slider TimeSlider;

    public CameraMovement CameraMov;

    public GridCollection Collection;
    public FlorestBiomeVisualization FlorestBiome_;

    public GridDefinition GridDefinition = new GridDefinition();
    public CreateGridPositions CreateGridPositions = new CreateGridPositions();
    public CreateGridVisualization CreateGridVisualization = new CreateGridVisualization();

    public ApplyGridRandomizer Randomizer = new ApplyGridRandomizer();                          //This is a simply randomizer to be applied on the grid.


    public List<GameObject> GridTiles = new List<GameObject>();
    public List<Vector3> GridPositions = new List<Vector3>(); 
    public List<TileInformation> GridTileInfo = new List<TileInformation>();

    public void GenerateNewPatternGrid()
    {
        GenerateNewGrid(GenerationMode.Pattern);
    }

    public void GenerateNewRandomGrid()
    {
        GenerateNewGrid(GenerationMode.Random);
    }

    public void CombineGeneration()
    {
        GridDefinition = Collection.GetGridDefinition();

        ClearCurrentGrid();

        float x = GridDefinition.Size_X / 2;
        float z = GridDefinition.Size_Y / 2;

        CameraMov.ForceCameraPosition(new Vector2(x, z));

        GridPositions = CreateGridPositions.GeneratePositions(GridDefinition);

        GridTiles = CreateGridVisualization.CreateGridTiles(GridPositions, GridDefinition);

        GridTileInfo = GetTileInformation(GridTiles);

        Randomizer.PopulateWithGrass(GridTileInfo);

        Collection.SetupCollectionData(GridPositions, GridTileInfo, GridDefinition);

        FlorestBiome_.Collection = Collection;

        float WaitTime = TimeSlider.value;

        StartCoroutine(FlorestBiome_.GenerateVisualizeFlorest(WaitTime, BiomeSize.Small, GenerationMode.Pattern));
        StartCoroutine(FlorestBiome_.GenerateVisualizeFlorest(WaitTime, BiomeSize.Small, GenerationMode.Random));
        StartCoroutine(FlorestBiome_.GenerateVisualizeFlorest(WaitTime, BiomeSize.Medium, GenerationMode.Random));
    }

    public void GenerateNewGrid(GenerationMode mode)
    {
        GridDefinition = Collection.GetGridDefinition();

        ClearCurrentGrid();

        float x = GridDefinition.Size_X/2;
        float z = GridDefinition.Size_Y/2;

        CameraMov.ForceCameraPosition(new Vector2(x, z));

        GridPositions = CreateGridPositions.GeneratePositions(GridDefinition);

        GridTiles = CreateGridVisualization.CreateGridTiles(GridPositions, GridDefinition);

        GridTileInfo = GetTileInformation(GridTiles);

        Randomizer.PopulateWithGrass(GridTileInfo);
        
        Collection.SetupCollectionData(GridPositions, GridTileInfo, GridDefinition);

        FlorestBiome_.Collection = Collection;

        float WaitTime = TimeSlider.value;

        StartCoroutine(FlorestBiome_.GenerateVisualizeFlorest(WaitTime, BiomeSize.Small, mode));
    }

    private List<TileInformation> GetTileInformation(List<GameObject> tiles)
    {
        List<TileInformation> infos = new List<TileInformation>(tiles.Count);

        for (int x = 0; x < tiles.Count; x++)
        {
            infos.Add(tiles[x].GetComponent<TileInformation>());
        }

        return infos;
    }

    public void ClearCurrentGrid()
    {
        if (GridTiles == null || GridTiles.Count == 0)
            return;

        //Debug.Log("Attempting to clear the current list of tiles.");

        foreach (GameObject gridTile in GridTiles)
        {
            Destroy(gridTile);
        }

        GridTiles.Clear();

        //Debug.Log("List clear!");    
    }
}
