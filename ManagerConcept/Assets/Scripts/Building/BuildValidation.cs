using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Building
{
    public class BuildValidation : MonoBehaviour
    {
        private GridCollection _gridCollection;
        
        void Awake()
        {
            _gridCollection = GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<GridCollection>();            
        }

        public bool BuildPositionValidationRayCheck(Vector3 posValidation)
        {
            TileInformation tileInfo = _gridCollection.GetValueOnCollection(posValidation);

            //If we have a different tile from grass or dirt we don't build there. (Mountain || Forest)
            if (tileInfo.TileDefinition.Type != TileType.Grass &&
                tileInfo.TileDefinition.Type != TileType.Dirt)
            {
                return false;
            }

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D rayHit = Physics2D.Raycast(ray.origin, ray.direction, 30, LayerHelper.BuildsLayerBitmask);
            
            if (rayHit.collider != null)
            {
                //Debug.Log("We hitted a object.", rayHit.collider.gameObject);
                return false;
            }
            //else
            //{
            //    Debug.Log("We didn't hit a object.");
            //}
            
            return true;
        }
    }
}
