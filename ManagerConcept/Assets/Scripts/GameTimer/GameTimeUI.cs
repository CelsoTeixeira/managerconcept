using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameLogic
{
    public class GameTimeUI : MonoBehaviour
    {
        public Image TickSlider;
        public Text SeasonText;
        public Text SpeedText;

        private GameControl _gameControl;
        
        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();        
        }

        void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(GameTimeUpdate);
        }

        void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(GameTimeUpdate);
        }

        void GameTimeUpdate()
        {
            TickSlider.fillAmount = ((_gameControl.TickControl.CurrentGameTickTimer * 100 ) / _gameControl.TickControl.CurrentGameTick)/ 100;

            //float x = ((_gameControl.TickControl.CurrentGameTickTimer * 100) / _gameControl.TickControl.CurrentGameTick) / 100;
            //Debug.Log(x);
        }

        public void UpdateSeasonTextManually(int x)
        {
            SeasonText.text = x.ToString();
        }

        public void PauseTimeScale()
        {
            _gameControl.ChangeGameState(GameState.Pause);
        }

        public void ContinueTimeScale()
        {
            _gameControl.ChangeGameState(GameState.Update);
        }

        public void IncreaseTimeScale()
        {
            switch (_gameControl.TickControl.CurrentTimeScale)
            {
                case GameTimeScale.Normal:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.Fast);
                    break;
                    
                case GameTimeScale.Fast:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.DoubleFast);
                    break;

                case GameTimeScale.DoubleFast:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.TripleFast);
                    break;

                case GameTimeScale.TripleFast:
                    Debug.Log("Already max speed");
                    break;                
            }

            TimerTextUpdate();
        }

        public void DecreaseTimer()
        {
            switch (_gameControl.TickControl.CurrentTimeScale)
            {
                case GameTimeScale.Normal:
                    Debug.Log("Already min speed");
                    break;

                case GameTimeScale.Fast:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.Normal);
                    break;

                case GameTimeScale.DoubleFast:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.Fast);
                    break;

                case GameTimeScale.TripleFast:
                    _gameControl.TickControl.ChangeTimeScale(GameTimeScale.DoubleFast);
                    break;
            }

            TimerTextUpdate();
        }

        private void TimerTextUpdate()
        {
            switch (_gameControl.TickControl.CurrentTimeScale)
            {
                case GameTimeScale.Normal:
                    SpeedText.text = "x1";
                    break;

                case GameTimeScale.Fast:
                    SpeedText.text = "x2";
                    break;

                case GameTimeScale.DoubleFast:
                    SpeedText.text = "x6";
                    break;

                case GameTimeScale.TripleFast:
                    SpeedText.text = "x10";
                    break;
            }
        }     
    }
}