using UnityEngine;
using System;
using System.Collections.Generic;

using Generation;

//Simply struct to save the data we need for the map.
[Serializable]
public struct MapDataInfo
{
    public List<Vector3> MapPositions;
    public List<TileDefinition> MapTileDefinitions;
    public GridDefinition MapGridDefinition;
}

public class GridCollection : MonoBehaviour
{
    /// <summary>
    ///     We use the Dictionary to get information based on where we are on the map, we probably wont use this 
    /// on this way because we are decoupling the Build and the Tile.
    /// </summary>
    private Dictionary<Vector3, TileInformation> Collection = new Dictionary<Vector3, TileInformation>();
    
    private MapDataInfo MapInfo = new MapDataInfo();
    
    //Populate our collection based on Position and TileINformation
    private void PopulateCollection(List<Vector3> key, List<TileInformation> value)
    {
        Collection.Clear();

        for (int x = 0; x < value.Count; x++)
        {
            Collection.Add(key[x], value[x]);
        }
    }

    public TileInformation GetValueOnCollection(Vector3 key)
    {
        if (Collection.ContainsKey(key))
        {
            return Collection[key];
        }
        else
        {
            return null;
        }
    }

    //Save the data we need to serialize.
    public void SetupCollectionData(List<Vector3> pos, List<TileInformation> infos, GridDefinition def)
    {
        List<TileDefinition> defs = new List<TileDefinition>();

        for (int x = 0; x < infos.Count; x++)
        {
            defs.Add(infos[x].TileDefinition);
        }
        
        MapInfo.MapPositions = pos;
        MapInfo.MapTileDefinitions = defs;
        MapInfo.MapGridDefinition = def;
        
        Collection.Clear();
            
        PopulateCollection(MapInfo.MapPositions, infos);
    }

    public void UpdateGridDefinition(GridDefinition def)
    {
        MapInfo.MapGridDefinition = def;
    }

    public MapDataInfo GetMapDataInfo()
    {
        return MapInfo;
    }

    public GridDefinition GetGridDefinition()
    {
        return MapInfo.MapGridDefinition;
    }
}