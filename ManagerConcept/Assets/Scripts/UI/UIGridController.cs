using UnityEngine;
using UnityEngine.UI;

using Generation;

public class UIGridController : MonoBehaviour
{
    public bool Working = false;

    public Slider Slider_X;
    public Slider Slider_Y;

    public Text Text_Current_X;
    public Text Text_Current_Y;

    public Slider Slider_TileSize_X;
    public Slider Slider_TileSize_Y;

    public Text Text_Current_TileSize_X;
    public Text Text_Current_TileSize_Y;

    public Text Text_Number_Tiles;

    //public GameObject AdvancedGenerationDefinitionPanel;

    private GridController _gridController;
    private GridDefinition _gridDefinition = new GridDefinition();
    
    void Awake()
    {
        _gridController = GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<GridController>();
       
        //AdvancedGenerationDefinitionPanel = GameObject.FindGameObjectWithTag(Helper.AdvancedGeneratorPanel);
    }

    //NOTE(Celso): We can make this be a struct, we're going to make this go to our default struct.
    void Start()
    {
        if (!Working)
            return;

        Slider_X.maxValue = _gridController.GridDefinition.MaxSize_X;
        Slider_Y.maxValue = _gridController.GridDefinition.MaxSize_Y;

        Slider_X.value = _gridController.GridDefinition.Size_X;
        Slider_Y.value = _gridController.GridDefinition.Size_Y;

        Slider_TileSize_X.maxValue = 4f;
        Slider_TileSize_Y.maxValue = 4f;

        Slider_TileSize_X.minValue = 1f;
        Slider_TileSize_Y.minValue = 1f;

        //Text_Number_Tiles.text = _gridController.NumberOfTiles.ToString();
    }

    void ControllerUpdate()
    {
        if (!Working)
            return;

        //NOTE(Celso): We could change this to be input field maybe.
        Text_Current_X.text = "Size X: " + Slider_X.value.ToString();
        Text_Current_Y.text = "Size Y: " + Slider_Y.value.ToString();

        Text_Current_TileSize_X.text = "Tile Size X: " + Slider_TileSize_X.value.ToString();
        Text_Current_TileSize_Y.text = "Tile Size Y: " + Slider_TileSize_Y.value.ToString();

        //Text_Number_Tiles.text = "Number of Tiles: " + _gridController.NumberOfTiles.ToString();
    }

    public void UpdateGridDefinitionValues()
    {
        _gridDefinition.Size_X = (int) Slider_X.value;
        _gridDefinition.Size_Y = (int) Slider_Y.value;

        _gridDefinition.TileSize_X = (int) Slider_TileSize_X.value;
        _gridDefinition.TileSize_Y = (int) Slider_TileSize_Y.value;

        _gridController.GridDefinition = _gridDefinition;

        _gridController.ForceCollectionUpdate(_gridDefinition);

        //Debug.Log("NEW GRID DEFINITION!");
        //Debug.Log("Grid Definition - Size X: " + _gridDefinition.Size_X);
        //Debug.Log("Grid Definition - Size Y: " + _gridDefinition.Size_Y);
        //Debug.Log("Grid Definition - TileSize X: " + _gridDefinition.TileSize_X);
        //Debug.Log("Grid Definition - TileSize Y: " + _gridDefinition.TileSize_Y);
    }

    public void GenerateNewGrid()
    {
        //Debug.Log("Generating a new grid.");
        //_gridController.GenerateNewGrid();
    }

    //public void ApplyNewRandomizer()
    //{
    //    //Debug.Log("Applying a new randomizer on the existing grid.");
    //    _gridController.ApplyGridRandomizer();
    //}

    public void ClearGrid()
    {
        //Debug.Log("CLearing the current grid.");
        _gridController.ClearCurrentGrid();
    }
    
}