* This is a study for my "TCC", this is the final project for UNI here in Brazil, the project is about procedural content generation for games and how we can minimize the bad aspect of it when we're talking about player experience and with this, how we can make the procedural heneration have a good impact on the flow.

*The main idea here its to manage a kingdow. You don't macro control the people, you just do high level orders, like you want to have one worker doing farm work, but the worker will have its own necessites, if he wants to eat, he will leave the work place and go eat.

*The combat will be high level too, you will not have directly control.

*There is events that can happen on the game. Its random generate, but I will have it on a Wiki on the game so how to trigger is know.

*There is a initial development on the History Generator, the basic idea here it's to have the events generate a log and create a history based on this log.

I tried to move logic away from Update and to group blocks of same logic, basically we have a lot of delegates and actions calling what we need and everything we don't need to constant simulate we're not simulating. With this I have stress tested the game on my Laptop and got like 300 workers without any drop on FPS.
On the graph side, we have everything on spritesheets and I try to reuse the maximun amount of assets. With this, the map its like 1 ~ 3 draw calls, depending on the size. The game running with everything its getting something like 12 draw calls, and I think I can work a little more on the UI assets and the Sprite Profiller and check if I can save more draw calls there.

* You can contact me on: celso6465@terra.com.br