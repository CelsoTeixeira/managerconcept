using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Generation;
using Generation.Biomes;

namespace Generation.Biomes
{
    public class ForestBiome
    {
        public List<TileInformation> GenerateForest(EpicenterData epicenterData, GridCollection col)
        {
            //Debug.Log("Epicenter center: " + epicenterData.Position);
            //Debug.Log("Epicenter size: " + epicenterData.Size);

            int amountToGenerate = BiomeUtility.GetIterationByBiomeSize(epicenterData.Size);
            
            List<Vector3> tilePos = new List<Vector3>();
            List<TileInformation> tileUsed = new List<TileInformation>();
            
            tilePos.Add(epicenterData.Position);

            float influenceArea = epicenterData.InfluenceArea - (epicenterData.InfluenceArea / 2);
            //Debug.Log("Epicenter / 2: " + influenceArea);

            int iterationCounter = 0;

            do
            {
                Vector3 currentPos = new Vector3();

                currentPos = tilePos[0];
                tilePos.RemoveAt(0);
                
                TileInformation currentInfo = col.GetValueOnCollection(currentPos);

                //If we dont have used the tile yet.
                if (!tileUsed.Contains(currentInfo))
                {
                    //Mountain has priority over forest.
                    if (currentInfo.GetTileType() != TileType.Mountain)
                    {
                        float distance = Vector3.Distance(epicenterData.Position, currentPos);
                        
                        float influenceDistance = Random.Range(0, (epicenterData.InfluenceArea - influenceArea));
                        influenceDistance = influenceDistance + influenceArea;

                        //Debug.Log("New algo based on iterations: " + (0 + (BiomeUtility.GetIterationByBiomeSize(epicenterData.Size) / amountToGenerate)));

                        //Debug.Log("influenceDistance: " + influenceDistance);
                        //Debug.Log("New influence area: " + (epicenterData.InfluenceArea - (influenceDistance * 1.5f)).ToString());

                        if (distance < (epicenterData.InfluenceArea - influenceDistance))
                        {
                            //Debug.Log("Inside the influence area.");
                            currentInfo.ChangeTileType(TileType.ForestHighDensity);
                            //currentInfo.TileDefinition.MainType = MainType.Floresta;
                            //currentInfo.TileDefinition.DensityType = DensityType.Mid;
                            //Debug.Log("Change succes.");
                        }
                        else
                        {
                            float rng = Random.Range(0, 11);

                            //Debug.Log("We are outside the infleunce area, we're doing a RNG here. " + rng);

                            if (rng <= 5)
                            {
                                currentInfo.ChangeTileType(TileType.ForestHighDensity);
                                //currentInfo.TileDefinition.MainType = MainType.Floresta;
                                //currentInfo.TileDefinition.DensityType = DensityType.Mid;
                                //Debug.Log("Change succes from rng.");
                            }
                        }

                        iterationCounter += 1;
                        //Debug.Log("Decreasing generation iteration.");
                    }

                    tileUsed.Add(currentInfo);                    
                }
                
                List<Vector3> nearTiles = BiomeUtility.GetAdjacentTileVector(currentPos, col);

                if (nearTiles != null)
                {
                    for (int i = 0; i < nearTiles.Count; i++)
                    {
                        if (!tilePos.Contains(nearTiles[i]))
                        {
                            tilePos.Add(nearTiles[i]);
                        }
                    }
                }

            } while (iterationCounter < amountToGenerate);

            for (int i = 0; i < tileUsed.Count; i++)
            {
                if (tileUsed[i].TileDefinition.Type == TileType.Grass ||
                    tileUsed[i].TileDefinition.Type == TileType.Mountain)
                {
                    tileUsed.RemoveAt(i);

                    i = 0;
                }
            }

            return tileUsed;
        }

        public void GenerateForestWithRandomEpicenter(EpicenterData data, GridCollection col, GenerationMode mode)
        {
            int amountToGenerate = BiomeUtility.GetIterationByBiomeSize(data.Size);
            
            Vector3 epicenter = BiomeUtility.GetRandomEpicenter(col.GetGridDefinition());

            List<Vector3> tilePos = new List<Vector3>();
            List<TileInformation> tileUsed = new List<TileInformation>();

            tilePos.Add(epicenter);

            do
            {
                Vector3 currentPos = new Vector3();

                switch (mode)
                {
                    case GenerationMode.Pattern:
                        ////Get the first tile on the list.
                        currentPos = tilePos[0];
                        ////Remove it from the list.
                        tilePos.RemoveAt(0);
                        break;

                    case GenerationMode.Random:
                        //Get a random position.
                        int rngPosition = Random.Range(0, tilePos.Count);
                        //Debug.Log(rngPosition);

                        currentPos = tilePos[rngPosition];
                        tilePos.RemoveAt(rngPosition);
                        break;
                }

                //Get the TileInformation
                TileInformation currentInfo = col.GetValueOnCollection(currentPos);
                
                
                if (!tileUsed.Contains(currentInfo))
                {
                    TileType currentType = currentInfo.GetTileType();

                    //Mountains have priority
                    if (currentType != TileType.Mountain)
                    {
                        if (currentType != TileType.ForestHighDensity)
                        {
                            //Change and update the sprite
                            currentInfo.ChangeTileType(TileType.ForestHighDensity);
                        }
                    }

                    amountToGenerate -= 1;
                    tileUsed.Add(currentInfo);
                }
                
                //Debug.Log("ForestHighDensity: " + internalCounter, currentInfo.gameObject);

                //Get near tiles.
                List<Vector3> nearTiles = BiomeUtility.GetAdjacentTileVector(currentPos, col);

                //Populate list for each Vector3 we have inside nearTiles.
                foreach (Vector3 nearTile in nearTiles)
                {
                    if (!tilePos.Contains(nearTile))
                    {
                        tilePos.Add(nearTile);
                    }
                }

            } while (amountToGenerate != 0);
        }

        public void UpdateForestTileDensity(List<TileInformation> tiles, GridCollection col)
        {
            for (int i = 0; i < tiles.Count; i++)
            {
                List<TileInformation> nearTiles = BiomeUtility.GetAdjacentTileInfoByInfo(tiles[i], col);

                if (nearTiles != null)
                {
                    int numberOfAdjacentForest = 0;

                    for (int j = 0; j < nearTiles.Count; j++)
                    {
                        if (nearTiles[j].TileDefinition.Type == TileType.ForestHighDensity ||
                            nearTiles[j].TileDefinition.Type == TileType.ForestMidDensity ||
                            nearTiles[j].TileDefinition.Type == TileType.ForestLowDensity)
                        {
                            numberOfAdjacentForest ++;
                        }
                    }

                    switch (numberOfAdjacentForest)
                    {
                        case 0:
                            tiles[i].ChangeTileType(TileType.ForestLowDensity);
                            //tiles[i].TileDefinition.DensityType = DensityType.Low;
                            //tiles[i].PostProcess();
                            break;

                        case 1:
                            tiles[i].ChangeTileType(TileType.ForestLowDensity);
                            //tiles[i].TileDefinition.DensityType = DensityType.Low;
                            //tiles[i].PostProcess();
                            break;

                        case 2:
                            tiles[i].ChangeTileType(TileType.ForestLowDensity);
                            //tiles[i].TileDefinition.DensityType = DensityType.Mid;
                            //tiles[i].PostProcess();
                            break;

                        case 3:
                            tiles[i].ChangeTileType(TileType.ForestMidDensity);
                            //tiles[i].TileDefinition.DensityType = DensityType.Mid;
                            //tiles[i].PostProcess();
                            break;

                        case 4:
                            tiles[i].ChangeTileType(TileType.ForestHighDensity);
                            //tiles[i].TileDefinition.DensityType = DensityType.High;
                            //tiles[i].PostProcess();
                            break;
                    }
                    
                    ////Post process.
                    //tiles[i].PostProcess();
                }
            }
        }
    }
}