using System;
using UnityEngine;
using System.Collections;
using Entity.WorkerNPC;
using GameLogic;

namespace Entity.Behaviour.Status
{
    public enum StatusNecessity
    {
        None = 0,
        Low = 1,
        Med = 2,
        High = 3,
        Penalty = 4,       //If we have anythign here we want to apply penalties.     
    }

    [Serializable]
    public class StatusBase : IStatus
    {
        public ResourceType ResourceType;
        public StatusNecessity Necessity;
        
        public float Modifier;

        public float _rate;
        public float Rate
        {
            get { return _rate; }
        }
        
        //TODO: Turn this into a struct.
        public float MaxRate = 100;
        public float MinRate = 0;
        
        public float DecayRate = 1;
        

        public float MinimunRateAfterConsume = 70f;
        public float ConsumeRate = 1;        
        protected float _amountToConsume;

        public bool Consuming = false;

        
        public bool NotEnoughResources = false;
        public int MaxTicksWithoutResources = 30;
        
        protected int _currentTickWithoutResource = 0;
        public int CurrentTickWithoutResources { get { return _currentTickWithoutResource; } }

        protected int _currentTickTryingToConsume = 0;                                          //This is a simple counter to wait a time before the worker tries to consume again.
        public int CurrentTickTryingToConsume { get { return _currentTickTryingToConsume; } }

        public BehaviorUpdate BehaviorUpdate;

        protected ResourceContainer _resourceContainer;
        
        public GameEvent FinishConsumeCallback = new GameEvent();
        public GameEvent FailConsumeCallback = new GameEvent();


        public virtual void Setup(BehaviorUpdate update, ResourceContainer container)
        {
            this.BehaviorUpdate = update;
            this._rate = MaxRate;
            this._resourceContainer = container;
        }

        //This is running on Tick Update
        public virtual void StatusTickUpdate()
        {
            ControlRateTick();
            ControlConsumeRateTick();
            ControlNecessity();

            ControlDecayTick();
        }

        //This will be on Tick control.
        //This tick should run on something like 1 seconds.
        protected virtual void ControlRateTick()
        {
            //If we don't consume we just decay the rate.
            if (!Consuming)     
            {
                _rate -= DecayRate*Modifier;

                _rate = Mathf.Clamp(_rate, MinRate, MaxRate);
            }
        }

        //NOTE(Celso): We can change this to be inside some low tick update
        //We can can remove the consumeTimer if we put this inside a lower tick call.
        protected virtual void ControlConsumeRateTick()
        {
            if (Consuming)
            {
                //If we have consumed enough, we leave.
                if (Rate > _amountToConsume)
                {
                    Consuming = false;
                    this.BehaviorUpdate.ChangeState(WorkerState.Wanderer);
                    
                    FinishConsumeCallback.Broadcast();

                    return;
                }
                
                float amountToRecover = ConsumeRate * Modifier;     //This should be flat.

                //If we have enough resource to get from the container
                if (_resourceContainer.GetAmount(ResourceType, amountToRecover))
                {
                    _rate += amountToRecover;   //Increase our rate.

                    _rate = Mathf.Clamp(_rate, MinRate, MaxRate);

                    NotEnoughResources = false;     //If we just consumed, we update this.  
                    _currentTickWithoutResource = 0;    //Reset this counter.
                
                }
                else
                {
                    //If we don't have enough resource to get from container:
                    //We want to wait for some ticks, and after a limit we want to kill the npc.
                    NotEnoughResources = true;                 

                    Necessity = StatusNecessity.Penalty;

                    this.BehaviorUpdate.ChangeState(WorkerState.Wanderer);  //Send the worker back to waht hes doing.
                    
                }
            }
        }

        protected virtual void ControlDecayTick()
        {
            //Debug.Log("Trying to consume counter: " + _currentTickTryingToConsume);

            if (NotEnoughResources)
            {
                _currentTickWithoutResource++;  //Increase the counter
                _currentTickTryingToConsume++;

                if (_currentTickWithoutResource > MaxTicksWithoutResources)
                {
                    //We ded
                    Debug.Log("NPC Dead!");
                }
            }
        }
        
        //TODO: This should run on the tick too.
        protected virtual void ControlNecessity()
        {
            if (Rate >= 60 && Rate <= 100)
            {
                Necessity = StatusNecessity.Low;
            }
            else if (Rate >= 30 && Rate < 60)
            {
                Necessity = StatusNecessity.Med;
            }
            else if (Rate > 5 && Rate < 29)
            {
                Necessity = StatusNecessity.High;
            }
            else if (Rate <= 0 && Rate <= 5)
            {
                Necessity = StatusNecessity.Penalty;
            }
        }

        public void ResetCounterTryingToConsume()
        {
            _currentTickTryingToConsume = 0;
        }

        #region IStatus implementation
        public void ChangeModifier(int i)
        {
            Modifier = i;
        }

        public virtual bool Consume()
        {
            if (Consuming)
                return false;

            _amountToConsume = UnityEngine.Random.Range(MinimunRateAfterConsume, MaxRate);

            Consuming = true;

            return true;
        }
        #endregion

        public void StopConsume()
        {
            Consuming = false;
        }
    }
}