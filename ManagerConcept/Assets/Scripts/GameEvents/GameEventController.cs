using UnityEngine;
using System.Collections;

namespace GameLogic.Events
{

    [RequireComponent(typeof(ResourceEvent))]
    [RequireComponent(typeof(PopulationEvent))]
    public class GameEventController : MonoBehaviour
    {
        private ResourceEvent _resourceEvent;
        private PopulationEvent _populationEvent;

        private GameControl _gameControl;

        void Awake()
        {
            _resourceEvent = GetComponent<ResourceEvent>();
            _populationEvent = GetComponent<PopulationEvent>();
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
        }

        void OnEnable()
        {
            _gameControl.TickControl.UpdateTick.Subscribe(GameEventTick);
        }

        void OnDisable()
        {
            _gameControl.TickControl.UpdateTick.UnSubscribe(GameEventTick);
        }

        private void GameEventTick()
        {
            //Debug.Log("Calling the events update!");

            _resourceEvent.BaseUpdate();
            _populationEvent.BaseUpdate();
        }
    }
}