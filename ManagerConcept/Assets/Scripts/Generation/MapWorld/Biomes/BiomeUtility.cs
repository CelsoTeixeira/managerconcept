using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Biomes
{
    public enum Directions
    {
        N,
        L, 
        S, 
        O
    }

    public static class BiomeUtility
    {
        public static List<Vector3> FourDirections = new List<Vector3>()
        {
            new Vector3(0, 1, 0),  //N
            new Vector3(1, 0, 0),  //E
            new Vector3(0, -1, 0), //S
            new Vector3(-1, 0, 0)  //W
        };

        public static List<Vector3> EightDirections = new List<Vector3>()
        {
            new Vector3(0, 1, 0),       //N
            new Vector3(1, 1, 0),       //NE

            new Vector3(1, 0, 0),       //E
            new Vector3(1, -1, 0),      //SE

            new Vector3(0, -1, 0),      //S
            new Vector3(-1, -1, 0),     //SW

            new Vector3(-1, 0, 0),      //W
            new Vector3(-1, 1, 0)       //NW      
        }; 

        public static Vector3 GetRandomEpicenter(GridDefinition def)
        {
            Vector3 newEpicenter = new Vector3();
            newEpicenter.x = Random.Range(0, def.Size_X);
            newEpicenter.y = Random.Range(0, def.Size_Y);
            newEpicenter.z = 0;

            return newEpicenter;
        }
        
        //This is what we're using to based our distance.
        public static int GetBiomeInfluenceArea(BiomeSize size)
        {
            switch (size)
            {
                case BiomeSize.Small:
                    return 4;

                case BiomeSize.Medium:
                    return 8;

                case BiomeSize.Large:
                    return 12;
              
                default:
                    return 4;
            }
        }

        public static int GetIterationByBiomeSize(BiomeSize size)
        {
            switch (size)
            {
                case BiomeSize.Small:
                    return 25;

                case BiomeSize.Medium:
                    return 50;

                case BiomeSize.Large:
                    return 85;

                default:
                    return 4;
            }
        }

        public static bool CheckAdjacentTile(Directions d, Vector3 pos, GridCollection col)
        {
            Vector3 direction = new Vector3();

            switch (d)
            {
                case Directions.N:
                    direction = FourDirections[0];
                    break;

                case Directions.L:
                    direction = FourDirections[1];
                    break;

                case Directions.S:
                    direction = FourDirections[2];
                    break;

                case Directions.O:
                    direction = FourDirections[3];
                    break;
            }

            Vector3 position = pos + direction;
            TileInformation tileInformation = col.GetValueOnCollection(position);
             
            

            return false;
        }

        public static List<Vector3> GetAdjacentTileVector(Vector3 pos, GridCollection col)
        {
            List<Vector3> nearTiles = new List<Vector3>();

            for (int x = 0; x < FourDirections.Count; x++)
            {
                Vector3 dir = pos + FourDirections[x];
                TileInformation tileInfo = col.GetValueOnCollection(dir);

                if (tileInfo != null)
                {
                    nearTiles.Add(dir);
                }
            }

            return nearTiles;
        }

        public static List<TileInformation> GetAdjacentTileInfo(Vector3 pos, GridCollection col)
        {
            List<TileInformation> nearTileInfo = new List<TileInformation>();

            for (int x = 0; x < FourDirections.Count; x++)
            {
                Vector3 dir = pos + FourDirections[x];
                TileInformation tileInfo = col.GetValueOnCollection(dir);

                if (tileInfo != null)
                {
                    nearTileInfo.Add(tileInfo);
                }
            }

            return nearTileInfo;
        }

        public static List<TileInformation> GetAdjacentTileInfoByInfo(TileInformation info, GridCollection col)
        {
            List<TileInformation> nearTileInfo = new List<TileInformation>();

            for (int i = 0; i < FourDirections.Count; i++)
            {
                Vector3 dir = info.gameObject.transform.position + FourDirections[i];
                TileInformation tileInfo = col.GetValueOnCollection(dir);

                if (tileInfo != null)
                {
                    nearTileInfo.Add(tileInfo);
                }
            }
            
            return nearTileInfo;
        }

        //TODO: Keep doing this, Unity its freezing like retard right now.
        public static List<TileInformation> GetAdjacentTileInfoByDistance(Vector3 pos, GridCollection col, float maxDistance)
        {
            List<TileInformation> nearTileInfo =  new List<TileInformation>();

            List<Vector3> usedTiles = new List<Vector3>();

            usedTiles.Add(pos);

            float distance = 0f;

            do
            {
                for (int i = 0; i < usedTiles.Count; i++)
                {
                    for (int j = 0; j < FourDirections.Count; j++)
                    {
                        Vector3 dir = usedTiles[i] + FourDirections[j];
                        TileInformation tileInfo = col.GetValueOnCollection(dir);

                        if (tileInfo != null)
                        {
                            float newDistance = Vector3.Distance(pos, usedTiles[i]);

                            if (distance > maxDistance)
                                break;

                            if (newDistance > distance)
                            {
                                distance = newDistance;
                            }
                            else
                            {
                                nearTileInfo.Add(tileInfo);
                            }
                        }
                    }

                    if (distance > maxDistance)
                        break;

                    List<Vector3> nearVector = new List<Vector3>();

                    nearVector = GetAdjacentTileVector(usedTiles[i], col);

                    for (int j = 0; j < nearVector.Count; j++)
                    {
                        if (!usedTiles.Contains(nearVector[j]))
                        {
                            usedTiles.Add(nearVector[j]);
                        }
                    }                    
                }                                
            } while (distance < maxDistance);

            
            return nearTileInfo;
        } 
    }
}