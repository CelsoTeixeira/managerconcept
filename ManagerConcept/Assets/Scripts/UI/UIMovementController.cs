using UnityEngine;
using System.Collections;
using CameraController;
using UnityEngine.UI;

namespace UIGeneral
{
    public class UIMovementController : MonoBehaviour
    {
        public Sprite Lock;
        public Sprite Unlock;

        private Image _sourceImage;

        void Awake()
        {
            _sourceImage = GetComponent<Image>();
        }

        /// <summary>
        ///     Refresh the _sourceImage to reflect the movement camera status.
        /// </summary>
        /// <param name="status"></param>
        public void LockUnlockMovement(CameraStatus status)
        {
            if (status == CameraStatus.LOCK)
            {
                _sourceImage.sprite = Lock;
            }
            else if (status == CameraStatus.UNLOCK)
            {
                _sourceImage.sprite = Unlock;
            }
        }
    }

}