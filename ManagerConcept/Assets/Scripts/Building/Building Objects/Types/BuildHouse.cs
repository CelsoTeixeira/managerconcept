using UnityEngine;
using System.Collections;
using Entity.Control;
using Generation.History;

namespace Building
{
    public class BuildHouse : BuildObject
    {
        public int PopulationSpace = 4;

        protected PopulationController _PopulationController;
        protected HistoryLogComponent _HistoryLogComponent;

        protected override void Awake()
        {
            base.Awake();

            BuildType = BuildType.House;

            _PopulationController =
                GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<PopulationController>();

            _HistoryLogComponent =
                GameObject.FindGameObjectWithTag(Helper.HistoryController).GetComponent<HistoryLogComponent>();
        }

        protected override void ActiveBuild()
        {
            base.ActiveBuild();

            //Debug.Log("Activating build.");

            p_BuildingControl.TickUpdate.Subscribe(BuildTickUpdate);
            p_BuildingControl.ConstantUpdate.Subscribe(BuildConstantUpdate);
        }

        protected override void OnDisable()
        {
            p_BuildingControl.TickUpdate.UnSubscribe(BuildTickUpdate);
            p_BuildingControl.ConstantUpdate.UnSubscribe(BuildConstantUpdate);
        }

        protected override void ConstructionFinished()
        {
            base.ConstructionFinished();

            _PopulationController.IncreaseMaxPopulation(PopulationSpace);
            
        }
    }

}