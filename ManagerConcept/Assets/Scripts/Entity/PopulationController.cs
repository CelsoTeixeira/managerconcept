using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Entity.Control
{
    public enum PopulationStatus
    {
        None = 0,
        Full = 1,
        AlmostFull = 2,
        Half = 3,
        AlmostEmpty = 4,
        Overcrowed = 5,
        Empty = 6,
    }

    public class PopulationController : MonoBehaviour
    {
        //TODO: MOve this to another class.
        public Text PopulationText;

        public PopulationStatus PopulationStatus = PopulationStatus.None;

        private int _maxPopulation;
        public int MaxPopulation { get { return _maxPopulation; } }

        private int _currentPopulation;
        public int CurrentPopulation { get { return _currentPopulation;} }
        
        //This will evaluate the current ratio population/maxpop
        private void PopulationStatusUpdate()
        {
            if (_currentPopulation > _maxPopulation)
            {
                PopulationText.text = _currentPopulation + " / " + " <color=red>" + _maxPopulation + "</color>";
            }
            else
            {
                //Also need to update the interface!
                PopulationText.text = _currentPopulation + " / " + _maxPopulation;
            }
            
            // Max ------ 100%
            // Current -- x        Max = Current * 100 / x

            if (_maxPopulation == 0)
                return;

            float currentPercentage = (_currentPopulation*100)/_maxPopulation;

            //Debug.Log("Current population with homes percent: " + currentPercentage);

            if (currentPercentage > 100)
            {
                PopulationStatus = PopulationStatus.Overcrowed;
            }
            else if (Mathf.RoundToInt(currentPercentage) == 100)
            {
                PopulationStatus = PopulationStatus.Full;
            }
            else if (currentPercentage < 100 && currentPercentage > 50)
            {
                PopulationStatus = PopulationStatus.AlmostFull;
            }
            else if (Mathf.RoundToInt(currentPercentage) == 50)
            {
                PopulationStatus = PopulationStatus.Half;
            }
            else if (currentPercentage < 50 && currentPercentage > 0)
            {
                PopulationStatus = PopulationStatus.AlmostEmpty;
            }
            else if (Mathf.RoundToInt(currentPercentage) == 0 || currentPercentage < 0)
            {
                PopulationStatus = PopulationStatus.Empty;
            }
        }

        public void IncreaseMaxPopulation(int i)
        {
            _maxPopulation += i;
            PopulationStatusUpdate();
        }

        public void DecreaseMaxPopulation(int i)
        {
            _maxPopulation -= i;
            PopulationStatusUpdate();
        }

        public void AddPopulation()
        {
            _currentPopulation ++;

            PopulationStatusUpdate();
        }

        public void DecreasePopulation()
        {
            _currentPopulation --;

            PopulationStatusUpdate();
        }
    }
}