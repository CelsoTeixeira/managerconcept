using System;
using UnityEngine;
using System.Collections;

using Building;

public enum TileType
{
    None = 0,
    Grass = 1,
    ForestHighDensity = 2,
    ForestMidDensity = 3,
    ForestLowDensity = 4,
    Mountain = 5,
    MountainHighDensity = 6,
    MountainMidDensity = 7,
    MountainLowDensity = 8,
    Rock = 9,
    Dirt = 10,

    EpicenterDebug1 = 20,
    EpicenterDebug2 = 21,
}

public enum MainType
{
    None,
    Floresta,
    Montanha,
    Grama
}

public enum DensityType
{
    None,
    Low,
    Mid,
    High
}

public enum BackgroundType
{
    LS,
    LSO,
    SO,
    NLS,
    NLSO,
    NSO,
    NL,
    NLO,
    NO,
    S,
    O,
    N,
    L
}


public class TileInformation : MonoBehaviour 
{
    public TileDefinition TileDefinition = new TileDefinition();
    
    public Vector3 Position;

    private SpriteRenderer _spriteRenderer;
    private TileSpritesCollection _tileSpritesCollection;

    public SpriteRenderer BackgroundRenderer;
    public SpriteRenderer MainRenderer;

    #region Mono methods

    void Awake()
    {
        _tileSpritesCollection =
            GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<TileSpritesCollection>();
    }

    void Start()
    {
        Position = transform.position;        
    }

    #endregion
    
    
    #region Texture

    public void UpdateTileSprite()
    {
        Sprite newSprite;

        switch (TileDefinition.Type)
        {
            case TileType.Grass:
                newSprite = _tileSpritesCollection.TileSprites[0];
                ChangeTileSprite(newSprite);
                break;

            case TileType.ForestLowDensity:
                newSprite = _tileSpritesCollection.TileForest[1];
                ChangeTileSprite(newSprite);
                break;

            case TileType.ForestMidDensity:
                newSprite = _tileSpritesCollection.TileForest[2];
                ChangeTileSprite(newSprite);
                break;

            case TileType.ForestHighDensity:
                newSprite = _tileSpritesCollection.TileForest[3];
                ChangeTileSprite(newSprite);
                break;
                
            case TileType.Dirt:
                newSprite = _tileSpritesCollection.TileSprites[4];                
                ChangeTileSprite(newSprite);
                break;


            case TileType.Mountain:
                newSprite = _tileSpritesCollection.TileMountain[0];
                ChangeTileSprite(newSprite);
                break;

            case TileType.MountainLowDensity:
                newSprite = _tileSpritesCollection.TileMountain[1];
                ChangeTileSprite(newSprite);
                break;

            case TileType.MountainMidDensity:
                newSprite = _tileSpritesCollection.TileMountain[2];
                ChangeTileSprite(newSprite);
                break;
            
            case TileType.MountainHighDensity:
                newSprite = _tileSpritesCollection.TileMountain[3];
                ChangeTileSprite(newSprite);
                break;           
            
            //NOTE(Celso): Take this off later.
            case TileType.EpicenterDebug1:
                newSprite = _tileSpritesCollection.TileDebug[0];
                ChangeTileSprite(newSprite);
                break;

            case TileType.EpicenterDebug2:
                newSprite = _tileSpritesCollection.TileDebug[1];
                ChangeTileSprite(newSprite);
                break;
        }
    }

    public void UpdateBackgroundSprite()
    {
        
    }

    public void UpdateMainSprite()
    {

        if (TileDefinition.MainType == MainType.Floresta)
        {
            
        }
        else if (TileDefinition.MainType == MainType.Montanha)
        {
            
        }
        else
        {
            //Main sprite = null;
            MainRenderer.sprite = null;

            //Background = Grama;
            BackgroundRenderer.sprite = _tileSpritesCollection.TileBackgroundGrama[0];

        }



    }

    public void PostProcess()
    {
        //Debug.Log("Post process");

        UpdateMainSprite();
        UpdateBackgroundSprite();
    }

    //We use this to change the texture of the material.
    public void ChangeTileSprite(Sprite toChange)
    {
        if (_spriteRenderer == null)
            _spriteRenderer = GetComponent<SpriteRenderer>();

        _spriteRenderer.sprite = toChange;
    }

    #endregion

    #region Type

    //This is used by the Information Panel.
    public string GetTileTypeToString()
    {
        return TileDefinition.Type.ToString();
    }

    public TileType GetTileType()
    {
        return TileDefinition.Type;
    }

    //This is used when we change any 
    public void ChangeTileType(TileType newType)
    {
        TileDefinition.Type = newType;

        UpdateTileSprite();
    }

    public void ChangeMainSprite(MainType newType, DensityType newDensity)
    {
        TileDefinition.MainType = newType;
        TileDefinition.DensityType = newDensity;

        UpdateMainSprite();
    }

    #endregion
}