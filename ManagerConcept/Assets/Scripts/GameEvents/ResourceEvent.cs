using UnityEngine;
using System.Collections;
using GameLogic.Events;
using GameLogic.Interface;

namespace GameLogic.Events
{
    public class ResourceEvent : BaseEvent
    {
        public string Message;

        private ResourceContainer _resourceContainer;
        private GameInterfaceControl _gameInterfaceControl;

        public override void Awake()
        {
            base.Awake();

            Type = EventType.Resource;

            _resourceContainer =
            GameObject.FindGameObjectWithTag(Helper.ResourceController).GetComponent<ResourceContainer>();

            _gameInterfaceControl =
                GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameInterfaceControl>();
        }

        protected override void Event()
        {
            base.Event();

            //For now, choose for a random resource.
            int rng = UnityEngine.Random.Range(0, 11);

            if (rng <= 5)
            {
                _resourceContainer.AddAmount(ResourceType.Food, 200);
            }
            else if (rng > 6 && rng <= 8)
            {
                _resourceContainer.AddAmount(ResourceType.Water, 400);
            }
            else if (rng > 8 && rng <= 11)
            {
                _resourceContainer.AddAmount(ResourceType.Wood, 100);
            }

            _gameInterfaceControl.RequestNewInterfaceUpdate(Message);
        }
        
        //public void AddResources(float food, float wood, float water)
        //{
        //    Debug.Log("Resource event");
        //    _resourceContainer.AddAmount(ResourceType.Food, food);
        //    _resourceContainer.AddAmount(ResourceType.Wood, wood);
        //    _resourceContainer.AddAmount(ResourceType.Water, water);
        //}

        //public void TakeResources(float food, float wood, float water)
        //{
        //    Debug.Log("Resource event");
        //    _resourceContainer.TakeAmount(ResourceType.Food, food);
        //    _resourceContainer.TakeAmount(ResourceType.Wood, wood);
        //    _resourceContainer.TakeAmount(ResourceType.Water, water);
        //}
    }
}