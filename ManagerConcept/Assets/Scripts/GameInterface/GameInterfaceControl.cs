using UnityEngine;
using System;
using System.Collections.Generic;

namespace GameLogic.Interface
{   
    /// <summary>
    ///     This handles the UI Information Panel.
    /// Anytime we need to show any message to the player we 
    /// call this and it will add the message to the Queue,
    /// the second we have avaiable slot, we just display it 
    /// for a amount of time.
    /// </summary>
    public class GameInterfaceControl : MonoBehaviour
    {
        public int TimeToWait = 5;

        public List<GameInterfaceComponent> InterfaceComponents = new List<GameInterfaceComponent>();
        private List<GameInterfaceComponent> _enabledComponents = new List<GameInterfaceComponent>();
        private List<GameInterfaceComponent> _disabledComponents = new List<GameInterfaceComponent>(); 
        
        private Queue<string> _requestsQueue = new Queue<string>();

        private int _interfaceActives = 0;

        private GameControl _gameControl;

        #region Mono
        public void Awake()
        {
            _gameControl = GetComponent<GameControl>();

            //Setup the components we will need.
            for (int i = 0; i < InterfaceComponents.Count; i++)
            {
                InterfaceComponents[i].SetupControl(this);

                InterfaceComponents[i].HideText();
                InterfaceComponents[i].HideCallback += OnInterfaceHide;    //We call this when we deactive the component.

                InterfaceComponents[i].TimeToWait = TimeToWait;
            }
        }

        //Subscribe to our main game loop.
        void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(GameInterfaceUpdate);
            _gameControl.TickControl.UpdateTick.Subscribe(FeedbackTickUpdate);
        }

        //Unsubscribe to our main game loop.
        void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(GameInterfaceUpdate);
            _gameControl.TickControl.UpdateTick.UnSubscribe(FeedbackTickUpdate);
        }
        #endregion

        void OnInterfaceHide()
        {
            _interfaceActives --;

            _interfaceActives = Mathf.Clamp(_interfaceActives, 0, 10);
        }

        public void GameInterfaceUpdate()
        {
            //Debug.Log("Interface feedback update.");

            //Debug.Log("Number of requests on Queue: " + _requestsQueue.Count);

            //If we have any request on the queue.
            if (_requestsQueue.Count > 0)
            {
                //If the active its less than the number of components we have, we have a disabled one to use.
                if (_interfaceActives < InterfaceComponents.Count)
                {
                    GameInterfaceComponent temp = CheckForUse();

                    if (temp != null)
                    {
                        _interfaceActives++;

                        _disabledComponents.Remove(temp);               //Remove from disabled and add to enable.

                        temp.ChangeText(_requestsQueue.Dequeue());      //Change the text to the first string on the Queue.

                        temp.ShowText();                                //Show the text componenet.
                    }                   
                }
            }
        }

        //Tick the timer.
        private void FeedbackTickUpdate()
        {
            if (_enabledComponents.Count > 0)
            {
                for (int i = 0; i < _enabledComponents.Count; i++)
                {
                    _enabledComponents[i].InternalUpdate();
                }
            }
        }

        //Check if we're already using a component.
        private GameInterfaceComponent CheckForUse()
        {
            for (int i = 0; i < InterfaceComponents.Count; i++)
            {
                if (InterfaceComponents[i].CanBeUsed)
                {
                    return InterfaceComponents[i];
                }
            }

            return null;
        }

        /// <summary>
        ///     This is the principal method we have in this class.
        /// Anytime we need some information to be displayed on the 
        /// screen we just call this and pass the string we will 
        /// show on screen.
        /// </summary>        
        public void RequestNewInterfaceUpdate(string textToUpdate)
        {
            _requestsQueue.Enqueue(textToUpdate);
        }

        //Send the component to the enabled list.
        public void SendToEnabledList(GameInterfaceComponent x)
        {
            if (!_enabledComponents.Contains(x))
            {
                _enabledComponents.Add(x);
            }

            if (_disabledComponents.Contains(x))
            {
                _disabledComponents.Remove(x);
            }
        }

        //Send to the disabled list.
        public void SendToDisabledList(GameInterfaceComponent x)
        {
            if (!_disabledComponents.Contains(x))
            {
                _disabledComponents.Add(x);
            }

            if (_enabledComponents.Contains(x))
            {
                _enabledComponents.Remove(x);
            }
        }
         
    }
}
