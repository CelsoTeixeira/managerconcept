using UnityEngine;
using System.Collections;

public static class LayerHelper
{
    public static int GridLayerBitmask = 1 << 8;
    public static int BuildsLayerBitmask = 1 << 9;
    public static int NPCsLayerBitmask = 1 << 10;
    public static int EntityLayerBitmask = 1 << 11;
}