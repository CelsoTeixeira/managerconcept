using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity.Control;
using Entity.WorkerNPC;
using Entiy.Build.CustomData;

//TODO: DEBUG ALL THE DEPLETED STATUS.

namespace Building
{   
    public enum BuildWorkStatus
    {
        Paused,
        Working,        
    }

    public enum BuildResourceStatus
    {
        Full,
        Depleted,
        SpaceAvaiable
    }
    
    public class BuildResource : BuildObject
    {
        public BuildWorkStatus WorkStatus = BuildWorkStatus.Paused;
        public BuildResourceStatus ResourceStatus = BuildResourceStatus.Full;

        public List<ResourceGenerationData> ResourceData = new List<ResourceGenerationData>();
        public ResourcePoolData ResourcePoolData;
        public ResourceWorkData ResourceWorkerData;

        public float ResourcePoolAmount;

        protected ResourceContainer p_ResourceContainer;

        private List<int> _ticks;           //This is a list of ticks we will want to have track.
        private int _depletedTick;          //This is the tick we enter after we have depleted the resource pool.

        private WorkerList _workerList;

        //This is to make sure we only subscribe once to the main loop.
        protected bool GenerationSubscribed = false;
        
        #region Mono
        protected override void Awake()
        {
            base.Awake();

            p_ResourceContainer = GameObject.FindGameObjectWithTag(Helper.ResourceController)
                        .GetComponent<ResourceContainer>();

            _workerList = GameObject.FindGameObjectWithTag(Helper.WorkerController)
                        .GetComponent<WorkerList>();
            
            _ticks = new List<int>();

            //We need to populate the tick list.
            //NOTE(Celso): We should think how we could to eliminate
            //this and do only one int. Maybe this can increase performace.
            for (int i = 0; i < ResourceData.Count; i++)
            {
                _ticks.Add(0);                
            }                        
        }

        protected override void Start()
        {
            base.Start();

            ResourcePoolAmount = ResourcePoolData.ResourcePool;     //We set the initial amount for the resource pool
        }
        #endregion

        #region Update override
        protected override void BuildConstantUpdate()
        {
            base.BuildConstantUpdate();
            
            if (ConstructionStatus != BuildConstructionStatus.Complete)
            {
                return;
            }

            //Control the build status, this should be controlled every frame.
            if (WorkersOnSite.Count == 0)   //If we don't have enough workers we pause the generation.
            {
                WorkStatus = BuildWorkStatus.Paused;
            }
            else if (WorkersOnSite.Count >= 1)   //If we have workers we start the generation.
            {
                WorkStatus = BuildWorkStatus.Working;
            }

            if (WorkStatus == BuildWorkStatus.Working)
            {
                if (!ParticleSystem.isPlaying)
                {
                    ParticleRenderer.material = ParticleMaterials[1];
                    ParticleSystem.Play();
                }
            }
            else if (ParticleSystem.isPlaying)
            {
                ParticleSystem.Stop();
            }
            
            //Debug.Log("Resource constant update.");
        }

        protected override void BuildTickUpdate()
        {
            //Base invoke
            base.BuildTickUpdate();

            //If we don't have a complete build, we don't generate stuff.
            if (ConstructionStatus != BuildConstructionStatus.Complete)
            {
                return;
            }

            //Increase our tick.
            TickIncrease();

            if (WorkStatus == BuildWorkStatus.Working)
            {
                //If we're working we calculate the resource generation.
                ResourceGeneration();                    
            }

            //We calculate the recover rate.
            ResourceInternalPoolUpdate();       //Calculate the geration rate for the pool.           

            //Debug.Log("Resource tick update.");
        }
        #endregion

        /// <summary>
        ///     This increase each tick we have on the list
        /// and make sure we loop between values.
        /// </summary>
        private void TickIncrease()
        {
            //Debug.Log("Build tick.", this.gameObject);

            //This is the loop to control every tick.
            for (int i = 0; i < _ticks.Count; i++)
            {
                _ticks[i]++;    //Increse the tick counter for each tick we want.

                if (_ticks[i] > ResourceData[i].GenerationMaxTick)
                {
                    _ticks[i] = 1;  //We want to go back for 1 if we exced the amount.
                }
            }            
        }
        
        /*
        NOTE(Celso): Right now we're using the next order:
            -> ResourceGeneration();
            -> ResourceInternalPoolUpdate();

            We actually do the resource generation and then we compute the pool generation.
            So we consume the resource from the pool and the we do the logic to generate the pool.
        */

        //This is where we handle the pool of resources update, what we're going to call and stuff.
        private void ResourceInternalPoolUpdate()
        {
            switch (ResourceStatus)
            {
                case BuildResourceStatus.Depleted:
                    //Counter here until we can produce again.
                    DepletedTimer();
                    break;
                    
                case BuildResourceStatus.SpaceAvaiable:
                    //Generation here.
                    CalculatePoolGeneration();
                    break;
                
                case BuildResourceStatus.Full:
                    //Do nothing, we have to much resource.
                    //We can change ui and stuff here.
                    break;
            }
        }
        
        #region Internal pool tracker
        #region Depleted Status
        /// <summary>
        ///     This is the timer that control the internal resource on the
        /// pool. If we're here, we don't want to generate nothing.
        /// </summary>
        private void DepletedTimer()
        {
            _depletedTick++;
            if (_depletedTick >= ResourcePoolData.DepletedTimeToRecover)
            {
                _depletedTick = 0;
                ResourceStatus = BuildResourceStatus.SpaceAvaiable;
            }
        }
        #endregion

        private void CalculatePoolGeneration()
        {
            //NOTE(Celso): We want to have both rates to generate our final rate.
            float recoverRate = ResourcePoolData.RecoverRate.Rate;
            float decayRate = 0f;

            //If we're working we want a decay rate based on workers.
            if (WorkStatus == BuildWorkStatus.Working)
            {
                decayRate += ResourcePoolData.DecayRate.Rate * ResourceWorkerData.GraphWorkerInfluence.Evaluate(WorkersOnSite.Count);

                //If we have more than enough workers, we can change the math to have more penalties per extra worker.
                if (WorkersOnSite.Count > ResourceWorkerData.ConfortWorkers)
                {
                    //Getting the exced workers.
                    int excedWorkers = ResourceWorkerData.ConfortWorkers - WorkersOnSite.Count;

                    //NOTE(Celso): We're multiplying the rate for each extra work we have, this will 
                    //scale realy high if we dont get this smarter.
                    decayRate += ResourcePoolData.DecayRate.Rate * 
                        (ResourceWorkerData.GraphWorkerInfluence.Evaluate(excedWorkers) * excedWorkers);

                    decayRate /= 2f;

                    //Debug.Log("Decay rate based on the amount of extra workers we have: " + decayRate);
                }
            }

            //Maybe we can have some other influence here to affect in a different way. Maybe based on the workers.
            float amountToGenerate = recoverRate - decayRate;
            //Debug.Log("How much we are generating on this tick: " + amountToGenerate);

            //What we're going to add on the resource pool from the generation.
            AddResourceOnPool(amountToGenerate);
        }

        private bool AddResourceOnPool(float amount)
        {
            ResourcePoolAmount += amount;
            ResourcePoolAmount = Mathf.Clamp(ResourcePoolAmount, 0, ResourcePoolData.ResourcePool); //Clamp the max/min amount of the pool.          
            //Debug.Log("The total of the pool: " + ResourcePoolAmount);

            //If we have more than we can store we go to full mode.
            if (ResourcePoolAmount >= ResourcePoolData.ResourcePool)
            {
                ResourceStatus = BuildResourceStatus.Full;
            }
            else if (ResourcePoolAmount <= 0)
            {
                ResourceStatus = BuildResourceStatus.Depleted;
            }

            return true;    //Just send true because we can't fail this right now.
        }

        /*  NOTE(Celso):
            When we try to get resources from the pool we check for the amount,
            if we have enough resources we get it, take off from the pool and keep going.
            We return true to flag that we succed on the operation.
            If we don't have enough we enter in depleted mode and send false from the operation.
        */
        private bool GetResourceFromPool(float amount)
        {
            //If we have enough resources we take the amount we want.
            if (ResourcePoolAmount >= amount)
            {
                ResourcePoolAmount -= amount;       //Decrease the amount from the pool.
                ResourcePoolAmount = Mathf.Clamp(ResourcePoolAmount, 0, ResourcePoolData.ResourcePool);     //We don't want negative resources.

                return true;    //Success.
            }
            else if (ResourcePoolAmount < amount)   //Else, we're going to force the state to go into depleted and have a penalty.
            {
                //Debug.Log("We dont have enough resources on the pool.");
                ResourceStatus = BuildResourceStatus.Depleted;      //Go to Depleted behaviour.
                return false;       //Fail.
            }

            return false;   //Fail.
        }
        #endregion

        #region Resource generation
        private void ResourceGeneration()
        {
            //If we're with Depleted status, just leave the generation because we dont generate
            //resources from the pool.
            if (ResourceStatus == BuildResourceStatus.Depleted)
                return;

            for (int i = 0; i < ResourceData.Count; i++)
            {
                //Debug.Log("Resource generation: " + currentTick);

                //Debug.Log("Modulo on maxTick: " + ResourceData[i].GenerationTick % maxTick);
                //Debug.Log("Modulo on currentTick: " + currentTick % ResourceData[i].GenerationTick);

                if ((_ticks[i] % ResourceData[i].GenerationTick) == 0)
                {
                    float graphInfluence = Mathf.Round(ResourceData[i].GraphTickInfluence.Evaluate(_ticks[i]) * 4) / 4;
                    float workerInfluence = Mathf.Round((ResourceData[i].GraphWorkerInfluence.Evaluate(WorkersOnSite.Count)) * 4) / 4;
                    //NOTE(Celso): We want to play around here just to check for a better math.
                    float amountGenerate = (ResourceData[i].GenerationAmount * workerInfluence) * graphInfluence;

                    //Debug.Log("The worker influence on the generation: " + workerInfluence);
                    //Debug.Log("The tick influence on the generation: " + graphInfluence);
                    //Debug.Log("The total of resource generated: " + amountGenerate);

                    //If we have enough resources on the pool we get it.
                    if (GetResourceFromPool(amountGenerate))
                    {
                        //Debug.Log("We have enough resource on the pool to take what we need.");

                        p_ResourceContainer.AddAmount(ResourceData[i].Type, amountGenerate);

                        //Debug.Log(
                        //    "Generating new resource! " + ResourceData[i].Type + " / " + amountGenerate +
                        //    Environment.NewLine
                        //    + "Tick: " + _ticks[i] + " / GenerationTick: " + ResourceData[i].GenerationTick
                        //    , this.gameObject);
                    }
                }
            }
        }
        #endregion

        #region Workers buttons
        /*
        NOTE(Celso): This is what we're using on the UI calls.
        We need to use this instead the Interface IBuildWorker
        because we don't have a worker to pass, we are just
        going to ask for a worker and let the worker list handle
        the rest for us.
        */

        public void AskWorker()
        {
            //Debug.Log("Asking for new worker!", this.gameObject);

            _workerList.AskForWorkers(this);
        }

        public void RemoveResourceWorker()
        {
            if (WorkersOnSite.Count <= 0)
            {
                //Debug.Log("No workers on site!");
                return;
            }

            //Debug.Log("Removing worker!", this.gameObject);
            Worker toRemove = WorkersOnSite[0];
                        
            toRemove.LeaveBuild();
            toRemove.DeleteJob();

            WorkersOnSite.RemoveAt(0);
        }
        #endregion

        #region Workers ovveride

        //public override void AddWorker(Worker worker)
        //{
        //    if (WorkersOnSite.Count < ResourceWorkerData.MaximunWorkers)
        //    {
        //        Debug.Log("Adding a new worker!");
        //        WorkersOnSite.Add(worker);
        //    }
        //    else
        //    {
        //        Debug.Log("Build is full! Go away!");
        //        p_BuildTracking.RemoveBuild(this);
        //        worker.ForceMovement();
        //    }
        //}

        #endregion

        #region Activation override
        protected override void ActiveBuild()
        {
            base.ActiveBuild();
            
            //Debug.Log("Activating build.");

            p_BuildingControl.TickUpdate.Subscribe(BuildTickUpdate);
            p_BuildingControl.ConstantUpdate.Subscribe(BuildConstantUpdate);

            GenerationSubscribed = true;
        }

        public void ResourceDeActivation()
        {
            p_BuildingControl.TickUpdate.UnSubscribe(BuildTickUpdate);
            p_BuildingControl.ConstantUpdate.UnSubscribe(BuildConstantUpdate);

            GenerationSubscribed = false;
        }

        protected override void OnDisable()
        {
            if (GenerationSubscribed)
            {
                ResourceDeActivation();
            }
        }
        #endregion
    }
}