using UnityEngine;
using System.Collections;

public class UIGeneralController : MonoBehaviour
{
    public GameObject AdvancedGenerationPanel;

    public GameObject TileInformationPanel;

    void Start()
    {
        AdvancedGenerationPanel.SetActive(false);

        //TileInformationPanel.SetActive(false);
    }
}