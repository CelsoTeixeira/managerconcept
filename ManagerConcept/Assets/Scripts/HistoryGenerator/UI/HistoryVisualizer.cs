using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Generation.History;

public class HistoryVisualizer : MonoBehaviour 
{
    //Need the scrollbar!

    public Text Text;
    
    private HistoryLogComponent _historyLogComponent;

    private Dictionary<string, LogBase> _logs; 

    void Awake()
    {
        _historyLogComponent =
            GameObject.FindGameObjectWithTag(Helper.HistoryController).GetComponent<HistoryLogComponent>();
    }

    void OnEnable()
    {
        GetLogs();

        //Debug.Log("On Enable");
    }

    void OnDisable()
    {
        
    }

    private void GetLogs()
    {
        _logs = _historyLogComponent.LogsCollection;

        if (_logs.Count == 0)
        {
            //We dont have anything to show, just tell there is no logs.
            Text.text = "Nenhuma entrada feita.";
            return;
        }

        string logs = "";

        for (int i = 0; i < _logs.Count; i++)
        {
            
            string date = _logs.Keys.ElementAt(i) + Environment.NewLine;
            
            string shortMessage = _logs.Values.ElementAt(i).ShortLog + Environment.NewLine;

            string longMessage = _logs.Values.ElementAt(i).LongLog + Environment.NewLine;

            logs += date;
            logs += shortMessage;
            logs += longMessage;
            logs += Environment.NewLine;
        }

        Text.text = logs;
    }

    public void UpdateLogs()
    {
        GetLogs();
    }




}