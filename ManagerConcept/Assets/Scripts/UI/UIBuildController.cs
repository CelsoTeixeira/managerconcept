using UnityEngine;
using System.Collections;
using Building;

public class UIBuildController : MonoBehaviour
{
    private BuildPlacement _buildPlacement;
    
    void Awake()
    {
        _buildPlacement = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildPlacement>();
    }

    public void PlaceNewBuild(GameObject buildToPlace)
    {
        _buildPlacement.PlaceNewBuild(buildToPlace);
    }
}
