using System;
using UnityEngine;
using System.Collections;

namespace GameLogic
{
    [Serializable]
    public class GameTick
    {
        private Action TickUpdate;

        public void Subscribe(Action action)
        {
            TickUpdate += action;
        }

        public void UnSubscribe(Action action)
        {
            TickUpdate -= action;
        }

        public Action Tick()
        {
            return TickUpdate;
        }
        
    }

}