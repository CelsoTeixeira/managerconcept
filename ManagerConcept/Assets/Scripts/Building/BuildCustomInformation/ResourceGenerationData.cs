using UnityEngine;
using System.Collections;

namespace Entiy.Build.CustomData
{
    [System.Serializable]
    public class ResourceGenerationData : ScriptableObject
    {
        public ResourceType Type;                       //This is our resource type.
        public float GenerationAmount;                  //This is the base amount to generate.
        public int GenerationTick;                      //This is how frequently we generate.
        public int GenerationMaxTick;                   //This is the maximun tick we can go to.
        public AnimationCurve GraphTickInfluence;       //This is the curve we will influence our generation based on the ticks.
        public AnimationCurve GraphWorkerInfluence;     //This is the curve we will influnece our generation based on the amount of workers we current have in the build.
    }
}


