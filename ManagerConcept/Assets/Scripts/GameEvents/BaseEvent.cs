using System;
using UnityEngine;
using System.Collections;

namespace GameLogic.Events
{
    public enum EventType
    {
        None = 0,
        Resource = 1,
        Population = 2
    }

    [Serializable]
    public class BaseEvent : MonoBehaviour, EventCaller
    {
        public EventType Type = EventType.None;

        public bool CanTriggerEvent = false;

        public int EventCooldown = 10;
        protected float _eventTimer = 0;

        [Range(0, 100)]
        public int EventProbability = 60;
        protected int _eventProbability;
        
        public virtual void Awake()
        {
            _eventProbability = EventProbability;
        }

        public virtual void BaseUpdate()
        {
            EventUpdate();
        }

        protected virtual void Event()
        {
            //Debug.Log("Triggering event!");
        }

        //This should run on the TickUpdate.
        protected virtual void EventUpdate()
        {
            _eventTimer ++; //Increase the timer by one.

            if (_eventTimer >= EventCooldown)   //If we wait enough time.
            {
                CanTriggerEvent = true;     //We can trigger the event.s

                float rng = UnityEngine.Random.Range(0, 100);       //Roll the dice.

                if (rng <= _eventProbability)    //If we got a good rool we call the event and reset the internal tracker.
                {
                    Event(); //Call the event!

                    _eventTimer = 0; //Reset the timer.
                    _eventProbability = EventProbability; //Reset the probability

                    CanTriggerEvent = false;
                }
                else
                {
                    //Debug.Log("Event chance failed, increasing the odds.");
                    _eventProbability ++;    //If we fail we increase our chance for the next tick.
                }
            }
        }
        


        #region Interface EventCaller
        public void CallEvent()
        {
            Event();
        }
        #endregion
    }



    public interface EventCaller
    {
        void CallEvent();
    }
}

