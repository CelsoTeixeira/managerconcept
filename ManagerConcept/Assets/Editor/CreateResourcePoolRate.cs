using UnityEngine;
using System.Collections;
using Entiy.Build.CustomData;
using UnityEditor;

public class CreateResourcePoolRate : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Resource Pool rate")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<ResourcePoolRate>();
    }
}
