using UnityEngine;
using System.Collections;

public class MainMenuInterface : BaseEventInterface 
{
    public override void OnEnable()
    {
        _GameControl.MainMenuEvent.Subscribe(ShowPanels);
        _GameControl.StartGameEvent.Subscribe(HidePanels);
        _GameControl.PauseGameEvent.Subscribe(HidePanels);
    }

    public override void OnDisable()
    {
        _GameControl.MainMenuEvent.UnSubscribe(ShowPanels);
        _GameControl.StartGameEvent.UnSubscribe(HidePanels);
        _GameControl.PauseGameEvent.UnSubscribe(HidePanels);
    }
}