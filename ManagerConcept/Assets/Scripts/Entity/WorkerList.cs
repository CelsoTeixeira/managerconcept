using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Building;

using Entity.Behaviour;
using Entity.WorkerNPC;

namespace Entity.Control
{
    public class WorkerList : MonoBehaviour
    {
        public List<Worker> FreeWorkers = new List<Worker>();
        
        public void AddNewFreeWorker(Worker worker)
        {
            if (!FreeWorkers.Contains(worker))
            {
                FreeWorkers.Add(worker);
            }
        }

        public void RemoveFreeWorker(Worker worker)
        {
            if (FreeWorkers.Contains(worker))
            {
                FreeWorkers.Remove(worker);
            }
        }

        public bool AskForWorkers(BuildObject toGo)
        {
            if (FreeWorkers.Count == 0 || FreeWorkers == null)
            {
                //Debug.Log("We don't have any free workers.");
                return false;
            }

            Worker currentWorker = FreeWorkers[0];
            FreeWorkers.RemoveAt(0);
            
            currentWorker.ChangeState(WorkerState.Working, toGo.gameObject);
            
            currentWorker.AssignJob(toGo.gameObject.transform.position, toGo.gameObject);

            //IMovement iMove = currentWorker.GetComponent<IMovement>();
            //iMove.GoToBuild(toGo, WorkerState.Working);

            return true;
        }        
    }
}