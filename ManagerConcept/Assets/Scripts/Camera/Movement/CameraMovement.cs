using UnityEngine;
using System.Collections;
using GameLogic;

namespace CameraController
{
    public enum CameraStatus
    {
        LOCK = 1,
        UNLOCK =2
    }

    [DisallowMultipleComponent]
    public class CameraMovement : MonoBehaviour
    {
        public float DistanceToMoveCamera = 2f;
        public float DefaultZ = -10f;
        public float SmoothTime = 0.2f;

        public float ScrollSize = 10f;

        public float ScrollMaxSize = 30f;
        public float ScrollMinSize = 5f;

        public Vector3 Rotation;

        public CameraStatus Status = CameraStatus.LOCK;
        
        private Vector3 _refVel = Vector3.zero;

        private GameControl _gameControl;

        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();

            this.transform.Rotate(Rotation);
            Camera.main.orthographicSize = ScrollSize;
        }

        void OnEnable()
        {
            _gameControl.TickControl.UpdateConstantTick.Subscribe(MouseConstantUpdate);
        }

        void OnDisable()
        {
            _gameControl.TickControl.UpdateConstantTick.UnSubscribe(MouseConstantUpdate);
        }

        void MouseConstantUpdate()
        {

            //Debug.Log("Mouse controller.");

            //if (Input.GetKeyDown(LockUnlockCode))
            //{
            //    Status = (Status == CameraStatus.LOCK) ? CameraStatus.UNLOCK : CameraStatus.LOCK;
            //}

            #region Mouse scroll
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                //Debug.Log("Scrolling");
                ScrollSize -= Input.GetAxis("Mouse ScrollWheel");

                ScrollSize = Mathf.Clamp(ScrollSize, ScrollMinSize, ScrollMaxSize);
        
                Camera.main.orthographicSize = ScrollSize;
            }
            #endregion

            #region MouseMovement
            
            if (Status == CameraStatus.LOCK)
                return;
            
            //Debug.Log("Camera movement working.");

            //Our mouse position to world point and with the same Z of the camera.
            Vector3 mousePosition = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, Camera.main.ScreenToWorldPoint(Input.mousePosition).z);
            
            //The distance between the camera and where the mouse is on the screen.
            float Distance = Vector3.Distance(gameObject.transform.position, mousePosition);

            //If we have a distance greater than the DistanceToMoveCamera
            if (Distance > DistanceToMoveCamera)
            {
                //We get the new X and Z, the Y will always be the same.
                float newX = Mathf.Lerp(transform.position.x, mousePosition.x, Time.deltaTime);
                float newY = Mathf.Lerp(transform.position.y, mousePosition.y, Time.deltaTime);

                //We SmoothDamp the current position to the newX and newY.
                transform.position = Vector3.SmoothDamp(transform.position, new Vector3(newX, newY, DefaultZ), 
                    ref _refVel, SmoothTime);
            }
            
            Debug.DrawLine(gameObject.transform.position, mousePosition, Color.green);
            //Debug.Log("Distance CameraObject-MousePosition: " + Distance);
            #endregion

        }

        public void LockUnlockStatus()
        {
            Status = (Status == CameraStatus.LOCK) ? CameraStatus.UNLOCK : CameraStatus.LOCK;
        }

        public void ChangeCameraStatus(CameraStatus newStatus)
        {
            Status = newStatus;
        }

        public void ForceCameraPosition(Vector2 pos)
        {
            transform.position = new Vector3(pos.x, pos.y, DefaultZ);
        }
    }
}