using System;
using UnityEngine;
using System.Collections.Generic;

using CameraController;
using Generation;
using Generation.Biomes;

[RequireComponent(typeof(GridCollection))]
[RequireComponent(typeof(TileSpritesCollection))]
public class GridController : MonoBehaviour
{
    public BiomePercentData PercentData = new BiomePercentData();                           //Basic data about the percents we want to have on the map.
    public GridDefinition GridDefinition = new GridDefinition();                             //This is the basic information about the size of our tiles and size of the grid.
    
    #region Generation
    private readonly CreateGridPositions _createGridPositions = new CreateGridPositions();            //This is used to create a list of positions based on the GridDefinition we're passing.
    public CreateGridVisualization _createGridVisualization = new CreateGridVisualization(); //This is used to create the visualization of the grid, based on the Positions and on the GridDefinition we're using

    private readonly BiomeGeneration _biomeGeneration = new BiomeGeneration();
    #endregion

    #region Tracking lists
    private GridCollection _gridCollection;

    private List<GameObject> _gridTiles = new List<GameObject>();                 
    private List<Vector3> _gridPositons = new List<Vector3>();                    
    private List<TileInformation> _gridTileInfo = new List<TileInformation>();    
    #endregion
    
    void Awake()
    {
        _gridCollection = GetComponent<GridCollection>();
    }

    #region Grid Generation
    /// <summary>
    ///     This method will generate a complete new grid.
    /// We will get new Positions based on the grid definition,
    /// and a new randomizer will be applied.
    /// </summary>
    private void GenerateNewGrid()
    {
        //Clear the current grid.
        ClearCurrentGrid();

        //Ask for a new list of positions based on the definition
        _gridPositons =  _createGridPositions.GeneratePositions(GridDefinition);

        //Ask for a new list of tiles based on the list of positions
        _gridTiles = _createGridVisualization.CreateGridTiles(_gridPositons, GridDefinition);

        //Cache tile informations
        _gridTileInfo = GridUtility.GetTileInformation(_gridTiles);
    
        BiomeInitialFill.FillGridWithGrass(_gridTileInfo);
            
        //Setup the Collection information
        _gridCollection.SetupCollectionData(_gridPositons, _gridTileInfo, GridDefinition);

        //Run the biome generation.
        _biomeGeneration.GenerateFullBiome(_gridTileInfo ,PercentData, _gridCollection);

        //End of generation
        
        //This will update our Pathfinder GridGraph.
        ProceduralGridCreation.GridUpdate(_gridCollection.GetGridDefinition());
        
        //Move the camera to the center of the grid.
        
        //We force the gc here.
        GC.Collect();
    }
    
    #endregion

    #region Grid Recreation

    /// <summary>
    ///     This method will recreate an previous grid
    /// based on the position and on the grid definition,
    /// the tile definition will be used to reaplied the randomizer.
    /// NOTE(Celso): I think we don't need to save the positions, 
    /// if we have the GridDefinition, everything will be equal because
    /// we have the same definition on the generation.
    /// </summary>
    public void RecreateGrid(List<Vector3> pos, List<TileDefinition> tileDef, GridDefinition def)
    {
        //Clear the current grid.
        ClearCurrentGrid();

        //Recreate grid positions based on the grid definition we have saved.

        //Recreate the grid visualization.
        _gridTiles = _createGridVisualization.CreateGridTiles(pos, def);

        //Apply TIleDefinition on each TileInformation.
            //->Get all TileInformation
            //->Change TileDefinition on each TileInformation
        
        _gridTileInfo = GridUtility.GetTileInformation(_gridTiles);

        for (int x = 0; x < _gridTileInfo.Count; x++)
        {
            _gridTileInfo[x].TileDefinition = tileDef[x];
        }
        
        //Finish
    }

    #endregion
    
    #region Utility
    
    //This can be called from the UI Grid Controller.
    public void ClearCurrentGrid()
    {
        GridUtility.DestroyGridObjects(_gridTiles);
        _gridTiles.Clear();

        //Debug.Log("List clear!");    
    }

    //NOTE(Celso): This will force a collection update, we're using this class  
    //for everything related to the generation, it will be better to sub-class 
    //some stuff here.
    public void ForceCollectionUpdate(GridDefinition def)
    {
        _gridCollection.UpdateGridDefinition(def);
    }

    public void CreateGrid()
    {
        GenerateNewGrid();
    }

    #endregion
}