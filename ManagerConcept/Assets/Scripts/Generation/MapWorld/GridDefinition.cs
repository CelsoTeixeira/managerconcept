using System;
using UnityEngine;
using System.Collections;

namespace Generation
{
    [Serializable]
    public class GridDefinition
    {
        public int Size_X;
        public int Size_Y;

        public int TileSize_X;
        public int TileSize_Y;

        public int MaxSize_X;
        public int MaxSize_Y;

        //Constructor with default values
        public GridDefinition()
        {
            Size_X = 10;
            Size_Y = 10;

            TileSize_X = 1;
            TileSize_Y = 1;

            MaxSize_X = 50;
            MaxSize_Y = 50;
            
        }
    }
}