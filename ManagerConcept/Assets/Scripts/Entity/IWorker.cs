using UnityEngine;
using System.Collections;

namespace Entity.WorkerNPC
{
    public interface IWorker
    {
        void LeaveBuild();
        void EnterBuild();

        void AssignHome(Vector3 position);
        void AssignJob(Vector3 position, GameObject gameObject);
        void DeleteJob();
    }
}