using System;
using UnityEngine;
using System.Collections;
using Generation.History;

namespace GameLogic.Seasons
{
    public class SeasonControl : MonoBehaviour
    {
        public SeasonsType CurrentSeason = SeasonsType.Spring;

        public Action OnChangeSeason = () => {};

        private HistoryLogComponent _historyLogComponent;

        #region Mono
        void Awake()
        {
            _historyLogComponent =
                GameObject.FindGameObjectWithTag(Helper.HistoryController).GetComponent<HistoryLogComponent>();            
        }

        void OnEnable()
        {
            OnChangeSeason += LogChangeSeason;
        }

        void OnDisable()
        {
            OnChangeSeason -= LogChangeSeason;
        }
        #endregion

        /// <summary>
        ///     This handles the sequency for the seasons, we can do something cool here and 
        /// do some randomization based on a random event.
        /// </summary>
        private void SeasonChange()
        {
            switch (CurrentSeason)
            {
                case SeasonsType.Spring:
                    CurrentSeason = SeasonsType.Summer;
                    break;
                    
                case SeasonsType.Summer:
                    CurrentSeason = SeasonsType.Autumn;
                    break;
                    
                case SeasonsType.Autumn: 
                    CurrentSeason = SeasonsType.Winter;
                    break;

                case SeasonsType.Winter:
                    CurrentSeason = SeasonsType.Spring;
                    break;
            }
        }        

        /// <summary>
        ///     This is what we use to pass to the next season.
        /// If we have any method subscribed on the OnChangeSeason,
        /// we call it.
        /// </summary>
        public void NextSeason()
        {
            SeasonChange();

            if (OnChangeSeason != null )
            {
                OnChangeSeason.Invoke();
            }
        }
        
        /// <summary>
        ///     This is what we are using to add a log entry on the HistoryLogComponent,
        /// this is called when the OnChangeSeason is called.        
        /// </summary>
        private void LogChangeSeason()
        {
            //_historyLogComponent.AddLog("� o in�cio de uma nova esta��o: " + CurrentSeason.ToString());
        }
    }
}

