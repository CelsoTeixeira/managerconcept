using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLogic;

public class BaseEventInterface : MonoBehaviour 
{
    public List<GameObject> Objects = new List<GameObject>();

    protected GameControl _GameControl;

    protected virtual void Awake()
    {
        _GameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
        
    }

    protected void ToggleObjects(bool cond)
    {
        for (int i = 0; i < Objects.Count; i++)
        {
            Objects[i].SetActive(cond);
        }
    }

    protected virtual void ShowPanels()
    {
        ToggleObjects(true);
    }

    protected virtual void HidePanels()
    {
        ToggleObjects(false);
    }


    //This is what we want to customize for each event type.
    public virtual void OnEnable()
    {

    }

    public virtual void OnDisable()
    {
        
    }



}