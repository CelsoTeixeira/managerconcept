using UnityEngine;
using System.Collections;
using Entiy.Build.CustomData;
using UnityEditor;

public class CreateResourcePoolData : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Resource Pool Data")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<ResourcePoolData>();
    }
}
