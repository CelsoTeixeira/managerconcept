using UnityEngine;
using System.Collections;

namespace Building
{
    public enum BuildPlacementStatus
    {
        None = 0,
        Working = 1,
    }

    public class BuildPlacement : MonoBehaviour
    {
        [Tooltip("Parent transform for builds.")]
        public Transform BuildHolder;

        private BuildPlacementStatus _status = BuildPlacementStatus.None;

        private GameObject _objectToConstruct;

        private GameObject _currentObjectToPlace;
        private BuildObject _currentBuildObject;
        private BoxCollider2D _currentCollider;

        private MouseInput _mouseInput;
        private BuildValidation _buildValidation;

        private BuildingControl _buildingControl;
        private ResourceContainer _resourceContainer;

        private BuildTracking _buildTracking;

        #region Mono

        private void Awake()
        {
            _mouseInput = GameObject.FindGameObjectWithTag(Helper.InputHandler).GetComponent<MouseInput>();
            _resourceContainer =
                GameObject.FindGameObjectWithTag(Helper.ResourceController).GetComponent<ResourceContainer>();
            _buildValidation = GetComponent<BuildValidation>();
            _buildingControl = GetComponent<BuildingControl>();

            _buildTracking = GetComponent<BuildTracking>();
        }

        private void OnEnable()
        {
            _buildingControl.BuildUtilitiesTick.Subscribe(PlacementUpdate);
        }

        private void OnDisable()
        {
            _buildingControl.BuildUtilitiesTick.UnSubscribe(PlacementUpdate);
        }

        #endregion

        private void PlacementUpdate()
        {
            if (_status != BuildPlacementStatus.Working)
                return;

            Vector3 mousePosition = _mouseInput.GetRayHitPosition();

            if (_currentObjectToPlace == null)
            {
                GameObject newgo = Instantiate(_objectToConstruct, mousePosition, Quaternion.identity) as GameObject;
                _currentObjectToPlace = newgo;
                _currentBuildObject = _currentObjectToPlace.GetComponent<BuildObject>();

                _currentCollider = _currentBuildObject.gameObject.GetComponent<BoxCollider2D>();
                _currentCollider.enabled = false;
            }

            //This makes the build follow the mouse.
            _currentObjectToPlace.transform.position = mousePosition;


            if (Input.GetMouseButtonDown(0))
            {
                //We need a raycheck here.
                //We used to use SimpleBuildPositionValidation.
                if (_buildValidation.BuildPositionValidationRayCheck(mousePosition))
                {
                    //Check for wood cost, if true take resource from container.
                    if (_resourceContainer.TakeAmount(ResourceType.Wood, _currentBuildObject.BuildCost.WoodCost))
                    {
                        //Debug.Log("Building!");

                        _currentObjectToPlace.transform.position = mousePosition;
                        _currentObjectToPlace.transform.SetParent(BuildHolder);
                        _currentCollider.enabled = true;

                        //NOTE(Celso): This is where we active the build and let it handle their own initialization.
                        IBuildActivation buildActivation = _currentObjectToPlace.GetComponent<IBuildActivation>();
                        if (buildActivation != null)
                            buildActivation.Active();

                        ResetCurrentObject();
                    }
                    else
                    {
                        MouseRightClick();
                    }
                }
                else
                {
                    Debug.Log("Building failed. Going to remove the build.");

                    if (_currentObjectToPlace != null)
                    {
                        MouseRightClick();
                    }
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (_currentObjectToPlace != null)
                {
                    MouseRightClick();
                }
            }
        }
        
        //This is beign called from the UI button.
        public void PlaceNewBuild(GameObject newBuild)
        {
            //This places the new build on the logic.
            _objectToConstruct = newBuild;

            //Make the transition to the working side of the placement.
            _status = BuildPlacementStatus.Working;
        }

        public void PlaceNewBuild(GameObject newBuild, Vector3 pos, BuildType type)
        {
            GameObject build = Instantiate(newBuild, pos, Quaternion.identity) as GameObject;
            build.transform.SetParent(BuildHolder);

            if (type == BuildType.Center)
            {
                _buildTracking.CityCenterPosition = pos;
            }
        }

        private void MouseRightClick()
        {
            Destroy(_currentObjectToPlace);

            //Debug.Log("Build removed.");

            ResetCurrentObject();
        }

        //Reset the hold status for every component we have.
        private void ResetCurrentObject()
        {
            _status = BuildPlacementStatus.None;
            _currentObjectToPlace = null;
            _currentBuildObject = null;
            _currentCollider = null;
        }
    }

}