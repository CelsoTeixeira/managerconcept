using UnityEngine;
using System.Collections;

using UnityEditor;

public class CreateNpcArtCollection : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/NPC Art Collection")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<NpcArtCollection>();
    }
}