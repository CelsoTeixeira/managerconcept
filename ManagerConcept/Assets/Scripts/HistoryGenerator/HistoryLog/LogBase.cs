using System;
using UnityEngine;
using System.Collections;
using GameLogic.Seasons;

namespace Generation.History
{
    public enum HistoryType
    {
        Resource,
        ResourceWood,
        ResourceFood,
        ResourceWater,
        Theft,
        ConstructionFinished,
        ConstructionWell,
        ConstructionFarm,
        ConstructionHouse,
        Fire,
        Invasion,
    }

    /// <summary>
    ///     This is the base for the Generation History.
    /// We're having the Type and ShortLog pre-defined,
    /// the Date and the LongLog it's going to be generated 
    /// at Runtime.
    ///     The date will be something like TICKSEASONYEAR.
    ///     The LongLog will be from a collection with lots 
    /// of possibilities to be chosen from, so with this we
    /// will get a lot of variations.
    /// </summary>
    
    [Serializable]
    public class LogBase
    {
        public HistoryType Type;    //The type of the history.

        public string Date;         //This is generate at runtime.

        public int Tick;
        public SeasonsType Season;
        public int SeasonCounter;

        public string ShortLog;     //This is pre setted.
        public string LongLog;      //This will be randomized.
    }
}
