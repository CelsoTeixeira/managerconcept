using UnityEngine;
using System.Collections;
using Building;
using Entity.WorkerNPC;

namespace Entity.Behaviour
{
    public interface IMovement
    {
        void GoToEmptyTile(Vector3 goTo);   //Use when we want to just move.
        void GoToBuild(BuildObject goTo, WorkerState state);       //Use when we want to go to a build.
    }
}