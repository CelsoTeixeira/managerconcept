using System;
using UnityEngine;
using System.Collections;
using GameLogic;
using GameLogic.Seasons;

namespace GameLogic
{
    public class GameTimer : MonoBehaviour
    {
        private int _timeCounter = 0;                                       //Tick timer counter.
        public int TimeCounter { get { return _timeCounter; } }

        private int _seasonCounter = 1;                                     //Season timer counter, we start at 1 here.
        public int SeasonCounter { get { return _seasonCounter; } }
        
        private GameControl _gameControl;
        private GameTimeUI _gameTimeUi;
        private SeasonControl _seasonControl;

        #region Mono
        void Awake()
        {
            _gameControl = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameControl>();
            _gameTimeUi = GameObject.FindGameObjectWithTag(Helper.UiController).GetComponent<GameTimeUI>();

            _seasonControl = GameObject.FindGameObjectWithTag(Helper.SeasonController).GetComponent<SeasonControl>();
        }

        void OnEnable()
        {
            _gameControl.TickControl.UpdateTick.Subscribe(TimerUpdate);
        }

        void OnDisable()
        {
            _gameControl.TickControl.UpdateTick.UnSubscribe(TimerUpdate);
        }
        #endregion
        
        /// <summary>
        ///     This is called from the main game loop we have.
        /// Its not constant, its called everytime we have a tick,
        /// based on current time.
        /// 
        ///     This simple does the count for the current time and 
        /// for the season.
        ///     1 season = 60 ticks.
        /// 
        ///     When we get a new season, we call for the NextSeason on 
        /// SeasonControl and let it handle what it need to handle.
        ///     We also only ask for a UI update when we change anything 
        /// usefull there.
        /// </summary>
        private void TimerUpdate()
        {
            //Debug.Log("Tempo: " + Time.time);

            _timeCounter++;
            
            if (_timeCounter >= 60)
            {
                _seasonCounter++;                                       //Increase the season counter
                _seasonControl.NextSeason();                            //Call for the next season
                
                _gameTimeUi.UpdateSeasonTextManually(_seasonCounter);   //Call for a UI update

                //Debug.Log("Survived one season!");
                
                _timeCounter = 0;       //Reset the time counter.
            }
        }




    }
}