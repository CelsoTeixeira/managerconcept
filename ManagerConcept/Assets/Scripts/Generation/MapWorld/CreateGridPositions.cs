using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation
{
    [Serializable]
    public class CreateGridPositions
    {
        public List<Vector3> GridPositions;

        public CreateGridPositions()
        {
            //Make sure we initialize the list when we create a new instance of this.
            GridPositions = new List<Vector3>();
        }
        
        //Generate a new list of positions.
        public List<Vector3> GeneratePositions(GridDefinition def)
        {
            //Make sure we have a empty list
            GridPositions.Clear();

            //Generate the new positions for the grid
            for (int x = 0; x < def.Size_X; x += def.TileSize_X)
            {
                for (int y = 0; y < def.Size_Y; y += def.TileSize_Y)
                {
                    GridPositions.Add(new Vector3(x, y, 0));
                }
            }

            //Return the new list
            return GridPositions;
        }
    }

}