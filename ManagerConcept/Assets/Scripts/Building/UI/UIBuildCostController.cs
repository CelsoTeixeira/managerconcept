using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIBuildCostController : MonoBehaviour
{

    public GameObject Panel;

    public Text WoodCost;
    public Text TickCost;

    void Awake()
    {
        Panel.SetActive(false);
    }

    public void RequestPanel(string woodCost, string tickCost)
    {
        WoodCost.text = woodCost;
        TickCost.text = tickCost;

        Panel.SetActive(true);
    }

    public void HidePanel()
    {
        Panel.SetActive(false);
    }

}