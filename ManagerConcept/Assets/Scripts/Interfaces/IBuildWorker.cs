using UnityEngine;
using System.Collections;


using Entity.WorkerNPC;

public interface IBuildWorker
{
    void AddWorker(Worker worker);
    void RemoveWorker(Worker worker);
    void RemoveAllWorkers();
}