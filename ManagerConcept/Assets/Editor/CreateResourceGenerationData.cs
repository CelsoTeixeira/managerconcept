using UnityEngine;
using UnityEditor;

using Entiy.Build.CustomData;

public class CreateResourceGenerationData : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Resource Generation Data")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<ResourceGenerationData>();
    }
}
