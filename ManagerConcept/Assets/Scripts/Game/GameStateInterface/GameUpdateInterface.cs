using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameLogic;

public class GameUpdateInterface : BaseEventInterface 
{
    public override void OnEnable()
    {
        _GameControl.StartGameEvent.Subscribe(ShowPanels);
        _GameControl.PauseGameEvent.Subscribe(HidePanels);
        _GameControl.MainMenuEvent.Subscribe(HidePanels);
    }

    public override void OnDisable()
    {
        _GameControl.StartGameEvent.UnSubscribe(ShowPanels);
        _GameControl.PauseGameEvent.UnSubscribe(HidePanels);
        _GameControl.MainMenuEvent.UnSubscribe(HidePanels);
    }
}