using System;
using UnityEngine;
using System.Collections;
using Entity.WorkerNPC;

namespace Entity.Behaviour.Status
{
    [Serializable]
    public class SleepStatus : StatusBase
    {
        protected override void ControlConsumeRateTick()
        {
            if (Consuming)
            {
                if (Rate >= _amountToConsume)
                {
                    Consuming = false;
                    this.BehaviorUpdate.ChangeState(WorkerState.Wanderer);
                    return;
                }

                float amountToRecover = ConsumeRate*Modifier;

                _rate += amountToRecover;                
            }
        }


        public override bool Consume()
        {
            if (Consuming)
                return false;

            _amountToConsume = UnityEngine.Random.Range(90, 100);

            Consuming = true;
            
            return true;
        }
    }
}