using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Entity.Control;
using Entity.WorkerNPC;

namespace Building
{
    public class BuildTracking : MonoBehaviour
    {
        public List<BuildObject> BuildsComplete = new List<BuildObject>();
        public List<BuildObject> OnConstruction = new List<BuildObject>();
        public List<BuildObject> BuildsWithoutWorkers = new List<BuildObject>(); 

        public Vector3 CityCenterPosition;

        private WorkerList _workerList;
        private BuildingControl _buildingControl;        

        void Awake()
        {
            _workerList = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerList>();
            _buildingControl = GetComponent<BuildingControl>();
        }

        public void OnEnable()
        {
            _buildingControl.ConstantUpdate.Subscribe(BuildTrackerUpdate);
        }

        public void OnDisable()
        {
            _buildingControl.ConstantUpdate.UnSubscribe(BuildTrackerUpdate);
        }

        private void BuildTrackerUpdate()
        {
            //Debug.Log("Tracking update");

            if (BuildsWithoutWorkers.Count > 0)
            {
                if (_workerList.AskForWorkers(BuildsWithoutWorkers[0]))
                {
                    //Debug.Log("Sending new worker!");
                    BuildObject build = BuildsWithoutWorkers[0];
                    //Debug.Log("Moving the build to OnConstruction");
                    BuildsWithoutWorkers.RemoveAt(0);
                    OnConstruction.Add(build);
                }               
            }
        }

        public void AddNewBuild(BuildObject build)
        {
            //Debug.Log("Adding a new build!");
            BuildsWithoutWorkers.Add(build);            
        }
        

        public void BuildComplete(BuildObject build)
        {
            if (BuildsWithoutWorkers.Contains(build))
            {
                BuildsWithoutWorkers.Remove(build);
            }

            if (OnConstruction.Contains(build))
            {
                OnConstruction.Remove(build);
            }

            if (!BuildsComplete.Contains(build))
            {
                BuildsComplete.Add(build);
            }
        }
    }
}