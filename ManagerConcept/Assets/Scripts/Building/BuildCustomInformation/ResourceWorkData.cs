using UnityEngine;
using System.Collections;

namespace Entiy.Build.CustomData
{
    [System.Serializable]
    public class ResourceWorkData : ScriptableObject
    {
        public int ConfortWorkers;      //This is the number of workers we can have working and preserve the Recover rate.
        public int MaximunWorkers;      //This is the maximun number of workers we can have inside the build.
        public AnimationCurve GraphWorkerInfluence;    //This is the influence each worker will have on the Recover and Decay rate.
    }
}