using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Generation.Biomes
{
    //NOTE(Celso): We could save this data for future map generation if we 
    //implement some seed system.
    [Serializable]
    public struct EpicenterData
    {
        public Vector3 Position;
        public BiomeSize Size;
        public float InfluenceArea;
    }

    [Serializable]
    public class BiomeEpicenters
    {
        public List<EpicenterData> FindEpicenter(List<TileInformation> infos, GridDefinition def, int maxAttempts)
        {
            List<Vector3> epicentersPosition = new List<Vector3>();
            List<EpicenterData> epicenterDatas = new List<EpicenterData>();

            for (int x = 0; x < maxAttempts; x++)
            {
                //Get a random point inside our grid definition
                Vector3 currentEpicenter = BiomeUtility.GetRandomEpicenter(def);

                //Debug.Log("New epicenter: " + currentEpicenter);

                //We already have this point ?
                if (!epicentersPosition.Contains(currentEpicenter))
                {
                    EpicenterData newEpicenter = new EpicenterData();
                        
                    BiomeSize newSize = BiomeSize.None;
                    float influenceArea = 0f;

                    //RNG to get a BiomeSize and InfluenceArea based on the size.
                    int sizeRange = Random.Range(0, 3);
                    switch (sizeRange)
                    {
                        case 0:
                            newSize = BiomeSize.Small;
                            influenceArea = BiomeUtility.GetBiomeInfluenceArea(newSize);
                            break;

                        case 1:
                            newSize = BiomeSize.Medium;
                            influenceArea = BiomeUtility.GetBiomeInfluenceArea(newSize);
                            break;

                        case 2:
                            newSize = BiomeSize.Large;
                            influenceArea = BiomeUtility.GetBiomeInfluenceArea(newSize);
                            break;
                    }

                    //Setting up basic data for the epicenterData.
                    newEpicenter.Position = currentEpicenter;
                    newEpicenter.Size = newSize;
                    newEpicenter.InfluenceArea = influenceArea;

                    //We start with a validPosition.
                    bool validPosition = true;
                    
                    //If we have points on the List we check for validation.
                    if (epicentersPosition.Count > 0)
                    {
                        //For each point inside the list
                        for (int i = 0; i < epicentersPosition.Count; i++)
                        {
                            //If our posisition its not valid we break from the for loop.
                            if (!validPosition)
                                break;
                            
                            //We check the distance from each point inside our list to the current point.
                            float Distance = Vector3.Distance(currentEpicenter, epicentersPosition[i]);

                            //float MedPoint = GetMedPoint(currentEpicenter, epicentersPosition);

                            //If this distance its less than the influence area we're generated, we dont
                            //want this position because it's to close to another point already on the list.
                            if (Distance < influenceArea)
                            {
                                validPosition = false;
                            }
                        } 
                    }

                    //After validation, if we have a validPosition we add the point to the tracking list.
                    if (validPosition)
                    {
                        epicentersPosition.Add(currentEpicenter);
                        epicenterDatas.Add(newEpicenter);   
                    }
                }
            }

            //Send back our epicenterData.
            return epicenterDatas;
        }
                
        private float GetMedPoint(Vector3 point, List<Vector3> otherPoints)
        {
            if (otherPoints.Count == 0)
                return 0;

            float medPoint = 0f;

            for (int x = 0; x < otherPoints.Count; x++)
            {
                float distance = Vector3.Distance(point, otherPoints[x]);

                medPoint += distance;
            }

            medPoint = medPoint / otherPoints.Count;
            return medPoint;
        }
    }
}
