using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Entity.Behaviour.Status;
using Entity.Control;
using Entity.WorkerNPC;

namespace Entity.Behaviour
{
    public enum WorkerState
    {
        Wanderer = 1,       //If we're here we're free.
        Working = 2,         //If we're here we're busy.
        Moving = 3,
        Eating = 4,
        Drinking = 5,
        Sleeping = 6,
    };

    [RequireComponent(typeof(Movement))]
    [RequireComponent(typeof(Wanderer))]
    [RequireComponent(typeof(StatusControl))]
    public class BehaviorUpdate : MonoBehaviour
    {
        [Header("Consume Particles")]
        [Tooltip("The actual particle system.")]
        public ParticleSystem ConsumeParticleSystem;
        [Tooltip("The renderer from the particle system.")]
        public Renderer ConsumeRenderer;   

        //TODO(Celso): Change this to a different class and have more options and not a list to make things clear.
        public List<Material> ParticleMaterials; 

        public Vector3 Home;             //The worker home, will consume there.
        public Vector3 WorkPosition;        //The workplace, will work there.
        public GameObject WorkPlace;
        public IBuildWorker IBuild;

        public bool HaveWork = false;       //If we have or not a work.

        public float DistanceToEnterBuilding = 1.0f;                //The distance we need to be from the Target to enter it.
        public Vector2 DiferenceDistance = new Vector2(0f, 4f);     //This is used on the NearPosition method to add some diference on the position we're going.

        public WorkerState OldState;

        public WorkerState CurrentState = WorkerState.Wanderer;

        public WorkerState StateToChange;        
        public Vector3 Target;
        
        private Movement _movementState;
        public Movement MovementState
        {
            get { return _movementState; }
            set { _movementState = value; }
        }

        private Wanderer _wandererState;
        public Wanderer WandererState
        {
            get { return _wandererState; }
            set { _wandererState = value; }
        }
        
        private Worker _worker;
        public Worker Worker
        {
            get { return _worker; }
            set { _worker = value; }
        }

        private WorkerList _workerList;
        public WorkerList WorkerList
        {
            get { return _workerList; }
            set { _workerList = value; }
        }

        private StatusControl _statusControl;

        void Awake()
        {
            this.MovementState = GetComponent<Movement>();
            this.WandererState = GetComponent<Wanderer>();

            this.WandererState.Movement = MovementState;
            this.WandererState.BehaviourSetup();

            _statusControl = GetComponent<StatusControl>();            
        }

        //TODO(Celso):  Right now, after we consume we just go to Wanderer mode, what we want to do it's to add some performace there, if we just consume 
        //TODO(Celso): it's because we have some priority on High, what we can do its to check for every other status and see if we have any on High and Medium, 
        //TODO(Celso): if we have we can just consume and save the travel time.
        public void ChangeState(WorkerState stateToGo, GameObject buildToGo = null)
        {
            //Debug.Log("Trying to change states!");

            _worker.ActiveSprites();    //Make sure the sprites are active.

            //This update the old state.
            if (CurrentState != WorkerState.Moving)
            {
                OldState = CurrentState;
            }

            //NOTE(Celso): After we consume, we're sending the worker to Wanderer mode, it's ok if we don't work anywhere,
            //so this will check if the Worker have a workplace, if true, we will send it to work and not walk around.
            if (HaveWork && stateToGo == WorkerState.Wanderer)
            {
                //Go to work instead!
                StateToChange = WorkerState.Working;
                
                //BuildToGo.transform.position = WorkPosition;
                
                CurrentState = WorkerState.Moving;

                Target = WorkPosition;
                MovementState.StartPath(Target);
            }
            else if (buildToGo != null)
            {
                StateToChange = stateToGo;      //Update the state we want to go.
                //BuildToGo = buildToGo;          //Update the build we need to go.

                CurrentState = WorkerState.Moving;      //Go to Moving state and CheckDistance().

                // If we're going to work, we just want to get there and deactive the sprite.
                if (stateToGo == WorkerState.Working) 
                {
                    Target = buildToGo.transform.position;
                    MovementState.StartPath(Target);
                }
                //Else we want to go to a near position and do stuff there.
                else
                {
                    Target = BehaviorHelper.NearPosition(buildToGo.transform.position, DiferenceDistance);
                    MovementState.StartPath(Target);
                }
            }
            //If we're going to consume, we want a near position.
            else if (stateToGo == WorkerState.Drinking || stateToGo == WorkerState.Eating ||
                     stateToGo == WorkerState.Sleeping)                                         
            {
                StateToChange = stateToGo;
                
                CurrentState = WorkerState.Moving;
                
                //This will make some small position diference.
                Target = BehaviorHelper.NearPosition(Home, DiferenceDistance);
                MovementState.StartPath(Target);
            }
            else   //This is a fail safe
            {
                CurrentState = stateToGo;
            }

            //_statusControl.ChangeModifiers(CurrentState);
        }

        //This is constant.
        public void StateUpdate()
        {
            _statusControl.StatusControlConstantUpdate();      //This is the Update for every status we have on the NPC.
            
            //NOTE(Celso): This is kinda of a hack, just because I'm fucking tracking this on the WorkersList but its fucked there.
            if (!HaveWork)
            {
                _workerList.AddNewFreeWorker(_worker);
            }
            
            switch (CurrentState)
            {
                case WorkerState.Moving:
                    CheckDistance();
                    if (ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Stop();

                    break;
                
                case WorkerState.Wanderer:
                    if (!_worker.CurrentSelection)
                        WandererState.BehaviourUpdate();

                    if (ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Stop();
                    break;

                case WorkerState.Working:

                    if (ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Stop();
                    break;

                case WorkerState.Eating:
                    ConsumeRenderer.material = ParticleMaterials[0];
                    if (!ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Play();
                    
                    //_statusControl.Consuming = true;
                    break;
                
                case WorkerState.Drinking:
                    ConsumeRenderer.material = ParticleMaterials[1];
                    if (!ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Play();
                    
                    //_statusControl.Consuming = true;
                    break;

                case WorkerState.Sleeping:
                    ConsumeRenderer.material = ParticleMaterials[2];
                    if (!ConsumeParticleSystem.isPlaying)
                        ConsumeParticleSystem.Play();
                    
                    //_statusControl.Consuming = true;
                    break;
            }
        }
        
        //NOTE(Celso): When we change to any state that have a Consume method, we want to stop any other Consume method
        //we may have on a diferent state.
        private void CheckDistance()
        {
            //Debug.Log("Distancia: " + Vector3.Distance(this.transform.position, Target));

            if (Vector3.Distance(this.transform.position, Target) < DistanceToEnterBuilding)   //Check the distance
            {
                switch (StateToChange)
                {
                    case WorkerState.Working:
                        //IBuildWorker buildWorker = BuildToGo.GetComponent<IBuildWorker>();
                        IBuild.AddWorker(_worker);
                        CurrentState = WorkerState.Working;
                        _worker.EnterBuild();
                        break;

                    case WorkerState.Eating:
                        _statusControl.HungerStatus.Consume();
                        _statusControl.ThirstyStatus.StopConsume();
                        _statusControl.SleepStatus.StopConsume();
                        CurrentState = WorkerState.Eating;
                        break;

                    case WorkerState.Drinking:
                        _statusControl.ThirstyStatus.Consume();
                        _statusControl.HungerStatus.StopConsume();
                        _statusControl.SleepStatus.StopConsume();
                        CurrentState = WorkerState.Drinking;
                        break;
                    
                    case WorkerState.Sleeping:                        
                        _statusControl.SleepStatus.Consume();
                        _statusControl.HungerStatus.StopConsume();
                        _statusControl.ThirstyStatus.StopConsume();
                        CurrentState = WorkerState.Sleeping;
                        break;
                }

                _statusControl.ChangeModifiers(CurrentState);   //We need to refresh the decay rate to reflect the working state.

                ResetCheckDistanceVariables();
            }
        }

        private void ResetCheckDistanceVariables()
        {
            StateToChange = WorkerState.Wanderer;
        }


        //TODO(Celso): I don't like all this naked string here.
        public string GetCurrentStatus()
        {
            switch (CurrentState)
            {
                case WorkerState.Drinking:
                    return "Bebendo água";
                    
                case WorkerState.Eating:
                    return "Comendo";
                    
                case WorkerState.Moving:
                    return "Andando";
                   
                case WorkerState.Sleeping:
                    return "Dormindo";
                    
                case WorkerState.Wanderer:
                    return "Atoa";
                    
                case WorkerState.Working:
                    return "Trabalhando";                    
            }

            return null;
        }
    }
}