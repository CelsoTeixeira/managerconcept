using UnityEngine;
using System;
using System.Collections.Generic;

using GameLogic;
using GameLogic.Seasons;

namespace Generation.History
{    
    public class HistoryLogComponent : MonoBehaviour
    {
        private Dictionary<string, LogBase> _logsCollection = new Dictionary<string, LogBase>();
        public Dictionary<string, LogBase> LogsCollection { get { return _logsCollection; } }

        public List<LogBase> Logs;

        private GameTimer _gameTimer;
        private SeasonControl _seasonControl;
        
        public delegate void LogAdder();
        public event LogAdder OnLogAdd;         //This is called when we add a new log on the collection.
        //We want to show some UI indication that we have new logs.

        private LongLogDB _longLogDb;

        #region Mono
        void Awake()
        {
            _gameTimer = GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<GameTimer>();
            _seasonControl = GameObject.FindGameObjectWithTag(Helper.SeasonController).GetComponent<SeasonControl>();

            _longLogDb = GetComponent<LongLogDB>();
        }    
        #endregion

        /// <summary>
        ///     We want to add with the log, the current time and the current season with the season counter.
        ///     Something like this:
        ///         40 of Summer, season 3.
        ///     Then we add the current log from the event.
        ///         The well have been constructed.
        ///     
        ///     The final log enter will be something like this:
        ///         40 of Summer, season 3.
        ///             The well have been constructed.
        ///          
        ///         41 of Summer, season 3.
        ///             New people have arrived to the reign.
        ///         
        ///         50 of Summer, season 3.
        ///           The end of this season its approaching. We shall prepare for the next one!         
        /// </summary>
        public void AddLog(LogBase log)
        {
            string timeCounter = _gameTimer.TimeCounter.ToString();
            string seasonCounter = _gameTimer.SeasonCounter.ToString();
            string currentSeason = _seasonControl.CurrentSeason.ToString();

            int timeCounterInt = _gameTimer.TimeCounter;
            int seasonCountInt = _gameTimer.SeasonCounter;
            SeasonsType seasonType = _seasonControl.CurrentSeason;
            
            string timeText = "Dia " + timeCounter + " do " + currentSeason + ", ano " + seasonCounter;            

            LogBase newLogBase= new LogBase();
            newLogBase.Date = timeText;
            newLogBase.ShortLog = log.ShortLog;
            newLogBase.Tick = timeCounterInt;
            newLogBase.SeasonCounter = seasonCountInt;
            newLogBase.Season = seasonType;
            newLogBase.Type = log.Type;

            //Gere we will choose a long log aswell.
            if (newLogBase.Type == HistoryType.ConstructionFarm)
            {
                newLogBase.LongLog = _longLogDb.GetLongLog(HistoryType.ConstructionFarm);
            }
            else if (newLogBase.Type == HistoryType.ConstructionWell)
            {
                newLogBase.LongLog = _longLogDb.GetLongLog(HistoryType.ConstructionWell);
            }
            else if (newLogBase.Type == HistoryType.ConstructionHouse)
            {
                newLogBase.LongLog = _longLogDb.GetLongLog(HistoryType.ConstructionHouse);
            }

            //Debug.Log("Time text: " + timeText);
            //Debug.Log("Log text: " + log);

            if (!_logsCollection.ContainsKey(timeText))
            {
                _logsCollection.Add(timeText, newLogBase);
            }
            
            Logs.Add(newLogBase);

            if (OnLogAdd != null)
            {
                OnLogAdd.Invoke();
            }            
        }        
    }
}