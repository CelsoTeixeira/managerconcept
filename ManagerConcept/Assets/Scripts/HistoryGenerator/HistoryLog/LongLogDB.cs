using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Generation.History;

using Random = UnityEngine.Random;

public class LongLogDB : MonoBehaviour 
{
    private Dictionary<HistoryType, HistoryEvent> _longLogDB = new Dictionary<HistoryType, HistoryEvent>();
    private Dictionary<HistoryType, HistoryEvent> _wellHistoryEvents = new Dictionary<HistoryType, HistoryEvent>();
    private Dictionary<HistoryType, HistoryEvent> _farmHistoryEvents = new Dictionary<HistoryType, HistoryEvent>();  
    private Dictionary<HistoryType, HistoryEvent> _houseHistoryEvents = new Dictionary<HistoryType, HistoryEvent>();

    public List<HistoryEvent> _houseEvents;
    public List<HistoryEvent> _farmEvents;
    public List<HistoryEvent> _wellEvents;   

    public List<HistoryEvent> AddOnAwake;

    void Awake()
    {
        for (int i = 0; i < AddOnAwake.Count; i++)
        {
            HistoryType type = AddOnAwake[i].HistoryType;

            switch (type)
            {
                case HistoryType.ConstructionHouse:
                    //_houseHistoryEvents.Add(AddOnAwake[i].HistoryType, AddOnAwake[i]);
                    _houseEvents.Add(AddOnAwake[i]);

                    break;

                case HistoryType.ConstructionFarm:
                    //_farmHistoryEvents.Add(AddOnAwake[i].HistoryType, AddOnAwake[i]);
                    _farmEvents.Add(AddOnAwake[i]);
                    break;

                case HistoryType.ConstructionWell:
                    //_wellHistoryEvents.Add(AddOnAwake[i].HistoryType, AddOnAwake[i]);
                    _wellEvents.Add(AddOnAwake[i]);
                    break;
            }

            //_longLogDB.Add(AddOnAwake[i].HistoryType, AddOnAwake[i]);
        }

        //Debug.Log(_wellHistoryEvents.Count);        
    }

    public string GetLongLog(HistoryType type)
    {
        switch (type)
        {
            case HistoryType.ConstructionHouse:
                if (_houseEvents.Count != 0)
                    return _houseEvents[Random.Range(0, _houseEvents.Count)].HistoryText;

                break;
                
            case HistoryType.ConstructionFarm:
                if (_farmEvents.Count != 0)
                    return _farmEvents[Random.Range(0, _farmEvents.Count)].HistoryText;

                break;

            case HistoryType.ConstructionWell:
                if (_wellEvents.Count != 0)
                    return _wellEvents[Random.Range(0, _wellEvents.Count)].HistoryText;

                break;
        }

        //If we fail, just send this back.
        return AddOnAwake[Random.Range(0, AddOnAwake.Count)].HistoryText;
    }
}

[Serializable]
public class HistoryEvent
{
    public string HistoryText;
    public HistoryType HistoryType;
}