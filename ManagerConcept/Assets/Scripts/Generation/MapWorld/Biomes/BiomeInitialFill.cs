using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Generation.Biomes
{
    public static class BiomeInitialFill
    {
        //This will simply fill the grid with grass.
        public static void FillGridWithGrass(List<TileInformation> infos)
        {
            for (int x = 0; x < infos.Count; x++)
            {
                //infos[x].TileDefinition.MainType = MainType.Grama;
                //infos[x].PostProcess();

                infos[x].ChangeTileType(TileType.Grass);
                infos[x].UpdateTileSprite();
            }
        }
    }
}