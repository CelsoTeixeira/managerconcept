using UnityEngine;
using System.Collections;

namespace Entiy.Build.CustomData
{
    [System.Serializable]
    public class ResourcePoolRate : ScriptableObject
    {
        public float Rate;
        public int TickRate;
        public AnimationCurve InfluenceGraph;   //We can influence the rate with tick if we want.
    }
}