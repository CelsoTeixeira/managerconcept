using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameLogic
{
    [Serializable]
    public class GameEvent
    {
        private List<Action> Events = new List<Action>();

        public void Subscribe(Action action)
        {
            Events.Add(action);
        }

        public void UnSubscribe(Action action)
        {
            Events.Remove(action);
        }

        public void Broadcast()
        {
            if (Events.Count == 0)
                return;

            for (int i = 0; i < Events.Count; i++)
            {
                Events[i].Invoke();
            }
        }
    }

}