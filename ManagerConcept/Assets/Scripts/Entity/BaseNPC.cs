using UnityEngine;
using System.Collections;

public class BaseNPC : BaseEntity
{
    [Tooltip("The NPC name.")]
    public string Name;

    //This will determine if we have control over the NPC.
    //When we implement the IMovement we just check this 
    //and control if we can move it or not.
    public bool HaveControl = false;

    public SpriteRenderer Head;
    public SpriteRenderer Body;
    
    public void ChangeArt(Sprite head, Sprite body)
    {
        Head.sprite = head;
        Body.sprite = body;
    }

    protected virtual void DeactiveSprites()
    {
        Head.gameObject.SetActive(false);
        Body.gameObject.SetActive(false);
    }

    public virtual void ActiveSprites()
    {
        Head.gameObject.SetActive(true);
        Body.gameObject.SetActive(true);
    }
}