using UnityEngine;
using System.Collections;

/// <summary>
///     This is simple Helper class to avoid
/// naked string on the code.
///     This is mainly related to Tags used when
/// we want to GetComponents.
/// </summary>
public static class Helper
{
    //Controller game object
    public static string GameController = "GameController";
    public static string CameraController = "MainCamera";
    
    public static string InputHandler = "InputHandler";

    public const string GeneralHandler = "GeneralHandler";
    
    public static string BuildController = "BuildController";
    public static string GridController = "GridController";
    public static string ResourceController = "ResourceController";
    public static string WorkerController = "WorkerController";
    public static string EventController = "EventController";
    public static string SeasonController = "SeasonController";
    public static string HistoryController = "HistoryController";

    public static string UiController = "UiController";
    
    //Tags
    public static string BuildTag = "Build";

    //UI Panels
    public static string AdvancedGeneratorPanel = "AdvancedGeneratorPanel";
    public static string InformationPanel = "InfoPanel";
    public static string CameraMovementLockUnlock = "CameraMovementLocker";
}