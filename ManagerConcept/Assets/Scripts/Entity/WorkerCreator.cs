using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Building;
using Entity.Behaviour;
using Entity.Behaviour.Status;
using Entity.Control;
using Generation.Name;

namespace Entity.WorkerNPC
{
    public class WorkerCreator : MonoBehaviour
    {
        public GameObject WorkerPrefab;

        public Transform WorkerHolder;
        
        public NpcArtCollection ArtCollection;

        private BuildTracking _buildTracking;
        private PopulationController _populationController;

        private NameGenerator _nameGenerator;

        void Awake()
        {
            _buildTracking = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildTracking>();
            _populationController =
                GameObject.FindGameObjectWithTag(Helper.GameController).GetComponent<PopulationController>();

            _nameGenerator = GetComponent<NameGenerator>();
        }

        public void SpawnNewWorker(Vector3 position)
        {
            GameObject newWorker = Instantiate(WorkerPrefab, position, Quaternion.identity) as GameObject;
            newWorker.transform.SetParent(WorkerHolder);            
        }

        public void SpawnWorkerAtCaslte()
        {
            GameObject newWorker = Instantiate(WorkerPrefab, _buildTracking.CityCenterPosition, Quaternion.identity) as GameObject;
            newWorker.transform.SetParent(WorkerHolder);

            BaseNPC npc = newWorker.GetComponent<BaseNPC>();
            npc.Name = _nameGenerator.GenerateRandomName(Random.Range(5, 9), Random.Range(10, 15), false);
            
            BehaviorUpdate update = newWorker.GetComponent<BehaviorUpdate>();
            update.Home = _buildTracking.CityCenterPosition;

            StatusControl statusControl = newWorker.GetComponent<StatusControl>();
            statusControl.Init();

            Worker worker = newWorker.GetComponent<Worker>();
            worker.ForceMovement();

            int rngHead = Random.Range(0, ArtCollection.Heads.Count);
            int rngBody = Random.Range(0, ArtCollection.Bodys.Count);

            npc.ChangeArt(ArtCollection.Heads[rngHead], ArtCollection.Bodys[rngBody]);

            _populationController.AddPopulation();
        }
    }
}