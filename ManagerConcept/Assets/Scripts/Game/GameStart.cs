using UnityEngine;
using System.Collections;
using Building;
using CameraController;
using Entity.WorkerNPC;

namespace GameLogic
{
    public class GameStart : MonoBehaviour
    {
        public int AmountOfWorkers = 4;

        public double FoodInitial = 400;
        public double WoodInitial = 200;
        public double WaterInitial = 400;

        private GridController _gridController;

        private CameraMovement _cameraMovement;
        private GameControl _gameControl;
        private WorkerCreator _workerCreator;

        private BuildPlacement _buildPlacement;
        private BuildCollection _buildCollection;
        //private BuildTracking _buildTracking;

        private ResourceContainer _resourceContainer;

        void Awake()
        {
            _gridController = GameObject.FindGameObjectWithTag(Helper.GridController).GetComponent<GridController>();
           
            _cameraMovement = GameObject.FindGameObjectWithTag(Helper.CameraController).GetComponent<CameraMovement>();
            _workerCreator = GameObject.FindGameObjectWithTag(Helper.WorkerController).GetComponent<WorkerCreator>();

            _buildPlacement = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildPlacement>();
            _buildCollection = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildCollection>();
            //_buildTracking = GameObject.FindGameObjectWithTag(Helper.BuildController).GetComponent<BuildTracking>();

            _resourceContainer =
                GameObject.FindGameObjectWithTag(Helper.ResourceController).GetComponent<ResourceContainer>();

            _gameControl = GetComponent<GameControl>();

            Screen.SetResolution(1024, 768, false);

            //Debug.Log(Application.dataPath);
        }

        private void InitialSetup()
        {
            //Generate the grid.
            _gridController.CreateGrid();

            //Force camera to go center
            Vector2 centerGrid = GridUtility.GetGridCenter(_gridController.GridDefinition);
            
            _cameraMovement.ForceCameraPosition(centerGrid);

            //Put center build
            _buildPlacement.PlaceNewBuild(_buildCollection.GetBuildFromCollection(BuildName.Castle), centerGrid, BuildType.Center);
            
            //Instantiate workers
            for (int i = 0; i < AmountOfWorkers; i++)
            {
                Vector3 pos = centerGrid + (Random.insideUnitCircle * 6);
                pos.z = 0;

                _workerCreator.SpawnWorkerAtCaslte();
            }
            
            _resourceContainer.AddAmount(ResourceType.Food, FoodInitial);
            _resourceContainer.AddAmount(ResourceType.Water, WaterInitial);
            _resourceContainer.AddAmount(ResourceType.Wood, WoodInitial);

            //Change GameState to Update!
            _gameControl.ChangeGameState(GameState.Update);
        }

        public void StartGame()
        {
            InitialSetup();
        }

        public void ContinueGame()
        {
            _gameControl.ChangeGameState(GameState.Update);
        }

        public void ReloadLevel()
        {
            Application.LoadLevel(0);
        }
    }

}