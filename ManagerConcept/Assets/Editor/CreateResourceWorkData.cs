using UnityEngine;
using System.Collections;
using Entiy.Build.CustomData;
using UnityEditor;

public class CreateResourceWorkData : MonoBehaviour 
{
    [MenuItem("Assets/Create/CustomAssets/Resource WorkPosition Data")]
    public static void Create()
    {
        CustomAssetUtility.CreateAsset<ResourceWorkData>();
    }
}
