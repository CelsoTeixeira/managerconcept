using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class TileDefinition 
{
    public TileType Type = TileType.None;
    public MainType MainType = MainType.None;
    public DensityType DensityType = DensityType.None;
}