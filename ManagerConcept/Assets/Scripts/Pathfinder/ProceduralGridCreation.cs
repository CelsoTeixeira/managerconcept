using UnityEngine;
using System.Collections;
using Generation;
using Pathfinding;

public class ProceduralGridCreation 
{
    public static void GridUpdate(GridDefinition col)
    {
        var graph = (GridGraph) AstarPath.active.astarData.FindGraphOfType(typeof (GridGraph));

        graph.width = col.Size_X;
        graph.depth = col.Size_Y;

        Vector3 center = GridUtility.GetGridCenter(col);
        center.x -= 0.5f;
        center.y -= 0.5f;
        center.z = 0f;

        graph.center = center;

        graph.nodeSize = 1f;

        graph.UpdateSizeFromWidthDepth();

        //graph.collision.mask = LayerMask.GetMask();

        AstarPath.active.Scan();
    }
}