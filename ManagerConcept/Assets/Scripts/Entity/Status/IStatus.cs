using System;
using UnityEngine;
using System.Collections;

namespace Entity.Behaviour.Status
{
    public interface IStatus
    {
        void ChangeModifier(int i);     //Change the modifier                
        bool Consume();                 //Increase the rate for the current status
    }

}


